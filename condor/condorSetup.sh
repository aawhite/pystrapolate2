#! /bin/bash
# Script for running many similar jobs

export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
source /cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/user/atlasLocalSetup.sh 
# asetup AnalysisBase,21.2.1,here
echo "Running: ../analysis/singleLimit.py ${@}"
lsetup "root 6.12.04-x86_64-slc6-gcc62-opt"

# constructive ee
../analysis/singleLimit.py chirality=LL interference=const channel=ee $@

# ../analysis/singleLimit.py chirality=LR interference=const channel=ee $@
# ../analysis/singleLimit.py chirality=RL interference=const channel=ee $@
# ../analysis/singleLimit.py chirality=RR interference=const channel=ee $@

# # destructive ee
# ../analysis/singleLimit.py chirality=LL interference=dest  channel=ee $@
# ../analysis/singleLimit.py chirality=LR interference=dest  channel=ee $@
# ../analysis/singleLimit.py chirality=RL interference=dest  channel=ee $@
# ../analysis/singleLimit.py chirality=RR interference=dest  channel=ee $@
