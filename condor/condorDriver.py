import os
import numpy as np
from math import log

# path to your analysis
dirname=os.path.dirname

print __file__
analysisPath=os.getcwd()
programName = analysisPath+"../singleLimit.py"
outputDir = "/lustre/umt3/user/aaronsw/dileptonPickles"


def createQueueEntry(fullArgString):
    global jobCount,outputPath, loopVariables
    outputName = fullArgString.replace(" ","_")
    # make nice, unique output name
    argList = fullArgString.split()
    outputName = [ argList[i][1:]+"W"+argList[i+1] for i in range(len(argList)) if i<len(argList) and argList[i] in loopVariables.keys() ]
    outputName = "_".join(outputName)

    outputFullPath = os.path.join(outputPath,outputName)
    fullArgString=fullArgString.format(outputFullPath)
    print "#"*50
    print jobCount, fullArgString

    job = "\n"
    job+= "# number {0}\n".format(jobCount)
    job+= "Arguments = {0}\n".format(fullArgString)
    job+= "Queue\n"

    # save job as file
    f=open(os.path.join(analysisPath,"job.condor"),"a")
    f.write(job)
    f.close()

    jobCount+=1



def makeCondorJob():
    # Make quick condor job for submitting multiple fits
    # save as analysisPath/job.condor
    global analysisPath, logOutputName

    outputDirCondor=os.path.join(analysisPath,logOutputName)
    # os.popen("rm -r {0}; mkdir {1}".format(outputDirRoot, outputDirRoot))
    os.popen("rm -r {0}; mkdir {1}".format(outputDirCondor, outputDirCondor))
    # outputPath=outputDirRoot

    # make header
    job = ""
    job+= "Universe = vanilla\n"
    job+= "Executable = {0}/condorSetup.sh\n".format(analysisPath)
    job+= "Output = {0}/$(Process).out\n".format(outputDirCondor)
    job+= "Error = {0}/$(Process).error\n".format(outputDirCondor)

    # save job as file
    f=open(os.path.join(analysisPath,"job.condor"),"w")
    f.write(job)
    f.close()


# ##########################################
# inputs to function as global variables
# ##########################################

loopVariables = {} 
# programName = "python2"

# output string is where the output root files get saved
outputPath="" # keep empty, global variable
# run string is the arguements for RooSwift


n=40
# n=100
tag="limits-optFitMin-ExtrapMinScan{0}x{0}".format(n)
# tag="limits-narrow-FitMinMaxScan{0}x{0}".format(n)

path = os.path.join(outputDir,"pickle-"+tag+"/limit2d-{{0}}.pickle")
os.popen("rm "+path.format().format("*"))
os.popen("mkdir {0}/pickle-{1}".format(outputDir,tag))
logOutputName = outputDir+"/condorLog-"+tag
os.popen("rm {0}/*; mkdir {0}".format(logOutputName))

limitsOutputPath=outputDir+"/limit2d-{0}".format(tag)
print "#"*20,limitsOutputPath,"*"*20
os.popen("rm -r {0}".format(limitsOutputPath))
os.popen("mkdir {0}".format(limitsOutputPath))

extrapMax     = 6000
lumi          = 139
release       = "21"
nToys         = 100

# limitsOn      = False

# change single to True to run a quick test
# single=True; quick=True
single=False; quick=False

argString=''
# argString += " singleLimit.py"
argString += " lumi={0}".format(lumi)
argString += " extrapMax={0}".format(extrapMax )
argString += " plotOn={0}".format(0)
argString += " nToys={0}".format(nToys)
argString += " limitsOutputPath={0}".format(limitsOutputPath)


fitMins    = np.logspace(log(160,10),log(500,10),n)
fitMaxs    = np.logspace(log(1300,10),log(3100,10),n)
extrapMins = np.logspace(log(1300,10),log(3100,10),n)




# ##########################################
# This is the main thing to make the condor execuitable!
# will save it as analysisPath/job.condor
# ##########################################
jobCount=0
job=""
makeCondorJob() # note, this also deletes previous output...

# fitMax/extrapMin scan
for fitMax in fitMaxs:
    for extrapMin in extrapMins:
        if fitMax>extrapMin: continue
        fitMin=200
        thisArgString  = argString
        thisArgString += " extrapMin={0}".format(int(extrapMin))
        thisArgString += " fitMin={0}".format(int(fitMin))
        thisArgString += " fitMax={0}".format(int(fitMax))
        createQueueEntry(thisArgString)

# # fitMin/fitMax scan
# for fitMax in fitMaxs:
#     for fitMin in fitMins:
#         if fitMin>fitMax: continue
#         extrapMin=fitMax
#         thisArgString  = argString
#         thisArgString += " extrapMin={0}".format(int(extrapMin))
#         thisArgString += " fitMin={0}".format(int(fitMin))
#         thisArgString += " fitMax={0}".format(int(fitMax))
#         createQueueEntry(thisArgString)


print "%"*50
print '''run with\n\tcondor_submit job.condor ; repeat "condor_release aaronsw"'''
print "or:\n\tcondor_submit_test job.condor"
print "Output written to: ",limitsOutputPath
# you could also use: os.popen("condor_submit job.condor")
