
################################################################################
# Dictionary of file locations, PDF functions, etc
################################################################################

# Structure: data[release][channel][pdf]
# where release = 20, 21, prev
# channel = ee, mm

################################################################################
# release 20, current
################################################################################
data20 = {"ee":{},"mm":{}}
# mm
data20["mm"]["backgroundModel"]           = "EXPR::backgroundModel('(0.0024952/(pow(0.0911876-x,2)+pow(0.0024952,2)))*pow(1-pow(x/13.0,1.0/3.0),diphotonLeadingCoef)*pow(x/13.0,p0+p1*log(x/13.0)+p2*log(x/13.0)^2+p3*log(x/13.0)^3)',x,diphotonLeadingCoef[3.9175,1,10],p0[-11.267,-30,10],p1[-4.2736,-10,5],p2[-0.92751,-5,1],p3[-0.083411,-1,0.1])"
data20["mm"]["backgroundTemplate"]        = "../data/mergedHistos_mm.root:mergedSpectrum_total:GeV:rebin10"
# ee
data20["ee"]["backgroundModel"]           = "EXPR::backgroundModel('(0.0024952/(pow(0.0911876-x,2)+pow(0.0024952,2)))*pow(1-pow(x/13.0,0.5),diphotonLeadingCoef)*pow(x/13.0,p0+p1*log(x/13.0)+p2*log(x/13.0)^2+p3*log(x/13.0)^3)',x,diphotonLeadingCoef[2.8088,1,10],p0[-11.492,-30,10],p1[-4.2473,-10,5],p2[-0.92928,-5,1],p3[-0.085168,-1,0.1])"
data20["ee"]["backgroundTemplate"]        = "../data/mergedHistos_ee.root:mergedSpectrum_total:GeV:rebin10"

################################################################################
# release 21, current
################################################################################
data21 = {"ee":{},"mm":{}}
# sharedInputs = "../../SharedInputsProj/rel21_templates/"
sharedInputs = "../../SharedInputsProj/"

##################################################
# Acc*eff
##################################################
data21["ee"]["accEff"] = sharedInputs+"/rel21_finalResults/selectionEfficiency/tfFitter/results_ee/Nominal/accEffParametrisation.root:accEff_TF1"
data21["mm"]["accEff"] = sharedInputs+"/rel21_finalResults/selectionEfficiency/tfFitter/results_mm/Nominal/accEffParametrisation.root:accEff_TF1"

# mm
# 2018 model
# data21["mm"]["backgroundModel"]                        = "EXPR::backgroundModel('(0.0024952/(pow(0.0911876-x,2)+pow(0.0024952,2)))*pow(1-pow(x/13.0,1.0/3.0),diphotonLeadingCoef)*pow(x/13.0,p0+p1*log(x/13.0)+p2*log(x/13.0)^2+p3*log(x/13.0)^3)',x,diphotonLeadingCoef[2.1005,1,15],p0[-1.1710e+01,-30.,10.],p1[-4.2252e+00,-10.,5.],p2[-9.3048e-01,-5.,1.],p3[-8.6747e-02,-1.,0.1])"
# copy Peter's signal model, parameters from new CP recs
data21["mm"]["backgroundModel"]                        = "EXPR::backgroundModel('(0.0024952/(pow(0.0911876-x,2)+pow(0.0024952,2)))*pow(1-pow(x/13.0,1.0/3.0),diphotonLeadingCoef)*pow(x/13.0,p0+p1*log(x/13.0)+p2*log(x/13.0)^2+p3*log(x/13.0)^3)',x,diphotonLeadingCoef[1.88782,0.5,15.],p0[-11.7032,-50.,0.],p1[-4.21916,-20.,0.],p2[-0.931291,-5.,0.],p3[-0.0869486,-1.,0.])"
# works 
data21["mm"]["backgroundModel"]                        = "EXPR::backgroundModel('(2.4952/(pow(91.1876-(x/1000),2)+pow(2.4952,2)))*pow(1-pow((x/1000)/13.0,1.0/3.0),diphotonLeadingCoef)*pow((x/1000)/13.0,p0+p1*log((x/1000)/13.0)+p2*log((x/1000)/13.0)^2+p3*log((x/1000)/13.0)^3)',x,diphotonLeadingCoef[30,1,60],p0[-11.492,-30,0],p1[-4.2473,-10,0],p2[-0.92928,-5,0],p3[-0.085168,-1,0])"
# copy of ee model with 1/2 -> 1/3
data21["mm"]["backgroundModel"]                        = "EXPR::backgroundModel('(2.4952/(pow(91.1876-(x/1000),2)+pow(2.4952,2)))*pow(1-pow((x/1000)/13.0,1.0/3.0),diphotonLeadingCoef)*pow((x/1000)/13.0,p0+p1*log((x/1000)/13.0)+p2*log((x/1000)/13.0)^2+p3*log((x/1000)/13.0)^3)',x,diphotonLeadingCoef[5,1,60],p0[-11.492,-30,0],p1[-4.2473,-10,0],p2[-0.92928,-5,0],p3[-0.085168,-1,0])"
# now tune params
data21["mm"]["backgroundModel"]                        = "EXPR::backgroundModel('(2.4952/(pow(91.1876-(x/1000),2)+pow(2.4952,2)))*pow(1-pow((x/1000)/13.0,1.0/3.0),diphotonLeadingCoef)*pow((x/1000)/13.0,p0+p1*log((x/1000)/13.0)+p2*log((x/1000)/13.0)^2+p3*log((x/1000)/13.0)^3)',x,diphotonLeadingCoef[2.5,1,15],p0[-14.60,-30,0],p1[-4.43,-10,0],p2[-0.931,-5,0],p3[-0.079,-1,0])"
data21["mm"]["data"]                                   = sharedInputs+"/rel21_dataTemplates/mInv_spectrum_combined_ll_rel21_fullRun2.root:mInv_spectrum_201518_mm_1GeV:GeV"
data21["mm"]["dataLog"]                                = sharedInputs+"/rel21_dataTemplates/mInv_spectrum_combined_ll_rel21_logBins_new.root:mInv_spectrum_201518_mm_1GeV"
data21["mm"]["sigLog-const-12"]                         = sharedInputs+"/rel21_ci/templates_r21_ll-oct-logbin/mm/CI_Template_LL_log.root:diff_CI_LL_const_12_TeV"
data21["mm"]["sigLog-const-20"]                         = sharedInputs+"/rel21_ci/templates_r21_ll-oct-logbin/mm/CI_Template_LL_log.root:diff_CI_LL_const_20_TeV"
data21["mm"]["sigLog-const-24"]                         = sharedInputs+"/rel21_ci/templates_r21_ll-oct-logbin/mm/CI_Template_LL_log.root:diff_CI_LL_const_24_TeV"
data21["mm"]["sigLog-const-28"]                         = sharedInputs+"/rel21_ci/templates_r21_ll-oct-logbin/mm/CI_Template_LL_log.root:diff_CI_LL_const_28_TeV"
data21["mm"]["sigLog-const-30"]                         = sharedInputs+"/rel21_ci/templates_r21_ll-oct-logbin/mm/CI_Template_LL_log.root:diff_CI_LL_const_30_TeV"
data21["mm"]["sigLog-const-34"]                         = sharedInputs+"/rel21_ci/templates_r21_ll-oct-logbin/mm/CI_Template_LL_log.root:diff_CI_LL_const_34_TeV"
data21["mm"]["sigLog-const-36"]                         = sharedInputs+"/rel21_ci/templates_r21_ll-oct-logbin/mm/CI_Template_LL_log.root:diff_CI_LL_const_36_TeV"
data21["mm"]["sigLog-const-40"]                         = sharedInputs+"/rel21_ci/templates_r21_ll-oct-logbin/mm/CI_Template_LL_log.root:diff_CI_LL_const_40_TeV"
data21["mm"]["sigLog-const-58"]                         = sharedInputs+"/rel21_ci/templates_r21_ll-oct-logbin/mm/CI_Template_LL_log.root:diff_CI_LL_const_58_TeV"
data21["mm"]["sigLog-dest-12"]                         = sharedInputs+"/rel21_ci/templates_r21_ll-oct-logbin/mm/CI_Template_LL_log.root:diff_CI_LL_dest_12_TeV"
data21["mm"]["sigLog-dest-16"]                         = sharedInputs+"/rel21_ci/templates_r21_ll-oct-logbin/mm/CI_Template_LL_log.root:diff_CI_LL_dest_16_TeV"
data21["mm"]["sigLog-dest-20"]                         = sharedInputs+"/rel21_ci/templates_r21_ll-oct-logbin/mm/CI_Template_LL_log.root:diff_CI_LL_dest_20_TeV"
data21["mm"]["sigLog-dest-24"]                         = sharedInputs+"/rel21_ci/templates_r21_ll-oct-logbin/mm/CI_Template_LL_log.root:diff_CI_LL_dest_24_TeV"
data21["mm"]["sigLog-dest-26"]                         = sharedInputs+"/rel21_ci/templates_r21_ll-oct-logbin/mm/CI_Template_LL_log.root:diff_CI_LL_dest_26_TeV"
data21["mm"]["sigLog-dest-30"]                         = sharedInputs+"/rel21_ci/templates_r21_ll-oct-logbin/mm/CI_Template_LL_log.root:diff_CI_LL_dest_30_TeV"
data21["mm"]["sigLog-dest-40"]                         = sharedInputs+"/rel21_ci/templates_r21_ll-oct-logbin/mm/CI_Template_LL_log.root:diff_CI_LL_dest_40_TeV"
data21["mm"]["backgroundTemplate"]               = sharedInputs+"/rel21_templates-dataNormed/merged_mm-dataNormed.root:mm_dibosonNoOutliers_dilepWt_smoothDY_smoothTTbar:GeV"
data21["mm"]["backgroundTemplateBumpy"]                     = sharedInputs+"/rel21_templates/merged_mm.root:mm_dibosonNoOutliers_dilepWt_nonSmoothDY_nonSmoothNonAllHadTTbar:GeV"
data21["mm"]["backgroundTemplate_KF"]                  = sharedInputs+"/rel21_templates-dataNormed/merged_mm_kFactorSyst_-dataNormed.root:mm_diboson_smoothDY_smoothTTbar:GeV"
data21["mm"]["backgroundTemplate_KF_ALPHAS__1down"]     = sharedInputs+"/rel21_templates-dataNormed/merged_mm_kFactorSyst_ALPHAS__1down-dataNormed.root:mm_dibosonNoOutliers_smoothDY_smoothTTbar:GeV"
data21["mm"]["backgroundTemplate_KF_ALPHAS__1up"]       = sharedInputs+"/rel21_templates-dataNormed/merged_mm_kFactorSyst_ALPHAS__1up-dataNormed.root:mm_dibosonNoOutliers_smoothDY_smoothTTbar:GeV"
data21["mm"]["backgroundTemplate_KF_CHOICE_HERAPDF20"]  = sharedInputs+"/rel21_templates-dataNormed/merged_mm_kFactorSyst_CHOICE_HERAPDF20-dataNormed.root:mm_dibosonNoOutliers_smoothDY_smoothTTbar:GeV"
data21["mm"]["backgroundTemplate_KF_CHOICE_NNPDF30"]    = sharedInputs+"/rel21_templates-dataNormed/merged_mm_kFactorSyst_CHOICE_NNPDF30-dataNormed.root:mm_dibosonNoOutliers_smoothDY_smoothTTbar:GeV"
data21["mm"]["backgroundTemplate_KF_PDF_EV1"]           = sharedInputs+"/rel21_templates-dataNormed/merged_mm_kFactorSyst_PDF_EV1-dataNormed.root:mm_dibosonNoOutliers_smoothDY_smoothTTbar:GeV"
data21["mm"]["backgroundTemplate_KF_PDF_EV2"]           = sharedInputs+"/rel21_templates-dataNormed/merged_mm_kFactorSyst_PDF_EV2-dataNormed.root:mm_dibosonNoOutliers_smoothDY_smoothTTbar:GeV"
data21["mm"]["backgroundTemplate_KF_PDF_EV3"]           = sharedInputs+"/rel21_templates-dataNormed/merged_mm_kFactorSyst_PDF_EV3-dataNormed.root:mm_dibosonNoOutliers_smoothDY_smoothTTbar:GeV"
data21["mm"]["backgroundTemplate_KF_PDF_EV4"]           = sharedInputs+"/rel21_templates-dataNormed/merged_mm_kFactorSyst_PDF_EV4-dataNormed.root:mm_dibosonNoOutliers_smoothDY_smoothTTbar:GeV"
data21["mm"]["backgroundTemplate_KF_PDF_EV5"]           = sharedInputs+"/rel21_templates-dataNormed/merged_mm_kFactorSyst_PDF_EV5-dataNormed.root:mm_dibosonNoOutliers_smoothDY_smoothTTbar:GeV"
data21["mm"]["backgroundTemplate_KF_PDF_EV6"]           = sharedInputs+"/rel21_templates-dataNormed/merged_mm_kFactorSyst_PDF_EV6-dataNormed.root:mm_dibosonNoOutliers_smoothDY_smoothTTbar:GeV"
data21["mm"]["backgroundTemplate_KF_PDF_EV7"]           = sharedInputs+"/rel21_templates-dataNormed/merged_mm_kFactorSyst_PDF_EV7-dataNormed.root:mm_dibosonNoOutliers_smoothDY_smoothTTbar:GeV"
data21["mm"]["backgroundTemplate_KF_PDF_EW__1down"]     = sharedInputs+"/rel21_templates-dataNormed/merged_mm_kFactorSyst_PDF_EW__1down-dataNormed.root:mm_dibosonNoOutliers_smoothDY_smoothTTbar:GeV"
data21["mm"]["backgroundTemplate_KF_PDF_EW__1up"]       = sharedInputs+"/rel21_templates-dataNormed/merged_mm_kFactorSyst_PDF_EW__1up-dataNormed.root:mm_dibosonNoOutliers_smoothDY_smoothTTbar:GeV"
data21["mm"]["backgroundTemplate_KF_PDF__1down"]        = sharedInputs+"/rel21_templates-dataNormed/merged_mm_kFactorSyst_PDF__1down-dataNormed.root:mm_dibosonNoOutliers_smoothDY_smoothTTbar:GeV"
data21["mm"]["backgroundTemplate_KF_PDF__1up"]          = sharedInputs+"/rel21_templates-dataNormed/merged_mm_kFactorSyst_PDF__1up-dataNormed.root:mm_dibosonNoOutliers_smoothDY_smoothTTbar:GeV"
data21["mm"]["backgroundTemplate_KF_PI__1down"]         = sharedInputs+"/rel21_templates-dataNormed/merged_mm_kFactorSyst_PI__1down-dataNormed.root:mm_dibosonNoOutliers_smoothDY_smoothTTbar:GeV"
data21["mm"]["backgroundTemplate_KF_PI__1up"]           = sharedInputs+"/rel21_templates-dataNormed/merged_mm_kFactorSyst_PI__1up-dataNormed.root:mm_dibosonNoOutliers_smoothDY_smoothTTbar:GeV"
data21["mm"]["backgroundTemplate_KF_REDCHOICE_NNPDF30"] = sharedInputs+"/rel21_templates-dataNormed/merged_mm_kFactorSyst_REDCHOICE_NNPDF30-dataNormed.root:mm_dibosonNoOutliers_smoothDY_smoothTTbar:GeV"
data21["mm"]["backgroundTemplate_KF_SCALE_Z__1down"]    = sharedInputs+"/rel21_templates-dataNormed/merged_mm_kFactorSyst_SCALE_Z__1down-dataNormed.root:mm_dibosonNoOutliers_smoothDY_smoothTTbar:GeV"
data21["mm"]["backgroundTemplate_KF_SCALE_Z__1up"]      = sharedInputs+"/rel21_templates-dataNormed/merged_mm_kFactorSyst_SCALE_Z__1up-dataNormed.root:mm_dibosonNoOutliers_smoothDY_smoothTTbar:GeV"

# blurry samples for cross check
data21["mm"]["m_uu_noSmear" ] = sharedInputs+"/rel21_smearedPtMassTemplates/combined.root:m_uu"
data21["mm"]["m_uu_smear00" ] = sharedInputs+"/rel21_smearedPtMassTemplates/combined.root:m_uu_smear00"
data21["mm"]["m_uu_smear01" ] = sharedInputs+"/rel21_smearedPtMassTemplates/combined.root:m_uu_smear01"
data21["mm"]["m_uu_smear05" ] = sharedInputs+"/rel21_smearedPtMassTemplates/combined.root:m_uu_smear05"
data21["mm"]["m_uu_smear10" ] = sharedInputs+"/rel21_smearedPtMassTemplates/combined.root:m_uu_smear10"
data21["mm"]["m_uu_smear15" ] = sharedInputs+"/rel21_smearedPtMassTemplates/combined.root:m_uu_smear15"
data21["mm"]["m_uu_smear20" ] = sharedInputs+"/rel21_smearedPtMassTemplates/combined.root:m_uu_smear20"
data21["mm"]["m_uu_smear25" ] = sharedInputs+"/rel21_smearedPtMassTemplates/combined.root:m_uu_smear25"
data21["mm"]["m_uu_smear30" ] = sharedInputs+"/rel21_smearedPtMassTemplates/combined.root:m_uu_smear30"
data21["mm"]["m_uu_smear35" ] = sharedInputs+"/rel21_smearedPtMassTemplates/combined.root:m_uu_smear35"
data21["mm"]["m_uu_smear40" ] = sharedInputs+"/rel21_smearedPtMassTemplates/combined.root:m_uu_smear40"
data21["mm"]["m_uu_smear45" ] = sharedInputs+"/rel21_smearedPtMassTemplates/combined.root:m_uu_smear45"
data21["mm"]["m_uu_smear50" ] = sharedInputs+"/rel21_smearedPtMassTemplates/combined.root:m_uu_smear50"
data21["mm"]["m_uu_smear90" ] = sharedInputs+"/rel21_smearedPtMassTemplates/combined.root:m_uu_smear90"
data21["mm"]["m_uu_smear200" ] = sharedInputs+"/rel21_smearedPtMassTemplates/combined.root:m_uu_smear200"
data21["mm"]["m_uu_smear1000" ] = sharedInputs+"/rel21_smearedPtMassTemplates/combined.root:m_uu_smear1000"

# blurry samples for cross check
pSmearPath = "/home/prime/dilepton/metCrossCheck/combined-data.root"
data21["mm"]["m_uu_psmear00" ] = pSmearPath+":m_uu_psmear00"
data21["mm"]["m_uu_psmear01" ] = pSmearPath+":m_uu_psmear01"
data21["mm"]["m_uu_psmear05" ] = pSmearPath+":m_uu_psmear05"
data21["mm"]["m_uu_psmear10" ] = pSmearPath+":m_uu_psmear10"
data21["mm"]["m_uu_psmear15" ] = pSmearPath+":m_uu_psmear15"
data21["mm"]["m_uu_psmear20" ] = pSmearPath+":m_uu_psmear20"
data21["mm"]["m_uu_psmear25" ] = pSmearPath+":m_uu_psmear25"
data21["mm"]["m_uu_psmear30" ] = pSmearPath+":m_uu_psmear30"
data21["mm"]["m_uu_psmear35" ] = pSmearPath+":m_uu_psmear35"
data21["mm"]["m_uu_psmear40" ] = pSmearPath+":m_uu_psmear40"
data21["mm"]["m_uu_psmear45" ] = pSmearPath+":m_uu_psmear45"
data21["mm"]["m_uu_psmear50" ] = pSmearPath+":m_uu_psmear50"
data21["mm"]["m_uu_psmear90" ] = pSmearPath+":m_uu_psmear90"
data21["mm"]["m_uu_psmear200" ] = pSmearPath+":m_uu_psmear200"
data21["mm"]["m_uu_psmear300" ] = pSmearPath+":m_uu_psmear300"
data21["mm"]["m_uu_psmear400" ] = pSmearPath+":m_uu_psmear400"
data21["mm"]["m_uu_psmear500" ] = pSmearPath+":m_uu_psmear500"
data21["mm"]["m_uu_psmear600" ] = pSmearPath+":m_uu_psmear600"
data21["mm"]["m_uu_psmear1000" ] = pSmearPath+":m_uu_psmear1000"

# ee
# force params negative
# data21["ee"]["backgroundModel"]                        = "EXPR::backgroundModel('(0.0024952/(pow(0.0911876-x,2)+pow(0.0024952,2)))*pow(1-x/13,diphotonLeadingCoef)*pow(x/13,p0+p1*log(x/13)+p2*log(x/13)^2+p3*log(x/13)^3)',x,diphotonLeadingCoef[6.8805],p0[-1.0870e+01,-30.,0.],p1[-4.1732e+00,-10.,0],p2[-9.5372e-01,-5.,0],p3[-8.9194e-02,-1.,0])"
# fix LCF
# data21["ee"]["backgroundModel"]                        = "EXPR::backgroundModel('(0.0024952/(pow(0.0911876-x,2)+pow(0.0024952,2)))*pow(1-x/13,diphotonLeadingCoef)*pow(x/13,p0+p1*log(x/13)+p2*log(x/13)^2+p3*log(x/13)^3)',x,diphotonLeadingCoef[6.8805],p0[-1.0870e+01,-30.,10.],p1[-4.1732e+00,-10.,5.],p2[-9.5372e-01,-5.,1.],p3[-8.9194e-02,-1.,0.1])"
# float LCF
# data21["ee"]["backgroundModel"]                        = "EXPR::backgroundModel('(0.0024952/(pow(0.0911876-x,2)+pow(0.0024952,2)))*pow(1-x/13,diphotonLeadingCoef)*pow(x/13,p0+p1*log(x/13)+p2*log(x/13)^2+p3*log(x/13)^3)',x,diphotonLeadingCoef[6.8805,0.0001,30],p0[-1.0870e+01,-30.,10.],p1[-4.1732e+00,-10.,5.],p2[-9.5372e-01,-5.,1.],p3[-8.9194e-02,-1.,0.1])"
# copy Peter's signal model, parameters from new CP recs
# data21["ee"]["backgroundModel"]                        = "EXPR::backgroundModel('(2.4952/(pow(91.1876-(x/1000),2)+pow(2.4952,2)))*pow(1-pow((x/1000)/13.0,0.5),diphotonLeadingCoef)*pow((x/1000)/13.0,p0+p1*log((x/1000)/13.0)+p2*log((x/1000)/13.0)^2+p3*log((x/1000)/13.0)^3)',x,diphotonLeadingCoef[30,1,60],p0[-11.492,-30,0],p1[-4.2473,-10,0],p2[-0.92928,-5,0],p3[-0.085168,-1,0])"
# updated after failed N-Bkg comparison test for LL, RR
# use for MC
# data21["ee"]["backgroundModel"]                         = "EXPR::backgroundModel('(2.4952/(pow(91.1876-(x/1000),2)+pow(2.4952,2)))*pow(1-pow((x/1000)/13.0,0.5),diphotonLeadingCoef)*pow((x/1000)/13.0,p0+p1*log((x/1000)/13.0)+p2*log((x/1000)/13.0)^2+p3*log((x/1000)/13.0)^3)',x,diphotonLeadingCoef[7,1,60],p0[-11.492,-30,0],p1[-4.2473,-10,0],p2[-0.92928,-5,0],p3[-0.085168,-1,0])"
# try optimize diphotonLeadingCoef for Data (not MC)
# data21["ee"]["backgroundModel"]                         = "EXPR::backgroundModel('(2.4952/(pow(91.1876-(x/1000),2)+pow(2.4952,2)))*pow(1-pow((x/1000)/13.0,0.5),diphotonLeadingCoef)*pow((x/1000)/13.0,p0+p1*log((x/1000)/13.0)+p2*log((x/1000)/13.0)^2+p3*log((x/1000)/13.0)^3)',x,diphotonLeadingCoef[24,1,40],p0[-11.492,-30,0],p1[-4.2473,-10,0],p2[-0.92928,-5,0],p3[-0.085168,-1,0])"
# looks a bit better of for signal inject on MC
data21["ee"]["backgroundModel"]                         = "EXPR::backgroundModel('(2.4952/(pow(91.1876-(x/1000),2)+pow(2.4952,2)))*pow(1-pow((x/1000)/13.0,0.5),diphotonLeadingCoef)*pow((x/1000)/13.0,p0+p1*log((x/1000)/13.0)+p2*log((x/1000)/13.0)^2+p3*log((x/1000)/13.0)^3)',x,diphotonLeadingCoef[5,1,60],p0[-11.492,-30,0],p1[-4.2473,-10,0],p2[-0.92928,-5,0],p3[-0.085168,-1,0])"
# tune to data fit
data21["ee"]["backgroundModel"]                         = "EXPR::backgroundModel('(2.4952/(pow(91.1876-(x/1000),2)+pow(2.4952,2)))*pow(1-pow((x/1000)/13.0,0.5),diphotonLeadingCoef)*pow((x/1000)/13.0,p0+p1*log((x/1000)/13.0)+p2*log((x/1000)/13.0)^2+p3*log((x/1000)/13.0)^3)',x,diphotonLeadingCoef[6,1,60],p0[-11.492,-30,0],p1[-4.2473,-10,0],p2[-0.92928,-5,0],p3[-0.085168,-1,0])"
# data21["ee"]["backgroundModel"]                         = "EXPR::backgroundModel('(2.4952/(pow(91.1876-(x/1000),2)+pow(2.4952,2)))*pow(1-pow((x/1000)/13.0,0.5),diphotonLeadingCoef)*pow((x/1000)/13.0,p0+p1*log((x/1000)/13.0)+p2*log((x/1000)/13.0)^2+p3*log((x/1000)/13.0)^3)',x,diphotonLeadingCoef[5,1,60],p0[-4.3,-30,0],p1[-4.7,-10,0],p2[-1.560,-5,0],p3[-0.157,-1,0])"
# data21["ee"]["backgroundModel"]                         = "EXPR::backgroundModel('(2.4952/(pow(91.1876-(x/1000),2)+pow(2.4952,2)))*pow(1-pow((x/1000)/13.0,0.5),diphotonLeadingCoef)*pow((x/1000)/13.0,p0+p1*log((x/1000)/13.0)+p2*log((x/1000)/13.0)^2+p3*log((x/1000)/13.0)^3)',x,diphotonLeadingCoef[26],p0[-4.3,-30,0],p1[-4.7,-10,0],p2[-1.560,-5,0],p3[-0.157,-1,0])"
data21["ee"]["data"]                                    = sharedInputs+"/rel21_dataTemplates/mInv_spectrum_combined_ll_rel21_fullRun2.root:mInv_spectrum_201518_ee_1GeV:GeV"
data21["ee"]["dataLog"]                                 = sharedInputs+"/rel21_dataTemplates/mInv_spectrum_combined_ll_rel21_logBins_new.root:mInv_spectrum_201518_ee_1GeV"
data21["ee"]["sigLog-const-12"]                         = sharedInputs+"/rel21_ci/templates_r21_ll-oct-logbin/ee/CI_Template_LL_log.root:diff_CI_LL_const_12_TeV"
data21["ee"]["sigLog-const-20"]                         = sharedInputs+"/rel21_ci/templates_r21_ll-oct-logbin/ee/CI_Template_LL_log.root:diff_CI_LL_const_20_TeV"
data21["ee"]["sigLog-const-24"]                         = sharedInputs+"/rel21_ci/templates_r21_ll-oct-logbin/ee/CI_Template_LL_log.root:diff_CI_LL_const_24_TeV"
data21["ee"]["sigLog-const-28"]                         = sharedInputs+"/rel21_ci/templates_r21_ll-oct-logbin/ee/CI_Template_LL_log.root:diff_CI_LL_const_28_TeV"
data21["ee"]["sigLog-const-30"]                         = sharedInputs+"/rel21_ci/templates_r21_ll-oct-logbin/ee/CI_Template_LL_log.root:diff_CI_LL_const_30_TeV"
data21["ee"]["sigLog-const-34"]                         = sharedInputs+"/rel21_ci/templates_r21_ll-oct-logbin/ee/CI_Template_LL_log.root:diff_CI_LL_const_34_TeV"
data21["ee"]["sigLog-const-36"]                         = sharedInputs+"/rel21_ci/templates_r21_ll-oct-logbin/ee/CI_Template_LL_log.root:diff_CI_LL_const_36_TeV"
data21["ee"]["sigLog-const-40"]                         = sharedInputs+"/rel21_ci/templates_r21_ll-oct-logbin/ee/CI_Template_LL_log.root:diff_CI_LL_const_40_TeV"
data21["ee"]["sigLog-const-58"]                         = sharedInputs+"/rel21_ci/templates_r21_ll-oct-logbin/ee/CI_Template_LL_log.root:diff_CI_LL_const_58_TeV"
data21["ee"]["sigLog-dest-12"]                         = sharedInputs+"/rel21_ci/templates_r21_ll-oct-logbin/ee/CI_Template_LL_log.root:diff_CI_LL_dest_12_TeV"
data21["ee"]["sigLog-dest-16"]                         = sharedInputs+"/rel21_ci/templates_r21_ll-oct-logbin/ee/CI_Template_LL_log.root:diff_CI_LL_dest_16_TeV"
data21["ee"]["sigLog-dest-20"]                         = sharedInputs+"/rel21_ci/templates_r21_ll-oct-logbin/ee/CI_Template_LL_log.root:diff_CI_LL_dest_20_TeV"
data21["ee"]["sigLog-dest-24"]                         = sharedInputs+"/rel21_ci/templates_r21_ll-oct-logbin/ee/CI_Template_LL_log.root:diff_CI_LL_dest_24_TeV"
data21["ee"]["sigLog-dest-26"]                         = sharedInputs+"/rel21_ci/templates_r21_ll-oct-logbin/ee/CI_Template_LL_log.root:diff_CI_LL_dest_26_TeV"
data21["ee"]["sigLog-dest-30"]                         = sharedInputs+"/rel21_ci/templates_r21_ll-oct-logbin/ee/CI_Template_LL_log.root:diff_CI_LL_dest_30_TeV"
data21["ee"]["sigLog-dest-40"]                         = sharedInputs+"/rel21_ci/templates_r21_ll-oct-logbin/ee/CI_Template_LL_log.root:diff_CI_LL_dest_40_TeV"
data21["ee"]["backgroundTemplate"]               = sharedInputs+"/rel21_templates-dataNormed/merged_ee-dataNormed.root:ee_dibosonNoOutliers_201516fakes_smoothDY_smoothTTbar:GeV"
data21["ee"]["backgroundTemplateBumpy"]                      = sharedInputs+"/rel21_templates/merged_ee.root:ee_dibosonNoOutliers_201516fakes_nonSmoothDY_nonSmoothNonAllHadTTbar:GeV"
data21["ee"]["backgroundTemplate_KF"]                   = sharedInputs+"/rel21_templates-dataNormed/merged_ee_kFactorSyst_-dataNormed.root:ee_dibosonNoOutliers_201516fakes_smoothDY_smoothTTbar:GeV"
data21["ee"]["backgroundTemplate_KF_ALPHAS__1down"]     = sharedInputs+"/rel21_templates-dataNormed/merged_ee_kFactorSyst_ALPHAS__1down-dataNormed.root:ee_dibosonNoOutliers_201516fakes_smoothDY_smoothTTbar:GeV"
data21["ee"]["backgroundTemplate_KF_ALPHAS__1up"]       = sharedInputs+"/rel21_templates-dataNormed/merged_ee_kFactorSyst_ALPHAS__1up-dataNormed.root:ee_dibosonNoOutliers_201516fakes_smoothDY_smoothTTbar:GeV"
data21["ee"]["backgroundTemplate_KF_CHOICE_HERAPDF20"]  = sharedInputs+"/rel21_templates-dataNormed/merged_ee_kFactorSyst_CHOICE_HERAPDF20-dataNormed.root:ee_dibosonNoOutliers_201516fakes_smoothDY_smoothTTbar:GeV"
data21["ee"]["backgroundTemplate_KF_CHOICE_NNPDF30"]    = sharedInputs+"/rel21_templates-dataNormed/merged_ee_kFactorSyst_CHOICE_NNPDF30-dataNormed.root:ee_dibosonNoOutliers_201516fakes_smoothDY_smoothTTbar:GeV"
data21["ee"]["backgroundTemplate_KF_PDF_EV1"]           = sharedInputs+"/rel21_templates-dataNormed/merged_ee_kFactorSyst_PDF_EV1-dataNormed.root:ee_dibosonNoOutliers_201516fakes_smoothDY_smoothTTbar:GeV"
data21["ee"]["backgroundTemplate_KF_PDF_EV2"]           = sharedInputs+"/rel21_templates-dataNormed/merged_ee_kFactorSyst_PDF_EV2-dataNormed.root:ee_dibosonNoOutliers_201516fakes_smoothDY_smoothTTbar:GeV"
data21["ee"]["backgroundTemplate_KF_PDF_EV3"]           = sharedInputs+"/rel21_templates-dataNormed/merged_ee_kFactorSyst_PDF_EV3-dataNormed.root:ee_dibosonNoOutliers_201516fakes_smoothDY_smoothTTbar:GeV"
data21["ee"]["backgroundTemplate_KF_PDF_EV4"]           = sharedInputs+"/rel21_templates-dataNormed/merged_ee_kFactorSyst_PDF_EV4-dataNormed.root:ee_dibosonNoOutliers_201516fakes_smoothDY_smoothTTbar:GeV"
data21["ee"]["backgroundTemplate_KF_PDF_EV5"]           = sharedInputs+"/rel21_templates-dataNormed/merged_ee_kFactorSyst_PDF_EV5-dataNormed.root:ee_dibosonNoOutliers_201516fakes_smoothDY_smoothTTbar:GeV"
data21["ee"]["backgroundTemplate_KF_PDF_EV6"]           = sharedInputs+"/rel21_templates-dataNormed/merged_ee_kFactorSyst_PDF_EV6-dataNormed.root:ee_dibosonNoOutliers_201516fakes_smoothDY_smoothTTbar:GeV"
data21["ee"]["backgroundTemplate_KF_PDF_EV7"]           = sharedInputs+"/rel21_templates-dataNormed/merged_ee_kFactorSyst_PDF_EV7-dataNormed.root:ee_dibosonNoOutliers_201516fakes_smoothDY_smoothTTbar:GeV"
data21["ee"]["backgroundTemplate_KF_PDF_EW__1down"]     = sharedInputs+"/rel21_templates-dataNormed/merged_ee_kFactorSyst_PDF_EW__1down-dataNormed.root:ee_dibosonNoOutliers_201516fakes_smoothDY_smoothTTbar:GeV"
data21["ee"]["backgroundTemplate_KF_PDF_EW__1up"]       = sharedInputs+"/rel21_templates-dataNormed/merged_ee_kFactorSyst_PDF_EW__1up-dataNormed.root:ee_dibosonNoOutliers_201516fakes_smoothDY_smoothTTbar:GeV"
data21["ee"]["backgroundTemplate_KF_PDF__1down"]        = sharedInputs+"/rel21_templates-dataNormed/merged_ee_kFactorSyst_PDF__1down-dataNormed.root:ee_dibosonNoOutliers_201516fakes_smoothDY_smoothTTbar:GeV"
data21["ee"]["backgroundTemplate_KF_PDF__1up"]          = sharedInputs+"/rel21_templates-dataNormed/merged_ee_kFactorSyst_PDF__1up-dataNormed.root:ee_dibosonNoOutliers_201516fakes_smoothDY_smoothTTbar:GeV"
data21["ee"]["backgroundTemplate_KF_PI__1down"]         = sharedInputs+"/rel21_templates-dataNormed/merged_ee_kFactorSyst_PI__1down-dataNormed.root:ee_dibosonNoOutliers_201516fakes_smoothDY_smoothTTbar:GeV"
data21["ee"]["backgroundTemplate_KF_PI__1up"]           = sharedInputs+"/rel21_templates-dataNormed/merged_ee_kFactorSyst_PI__1up-dataNormed.root:ee_dibosonNoOutliers_201516fakes_smoothDY_smoothTTbar:GeV"
data21["ee"]["backgroundTemplate_KF_REDCHOICE_NNPDF30"] = sharedInputs+"/rel21_templates-dataNormed/merged_ee_kFactorSyst_REDCHOICE_NNPDF30-dataNormed.root:ee_dibosonNoOutliers_201516fakes_smoothDY_smoothTTbar:GeV"
data21["ee"]["backgroundTemplate_KF_SCALE_Z__1down"]    = sharedInputs+"/rel21_templates-dataNormed/merged_ee_kFactorSyst_SCALE_Z__1down-dataNormed.root:ee_dibosonNoOutliers_201516fakes_smoothDY_smoothTTbar:GeV"
data21["ee"]["backgroundTemplate_KF_SCALE_Z__1up"]      = sharedInputs+"/rel21_templates-dataNormed/merged_ee_kFactorSyst_SCALE_Z__1up-dataNormed.root:ee_dibosonNoOutliers_201516fakes_smoothDY_smoothTTbar:GeV"

data21["ee"]["eeOnly_newFakesTemplate"]            = sharedInputs+"/rel21_fakes/fakes.root:ee_dibosonNoOutliers_smoothDY_smoothTTbar_seanFakes:GeV"
data21["ee"]["eeOnly_newFakesTemplate2x"]            = sharedInputs+"/rel21_fakes/fakes.root:ee_dibosonNoOutliers_smoothDY_smoothTTbar_seanFakes_2x:GeV"
data21["ee"]["eeOnly_newFakesTemplate3x"]            = sharedInputs+"/rel21_fakes/fakes.root:ee_dibosonNoOutliers_smoothDY_smoothTTbar_seanFakes_3x:GeV"
data21["ee"]["eeOnly_newFakesTemplate1p5x"]            = sharedInputs+"/rel21_fakes/fakes.root:ee_dibosonNoOutliers_smoothDY_smoothTTbar_seanFakes_1p5x:GeV"
data21["ee"]["eeOnly_fakeslessTemplate"]           = sharedInputs+"/rel21_templates/merged_ee.root:ee_dibosonNoOutliers_smoothDY_smoothTTbar:GeV"

# try to get one particular fit to work
# data21["ee"]["backgroundModel"] = "EXPR::backgroundModel('(2.4952/(pow(91.1876-(x/1000),2)+pow(2.4952,2)))*pow(1-pow((x/1000)/13.0,0.5),diphotonLeadingCoef)*pow((x/1000)/13.0,p0+p1*log((x/1000)/13.0)+p2*log((x/1000)/13.0)^2+p3*log((x/1000)/13.0)^3)',x,diphotonLeadingCoef[24,1,60], p0[-11.492,-30,0],p1[-4.2473,-10,0],p2[-0.92928,-5,0],p3[-0.085168,-1,0])"
# data21["ee"]["backgroundModel"] = "EXPR::backgroundModel('(2.4952/(pow(91.1876-(x/1000),2)+pow(2.4952,2)))*pow(1-pow((x/1000)/13.0,0.5),diphotonLeadingCoef)*pow((x/1000)/13.0,p0+p1*log((x/1000)/13.0)+p2*log((x/1000)/13.0)^2+p3*log((x/1000)/13.0)^3)',x,diphotonLeadingCoef[4], p0[-11.492,-30,0],p1[-4.2473,-10,0],p2[-0.92928,-5,0],p3[-0.085168,-1,0])"
# data21["ee"]["backgroundModel"] = "EXPR::backgroundModel('(2.4952/(pow(91.1876-(x/1000),2)+pow(2.4952,2)))*pow(1-pow((x/1000)/13.0,0.5),diphotonLeadingCoef)*pow((x/1000)/13.0,p0+p1*log((x/1000)/13.0)+p2*log((x/1000)/13.0)^2+p3*log((x/1000)/13.0)^3)',x,diphotonLeadingCoef[24,1,40],p0[-11.492,-30,0],p1[-4.2473,-10,0],p2[-0.92928,-5,0],p3[-0.085168,-1,0])"

################################################################################
# Previous analysis
################################################################################
prevSharedInputs = "/home/prime/dilepton/SharedInputsProj/prevRound_templates/"
dataPrev = {"ee":{},"mm":{}}
dataPrev["ee"]["backgroundModel"]                      = "EXPR::backgroundModel('(0.0024952/(pow(0.0911876-x,2)+pow(0.0024952,2)))*pow(1-x/13,diphotonLeadingCoef)*pow(x/13,p0+p1*log(x/13)+p2*log(x/13)^2+p3*log(x/13)^3)',x,diphotonLeadingCoef[6.8805,0.0001,30],p0[-1.0870e+01,-30.,10.],p1[-4.1732e+00,-10.,5.],p2[-9.5372e-01,-5.,1.],p3[-8.9194e-02,-1.,0.1])"
dataPrev["ee"]["backgroundTemplate"]                   = prevSharedInputs+"/DataBackground_EE_13TeV_36p5fb_Search.root:DY_Mass_Template:TeV:rebin1"
dataPrev["ee"]["backgroundTemplate_KF_PDFVar1"]        = prevSharedInputs+"/Sys_EE_13TeV.root:backgroundTemplate_KF_PDFVar1:TeV:rebin1"
dataPrev["ee"]["backgroundTemplate_KF_PDFVar2"]        = prevSharedInputs+"/Sys_EE_13TeV.root:backgroundTemplate_KF_PDFVar2:TeV:rebin1"
dataPrev["ee"]["backgroundTemplate_KF_PDFVar3"]        = prevSharedInputs+"/Sys_EE_13TeV.root:backgroundTemplate_KF_PDFVar3:TeV:rebin1"
dataPrev["ee"]["backgroundTemplate_KF_PDFVar4"]        = prevSharedInputs+"/Sys_EE_13TeV.root:backgroundTemplate_KF_PDFVar4:TeV:rebin1"
dataPrev["ee"]["backgroundTemplate_KF_PDFVar5"]        = prevSharedInputs+"/Sys_EE_13TeV.root:backgroundTemplate_KF_PDFVar5:TeV:rebin1"
dataPrev["ee"]["backgroundTemplate_KF_PDFVar6"]        = prevSharedInputs+"/Sys_EE_13TeV.root:backgroundTemplate_KF_PDFVar6:TeV:rebin1"
dataPrev["ee"]["backgroundTemplate_KF_PDFVar7"]        = prevSharedInputs+"/Sys_EE_13TeV.root:backgroundTemplate_KF_PDFVar7:TeV:rebin1"
dataPrev["ee"]["backgroundTemplate_KF_PDFChoice"]      = prevSharedInputs+"/Sys_EE_13TeV.root:backgroundTemplate_KF_PDFChoice:TeV:rebin1"
dataPrev["ee"]["backgroundTemplate_KF_PDFScale"]       = prevSharedInputs+"/Sys_EE_13TeV.root:backgroundTemplate_KF_PDFScale:TeV:rebin1"
dataPrev["ee"]["backgroundTemplate_KF_PDFAlphaS"]      = prevSharedInputs+"/Sys_EE_13TeV.root:backgroundTemplate_KF_PDFAlphaS:TeV:rebin1"
dataPrev["ee"]["backgroundTemplate_KF_THPI"]           = prevSharedInputs+"/Sys_EE_13TeV.root:backgroundTemplate_KF_THPI:TeV:rebin1"
dataPrev["ee"]["backgroundTemplate_KF_THEW"]           = prevSharedInputs+"/Sys_EE_13TeV.root:backgroundTemplate_KF_THEW:TeV:rebin1"
dataPrev["ee"]["backgroundTemplate_KF_PRW"]            = prevSharedInputs+"/Sys_EE_13TeV.root:backgroundTemplate_KF_PRW:TeV:rebin1"
dataPrev["ee"]["backgroundTemplate_KF_EFFReco"]        = prevSharedInputs+"/Sys_EE_13TeV.root:backgroundTemplate_KF_EFFReco:TeV:rebin1"
dataPrev["ee"]["backgroundTemplate_KF_EFFIso"]         = prevSharedInputs+"/Sys_EE_13TeV.root:backgroundTemplate_KF_EFFIso:TeV:rebin1"
dataPrev["ee"]["backgroundTemplate_KF_EFFTrig"]        = prevSharedInputs+"/Sys_EE_13TeV.root:backgroundTemplate_KF_EFFTrig:TeV:rebin1"
dataPrev["ee"]["backgroundTemplate_KF_EFFTotal"]       = prevSharedInputs+"/Sys_EE_13TeV.root:backgroundTemplate_KF_EFFTotal:TeV:rebin1"
dataPrev["ee"]["backgroundTemplate_KF_EXPScale"]       = prevSharedInputs+"/Sys_EE_13TeV.root:backgroundTemplate_KF_EXPScale:TeV:rebin1"
dataPrev["ee"]["backgroundTemplate_KF_BE"]             = prevSharedInputs+"/Sys_EE_13TeV.root:backgroundTemplate_KF_BE:TeV:rebin1"
dataPrev["ee"]["backgroundTemplate_KF_THTT"]           = prevSharedInputs+"/Sys_EE_13TeV.root:backgroundTemplate_KF_THTT:TeV:rebin1"
dataPrev["ee"]["backgroundTemplate_KF_THDB"]           = prevSharedInputs+"/Sys_EE_13TeV.root:backgroundTemplate_KF_THDB:TeV:rebin1"
dataPrev["ee"]["backgroundTemplate_KF_QCD"]            = prevSharedInputs+"/Sys_EE_13TeV.root:backgroundTemplate_KF_QCD:TeV:rebin1"
dataPrev["ee"]["backgroundTemplate_KF_EFFID"]          = prevSharedInputs+"/Sys_EE_13TeV.root:backgroundTemplate_KF_EFFID:TeV:rebin1"
dataPrev["ee"]["backgroundTemplate_KF_EXPRes"]         = prevSharedInputs+"/Sys_EE_13TeV.root:backgroundTemplate_KF_EXPRes:TeV:rebin1"

data = {"21":data21,"20":data20,"prev":dataPrev}

################################################################################
# Dictionay of lists of PDF names to consider
################################################################################
# Structure: data[release] = list
# where release = 20, 21, prev

# rel20, rel21:
currentPdfNames = [
            "backgroundTemplate",
            # fakes templates from sean
            "eeOnly_newFakesTemplate",
            "eeOnly_newFakesTemplate1p5x",
            "eeOnly_newFakesTemplate2x",
            "eeOnly_newFakesTemplate3x", # sure, just go crazy
            "eeOnly_fakeslessTemplate",
            # /fakes templates from sean
            "backgroundTemplate_KF",
            "backgroundTemplate_KF_ALPHAS__1down",
            "backgroundTemplate_KF_ALPHAS__1up",
            "backgroundTemplate_KF_CHOICE_HERAPDF20",
            "backgroundTemplate_KF_CHOICE_NNPDF30",
            "backgroundTemplate_KF_PDF_EV7",
            "backgroundTemplate_KF_PDF_EV6",
            "backgroundTemplate_KF_PDF_EV5",
            "backgroundTemplate_KF_PDF_EV4",
            "backgroundTemplate_KF_PDF_EV3",
            "backgroundTemplate_KF_PDF_EV2",
            "backgroundTemplate_KF_PDF_EV1",
            "backgroundTemplate_KF_PDF_EW__1down",
            "backgroundTemplate_KF_PDF_EW__1up",
            "backgroundTemplate_KF_PDF__1down",
            "backgroundTemplate_KF_PDF__1up",
            "backgroundTemplate_KF_PI__1down",
            "backgroundTemplate_KF_PI__1up",
            "backgroundTemplate_KF_REDCHOICE_NNPDF30",
            "backgroundTemplate_KF_SCALE_Z__1down",
            "backgroundTemplate_KF_SCALE_Z__1up",
           ]
# previous analysis
prevPdfNames = [
            "backgroundTemplate_KF_PDFVar1",
            "backgroundTemplate_KF_PDFVar2",
            "backgroundTemplate_KF_PDFVar3",
            "backgroundTemplate_KF_PDFVar4",
            "backgroundTemplate_KF_PDFVar5",
            "backgroundTemplate_KF_PDFVar6",
            "backgroundTemplate_KF_PDFVar7",
            "backgroundTemplate_KF_PDFChoice",
            "backgroundTemplate_KF_PDFScale",
            "backgroundTemplate_KF_PDFAlphaS",
            "backgroundTemplate_KF_THPI",
            "backgroundTemplate_KF_THEW",
            "backgroundTemplate_KF_PRW",
            "backgroundTemplate_KF_EFFReco",
            "backgroundTemplate_KF_EFFIso",
            "backgroundTemplate_KF_EFFTrig",
            "backgroundTemplate_KF_EFFTotal",
            "backgroundTemplate_KF_EXPScale",
            "backgroundTemplate_KF_BE",
            "backgroundTemplate_KF_THTT",
            "backgroundTemplate_KF_THDB",
            "backgroundTemplate_KF_QCD",
            "backgroundTemplate_KF_EFFID",
            "backgroundTemplate_KF_EXPRes",
           ]

# select pdfnames to use based on "release
pdfNames = {"20":currentPdfNames,"21":currentPdfNames,"prev":prevPdfNames}

# names of pt smeared MC samples
currentSmearedNames = [
    # "data",
    "m_uu_noSmear",
    "m_uu_smear00",
    # "m_uu_smear01",
    # "m_uu_smear05",
    # "m_uu_smear10",
    # "m_uu_smear15",
    # "m_uu_smear20",
    # "m_uu_smear25",
    # "m_uu_smear30",
    # "m_uu_smear35",
    # "m_uu_smear40",
    # "m_uu_smear45",
    # "m_uu_smear50",
    # "m_uu_smear90",
    # "m_uu_smear200",
    # "m_uu_smear1000",
]

pSmearedNames = [
    # "data",
    # "m_uu_noSmear",
    "m_uu_psmear00",
    "m_uu_psmear01",
    "m_uu_psmear05",
    "m_uu_psmear10",
    "m_uu_psmear15",
    "m_uu_psmear20",
    "m_uu_psmear25",
    "m_uu_psmear30",
    "m_uu_psmear35",
    "m_uu_psmear40",
    "m_uu_psmear45",
    "m_uu_psmear50",
    "m_uu_psmear90",
    "m_uu_psmear200",
    "m_uu_psmear300",
    "m_uu_psmear400",
    "m_uu_psmear500",
    "m_uu_psmear600",
    # "m_uu_psmear1000",
]

smearedNames = {"21":currentSmearedNames,"pSmear":pSmearedNames}

prevLimits  = [ \
    {"channel":"ee","chirality":"LL","interference":"dest", "prevLimExp":19.9},
    {"channel":"ee","chirality":"LR","interference":"dest", "prevLimExp":21.2},
    {"channel":"ee","chirality":"RL","interference":"dest", "prevLimExp":21.2},
    {"channel":"ee","chirality":"RR","interference":"dest", "prevLimExp":21.0},
    {"channel":"ee","chirality":"LL","interference":"const","prevLimExp":25.7},
    {"channel":"ee","chirality":"LR","interference":"const","prevLimExp":23.9},
    {"channel":"ee","chirality":"RL","interference":"const","prevLimExp":23.8},
    {"channel":"ee","chirality":"RR","interference":"const","prevLimExp":25.7},
    {"channel":"mm","chirality":"LL","interference":"const","prevLimExp":23.8},
    {"channel":"mm","chirality":"LR","interference":"const","prevLimExp":22.5},
    {"channel":"mm","chirality":"RL","interference":"const","prevLimExp":22.4},
    {"channel":"mm","chirality":"RR","interference":"const","prevLimExp":22.2},
    {"channel":"mm","chirality":"LL","interference":"dest", "prevLimExp":18.4},
    {"channel":"mm","chirality":"LR","interference":"dest", "prevLimExp":19.7},
    {"channel":"mm","chirality":"RL","interference":"dest", "prevLimExp":19.8},
    {"channel":"mm","chirality":"RR","interference":"dest", "prevLimExp":18.4},
]


# print smearedNames; quit()

##################################################
# Mass ranges
##################################################
# ranges used for first EB meeting
# version v1.3
ranges = {"ee":{},"mm":{}}
ranges["ee"]["const"]       = {}
ranges["ee"]["const"]["LL"] = {"fitMin":320,"fitMax":2070,"extrapMin":2070}
ranges["ee"]["const"]["LR"] = {"fitMin":330,"fitMax":2080,"extrapMin":2080}
ranges["ee"]["const"]["RL"] = {"fitMin":300,"fitMax":2070,"extrapMin":2070}
ranges["ee"]["const"]["RR"] = {"fitMin":320,"fitMax":2070,"extrapMin":2070}
# reasonable:
ranges["ee"]["dest"]        = {}
ranges["ee"]["dest"]["RL"]  = {"fitMin":310,"fitMax":1250,"extrapMin":2770}
ranges["ee"]["dest"]["LR"]  = {"fitMin":300,"fitMax":1250,"extrapMin":2770}
ranges["ee"]["dest"]["LL"]  = {"fitMin":320,"fitMax":1250,"extrapMin":2570}
ranges["ee"]["dest"]["RR"]  = {"fitMin":320,"fitMax":1250,"extrapMin":2570}
# ranges for muons
ranges["mm"]["const"]       = {}
ranges["mm"]["const"]["LL"] = {"fitMin":310,"fitMax":2070,"extrapMin":2070}
ranges["mm"]["const"]["LR"] = {"fitMin":310,"fitMax":2080,"extrapMin":2080}
ranges["mm"]["const"]["RL"] = {"fitMin":300,"fitMax":2070,"extrapMin":2070}
ranges["mm"]["const"]["RR"] = {"fitMin":310,"fitMax":2070,"extrapMin":2070}
# reasonable:
ranges["mm"]["dest"]        = {}
# ranges["mm"]["dest"]["RL"]  = {"fitMin":310,"fitMax":1250,"extrapMin":2770}
ranges["mm"]["dest"]["RL"]  = {"fitMin":270,"fitMax":1250,"extrapMin":2570}
ranges["mm"]["dest"]["LR"]  = {"fitMin":270,"fitMax":1250,"extrapMin":2570}
ranges["mm"]["dest"]["LL"]  = {"fitMin":320,"fitMax":1250,"extrapMin":2570}
ranges["mm"]["dest"]["RR"]  = {"fitMin":320,"fitMax":1250,"extrapMin":2570}



# Ranges for EB cross check with unified SR's
# Unified SR's chosen based on LL
# version v1.4
ranges = {"ee":{},"mm":{}}
ranges["ee"]["const"]       = {}
ranges["ee"]["const"]["LL"] = {"fitMin":320,"fitMax":2070,"extrapMin":2070}
ranges["ee"]["const"]["LR"] = {"fitMin":330,"fitMax":2070,"extrapMin":2070}
ranges["ee"]["const"]["RL"] = {"fitMin":300,"fitMax":2070,"extrapMin":2070}
ranges["ee"]["const"]["RR"] = {"fitMin":320,"fitMax":2070,"extrapMin":2070}
ranges["ee"]["dest"]        = {}
ranges["ee"]["dest"]["RL"]  = {"fitMin":310,"fitMax":1250,"extrapMin":2770}
ranges["ee"]["dest"]["LR"]  = {"fitMin":300,"fitMax":1250,"extrapMin":2770}
ranges["ee"]["dest"]["LL"]  = {"fitMin":320,"fitMax":1250,"extrapMin":2770}
ranges["ee"]["dest"]["RR"]  = {"fitMin":320,"fitMax":1250,"extrapMin":2770}
ranges["mm"]["const"]       = {}
ranges["mm"]["const"]["LL"] = {"fitMin":310,"fitMax":2070,"extrapMin":2070}
ranges["mm"]["const"]["LR"] = {"fitMin":310,"fitMax":2070,"extrapMin":2070}
ranges["mm"]["const"]["RL"] = {"fitMin":300,"fitMax":2070,"extrapMin":2070}
ranges["mm"]["const"]["RR"] = {"fitMin":310,"fitMax":2070,"extrapMin":2070}
ranges["mm"]["dest"]        = {}
ranges["mm"]["dest"]["RL"]  = {"fitMin":270,"fitMax":1250,"extrapMin":2570}
ranges["mm"]["dest"]["LR"]  = {"fitMin":270,"fitMax":1250,"extrapMin":2570}
ranges["mm"]["dest"]["LL"]  = {"fitMin":320,"fitMax":1250,"extrapMin":2570}
ranges["mm"]["dest"]["RR"]  = {"fitMin":320,"fitMax":1250,"extrapMin":2570}

# there was a problem with these, I fixed for LL:
# these have reasonable linearity and good limits!
ranges["ee"]["const"]["LL"] = {"fitMin":280,"fitMax":2200,"extrapMin":2200}
ranges["ee"]["const"]["LR"] = {"fitMin":280,"fitMax":2200,"extrapMin":2200}
ranges["ee"]["const"]["RL"] = {"fitMin":280,"fitMax":2200,"extrapMin":2200}
ranges["ee"]["const"]["RR"] = {"fitMin":280,"fitMax":2200,"extrapMin":2200}

# Setup v1.5
# there was a problem with these, I fixed for LL:
# these have reasonable linearity and good limits!
# used for reply to EB
# also, use update optimization
ranges["ee"]["const"]["LL"] = {"fitMin":280,"fitMax":2200,"extrapMin":2200}
ranges["ee"]["const"]["LR"] = {"fitMin":280,"fitMax":2200,"extrapMin":2200}
ranges["ee"]["const"]["RL"] = {"fitMin":280,"fitMax":2200,"extrapMin":2200}
ranges["ee"]["const"]["RR"] = {"fitMin":280,"fitMax":2200,"extrapMin":2200}
#
ranges["ee"]["dest"]["LL"]  = {"fitMin":310,"fitMax":1450,"extrapMin":2770}
ranges["ee"]["dest"]["LR"]  = {"fitMin":300,"fitMax":1450,"extrapMin":2770}
ranges["ee"]["dest"]["RL"]  = {"fitMin":320,"fitMax":1450,"extrapMin":2770}
ranges["ee"]["dest"]["RR"]  = {"fitMin":320,"fitMax":1450,"extrapMin":2770}
#
ranges["mm"]["const"]["LL"] = {"fitMin":310,"fitMax":2070,"extrapMin":2070}
ranges["mm"]["const"]["LR"] = {"fitMin":310,"fitMax":2070,"extrapMin":2070}
ranges["mm"]["const"]["RL"] = {"fitMin":300,"fitMax":2070,"extrapMin":2070}
ranges["mm"]["const"]["RR"] = {"fitMin":310,"fitMax":2070,"extrapMin":2070}
ranges["mm"]["dest"]["RL"]  = {"fitMin":270,"fitMax":1250,"extrapMin":2570}
ranges["mm"]["dest"]["LR"]  = {"fitMin":270,"fitMax":1250,"extrapMin":2570}
ranges["mm"]["dest"]["LL"]  = {"fitMin":320,"fitMax":1250,"extrapMin":2570}
ranges["mm"]["dest"]["RR"]  = {"fitMin":320,"fitMax":1250,"extrapMin":2570}

# # try modifying setup v1.6 adjustment, for linearity. Don't need to use for nominal b-only
# # this is after doing scan over fitMin
# ranges["ee"]["const"]["LL"] = {"fitMin":270,"fitMax":2200,"extrapMin":2200}
# ranges["ee"]["const"]["LR"] = {"fitMin":340,"fitMax":2200,"extrapMin":2200}
# ranges["ee"]["const"]["RL"] = {"fitMin":290,"fitMax":2200,"extrapMin":2200}
# ranges["ee"]["const"]["RR"] = {"fitMin":330,"fitMax":2200,"extrapMin":2200}
# ranges["ee"]["dest"]["LL"]  = {"fitMin":370,"fitMax":1450,"extrapMin":2770} # dest is finished
# ranges["ee"]["dest"]["LR"]  = {"fitMin":260,"fitMax":1450,"extrapMin":2770}
# ranges["ee"]["dest"]["RL"]  = {"fitMin":260,"fitMax":1450,"extrapMin":2770}
# ranges["ee"]["dest"]["RR"]  = {"fitMin":370,"fitMax":1450,"extrapMin":2770}

# # try modifying setup v1.7 adjustment, for linearity. Don't need to use for nominal b-only
# # this is after doing scan over fitMin
# ranges["ee"]["const"]["LL"] = {"fitMin":270,"fitMax":2000,"extrapMin":2200}
# ranges["ee"]["const"]["LR"] = {"fitMin":340,"fitMax":2000,"extrapMin":2200}
# ranges["ee"]["const"]["RL"] = {"fitMin":290,"fitMax":2000,"extrapMin":2200}
# ranges["ee"]["const"]["RR"] = {"fitMin":330,"fitMax":2000,"extrapMin":2200}

## based on previous def, but changing fitMin to match optimization
## const are good limis: linearity is iffy. return to previous
#ranges["ee"]["const"]["LL"] = {"fitMin":280,"fitMax":2200,"extrapMin":2200}
#ranges["ee"]["const"]["LR"] = {"fitMin":280,"fitMax":2200,"extrapMin":2200}
#ranges["ee"]["const"]["RL"] = {"fitMin":280,"fitMax":2200,"extrapMin":2200}
#ranges["ee"]["const"]["RR"] = {"fitMin":280,"fitMax":2200,"extrapMin":2200}
##
#ranges["ee"]["dest"]["LL"]  = {"fitMin":360,"fitMax":2780,"extrapMin":2780}
#ranges["ee"]["dest"]["LR"]  = {"fitMin":360,"fitMax":2780,"extrapMin":2780}
#ranges["ee"]["dest"]["RL"]  = {"fitMin":360,"fitMax":2780,"extrapMin":2780}
#ranges["ee"]["dest"]["RR"]  = {"fitMin":360,"fitMax":2780,"extrapMin":2780}


# # attempt to reduce SS
# ranges["ee"]["dest"]["LL"]  = {"fitMin":360,"fitMax":1250,"extrapMin":2780}
# ranges["ee"]["dest"]["LL"]  = {"fitMin":350,"fitMax":1250,"extrapMin":2780}
# ranges["ee"]["dest"]["LL"]  = {"fitMin":340,"fitMax":1250,"extrapMin":2780}
# ranges["ee"]["dest"]["LL"]  = {"fitMin":340,"fitMax":1050,"extrapMin":2780}
# ranges["ee"]["dest"]["LL"]  = {"fitMin":340,"fitMax":1050,"extrapMin":2500}
# ranges["ee"]["dest"]["LL"]  = {"fitMin":340,"fitMax":1050,"extrapMin":2500}
# ranges["ee"]["dest"]["LL"]  = {"fitMin":340,"fitMax":1250,"extrapMin":2500}
# # ranges["ee"]["dest"]["LL"]  = {"fitMin":340,"fitMax":1450,"extrapMin":2400}
# # ranges["ee"]["dest"]["LL"]  = {"fitMin":360,"fitMax":1850,"extrapMin":2400}
# # ranges["ee"]["dest"]["LL"]  = {"fitMin":360,"fitMax":1650,"extrapMin":2400}
# # ok for LL, RR except offset by 1. and bad limits
# ranges["ee"]["dest"]["LL"]  = {"fitMin":340,"fitMax":1250,"extrapMin":2500}
# ranges["ee"]["dest"]["RR"]  = {"fitMin":340,"fitMax":1250,"extrapMin":2500}


# ranges["ee"]["dest"]["RL"]  = {"fitMin":450,"fitMax":1000,"extrapMin":2200}



# # Setup v2.0
# ranges["ee"]["const"]["LL"] = {"fitMin":280,"fitMax":2000,"extrapMin":2000}
# ranges["ee"]["const"]["LR"] = {"fitMin":280,"fitMax":2000,"extrapMin":2000}
# ranges["ee"]["const"]["RL"] = {"fitMin":280,"fitMax":2000,"extrapMin":2000}
# ranges["ee"]["const"]["RR"] = {"fitMin":280,"fitMax":2000,"extrapMin":2000}

# setup v2.1
# used for S+B vs b-only plots
# unified SR
ranges["ee"]["const"]["LL"] = {"fitMin":300,"fitMax":2000,"extrapMin":2000}
ranges["ee"]["dest"]["LL"]  = {"fitMin":300,"fitMax":1450,"extrapMin":2770}
data21["ee"]["backgroundModel"] = "EXPR::backgroundModel('(2.4952/(pow(91.1876-(x/1000),2)+pow(2.4952,2)))*pow(1-pow((x/1000)/13.0,0.5),diphotonLeadingCoef)*pow((x/1000)/13.0,p0+p1*log((x/1000)/13.0)+p2*log((x/1000)/13.0)^2+p3*log((x/1000)/13.0)^3)',x,    diphotonLeadingCoef[6,1,60],p0[-11.492,-30,0],p1[-4.2473,-10,0],p2[-0.92928,-5,0],p3[-0.085168,-1,0])"
#
ranges["mm"]["dest"]["LL"]  = {"fitMin":300,"fitMax":1250,"extrapMin":2570}
ranges["mm"]["const"]["LL"] = {"fitMin":300,"fitMax":2070,"extrapMin":2070}
data21["mm"]["backgroundModel"] = "EXPR::backgroundModel('(2.4952/(pow(91.1876-(x/1000),2)+pow(2.4952,2)))*pow(1-pow((x/1000)/13.0,1.0/3.0),diphotonLeadingCoef)*pow((x/1000)/13.0,p0+p1*log((x/1000)/13.0)+p2*log((x/1000)/13.0)^2+p3*log((x/1000)/13.0)^3)',x,diphotonLeadingCoef[1.3],p0[-14.60,-30,0],p1[-4.43,-10,0],p2[-0.931,-5,0],p3[-0.079,-1,0])"

# look into errors when doing s+b vs b-only comparison with new ranges
# ranges["mm"]["const"]["LL"] = {"fitMin":350,"fitMax":2070,"extrapMin":2070}
# ranges["ee"]["dest"]["LL"]  = {"fitMin":310,"fitMax":1450,"extrapMin":2770}

# make test with full rnage S+B fit
# ranges["ee"]["const"]["LL"]  = {"fitMin":310,"fitMax":6000,"extrapMin":2000}
# ranges["ee"]["dest"]["LL"]  = {"fitMin":300,"fitMax":6000,"extrapMin":2770}

# for smearing tests
# ranges["mm"]["const"]["LL"] = {"fitMin":400,"fitMax":2070,"extrapMin":2070}

# setup v2.1
# actual ranges to use
# unified SR
ranges["ee"]["const"]["LL"] = {"fitMin":280,"fitMax":2200,"extrapMin":2200}
ranges["ee"]["dest"]["LL"]  = {"fitMin":310,"fitMax":1450,"extrapMin":2770}
data21["ee"]["backgroundModel"] = "EXPR::backgroundModel('(2.4952/(pow(91.1876-(x/1000),2)+pow(2.4952,2)))*pow(1-pow((x/1000)/13.0,0.5),diphotonLeadingCoef)*pow((x/1000)/13.0,p0+p1*log((x/1000)/13.0)+p2*log((x/1000)/13.0)^2+p3*log((x/1000)/13.0)^3)',x,    diphotonLeadingCoef[6,1,60],p0[-11.492,-30,0],p1[-4.2473,-10,0],p2[-0.92928,-5,0],p3[-0.085168,-1,0])"
#
ranges["mm"]["const"]["LL"] = {"fitMin":320,"fitMax":2070,"extrapMin":2070}
ranges["mm"]["dest"]["LL"]  = {"fitMin":310,"fitMax":1250,"extrapMin":2570}
data21["mm"]["backgroundModel"] = "EXPR::backgroundModel('(2.4952/(pow(91.1876-(x/1000),2)+pow(2.4952,2)))*pow(1-pow((x/1000)/13.0,1.0/3.0),diphotonLeadingCoef)*pow((x/1000)/13.0,p0+p1*log((x/1000)/13.0)+p2*log((x/1000)/13.0)^2+p3*log((x/1000)/13.0)^3)',x,diphotonLeadingCoef[1.3],p0[-14.60,-30,0],p1[-4.43,-10,0],p2[-0.931,-5,0],p3[-0.079,-1,0])"


# temporary for testing dest
# ranges["mm"]["dest"]["LL"]  = {"fitMin":310,"fitMax":1250,"extrapMin":2570}


