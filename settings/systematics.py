import numpy as np
nan = np.nan

systematics = {'ee': {'dest': {'pdfSs': 0.35109292152955573, 'nBkg': 2.739276263464075, 'fitSs': 1.6648776409637058}, 'const': {'pdfSs': 0.5663794697843461, 'nBkg': 12.256251267851603, 'fitSs': 2.4140785492352785}}, 'mm': {'dest': {'pdfSs': 0.18018779636002158, 'nBkg': 1.439142080594892, 'fitSs': 0.8383254161195787}, 'const': {'pdfSs': 0.4589671548155376, 'nBkg': 9.600583857884105, 'fitSs': 2.046340647998571}}}

lumiUncert = 0.017 # frac of total

nToys_toGenerate = 2000

nPdfs_toGenerate = 200

