################################################################################
# Tests how pystrap loads histogram and converts it to a RooDataHist
#
# What is checked
# 1) Histogram loading
# 2) Conversion to RooDataHist: bin contents, number of bins
#
# Exits 0 if no problem
# Exits 1 if failing a test
################################################################################

import sys
sys.path = ["../source"] + sys.path
from setup import *

release   = "21"
channel   = "ee"
# set up mass range so RDH is not trimmed
fitMin    = 0
fitMax    = 2
extrapMin = 2
extrapMax = 10

default = {\
           "signalInjectScale":0,
           "fitMin":fitMin,"fitMax":fitMax,"extrapMin":extrapMin,"extrapMax":extrapMax,
           "interpLeadingCoef":False,
           }

default["backgroundModel"]    = dictionary.data[release][channel]["backgroundModel"]
default["backgroundTemplate"] = dictionary.data[release][channel]["backgroundTemplate"]
nominal = ps.extrapolation(default)

# Get histogram values

rdh = nominal.get("template")
th1 = nominal._rootHists.values()[0]


################################################################################
# check number of entries
################################################################################
print yellow("Checking numbers of bins")
print yellow(rdh.numEntries())
print yellow(th1.GetNbinsX())
if rdh.numEntries() != th1.GetNbinsX():
    print red("fail")
    sys.exit(1)
else:
    print green("pass")

################################################################################
# check bin contents
################################################################################
print yellow("Checking bin contents")
rdhVals = [[rdh.get(i),rdh.weight()][1] for i in range(rdh.numEntries())]
th1Vals = [th1.GetBinContent(i) for i in range(1,th1.GetNbinsX()+1)]
diffs   = np.array(rdhVals) - np.array(th1Vals)
if np.any(diffs):
    print red("fail")
    sys.exit(1)
else:
    print green("pass")

print green("DONE")
sys.exit(0)
