from __future__ import division

################################################################################
#   Unit test for background only fit
################################################################################

import sys
sys.path = ["../source"] + sys.path
from setup import *
import plotter


def setupFit(interference=None,chirality=None,
                  channel=None,lumi=None,
                  fitMin=None,fitMax=None,
                  extrapMin=None,extrapMax=None,
                  signalMass=None,
                  inject=0,release="21",
                  useSbModel=1,
                  backgroundName="backgroundTemplate",
                  plotOn=0,fitOn=1,
                  fitToData=False,
                  tag="",
              ):
    """ Fit given signal
        make plot of extrapolation
        return nominal extrapolation
    """
    default = {\
               "signalInjectScale":inject,
               "fitMin":fitMin,"fitMax":fitMax,"extrapMin":extrapMin,"extrapMax":extrapMax,
               "interpLeadingCoef":False,
               "interference":interference,
               "chirality":chirality,
               "channel":channel,
               "lumi":lumi,
               "fitOn":fitOn,
               "tag":tag,
               "rebinSignal":1*rebinScale,
               "useSbModel":useSbModel,
               }
    scale = lumi/80.5
    default["signalTemplate"] = "../../SharedInputsProj/rel21_ci/templates_r21_{3}_august/CI_Template_{0}.root:diff_CI_{0}_{1}_{2}_TeV:GeV:rebin{4}:rescale{5}".format(chirality,interference,signalMass,channel,1*rebinScale,scale)
    default["backgroundModel"]    = dictionary.data[release][channel]["backgroundModel"]
    # fix dilep lead
    default["backgroundTemplate"] = dictionary.data[release][channel][backgroundName]
    default["backgroundTemplate"]+=":rescale{0}".format(lumi/139)
    default["backgroundTemplate"]+=":rebin{0}".format(rebinScale*10)
    # default["signalModel"] = "RooGaussian::signalModel(x,mean[2],std[0.5])"

    nominal = ps.extrapolation(default)
    nominal.get("lambda").setVal(signalMass)
    return nominal


# default inputs
channel = "mm"
release = "21"
lumi = 139

fitMin = 300
# fitMin = 1000
fitMax = 2000
extrapMin = fitMax
extrapMax = 5000
# fitMaxes = [1500,1750,2000,2500,3000]

inject = 0

interferences = ["dest","const"]
chiralities = ["LL","LR","RL","RR"]

interferences = ["dest"]
# interferences = ["const"]
# chiralities = ["LL"]

signalMasses = [16,30,50,90]
# signalMasses = [40]
signalMasses = [28]
# signalMasses = [26,40]
# signalMasses = [20]
# signalMasses = [12]
# signalMasses = [16]
# signalMasses = [80]

os.popen("rm plots/*")

# for channel in ["ee"]:
# for channel in ["mm"]:
for channel in ["ee","mm"]:
    for interference in interferences:
        signals = {}
        for chirality in chiralities:
            hists = {}
            path = "../../SharedInputsProj/rel21_ci/templates_r21_{0}_august/CI_Template_{1}.root".format(channel,chirality)
            f = TFile.Open(path)
            for signalMass in signalMasses:

                # histo
                scale = 139/80.5
                rebinScale = 10
                # name = "diff_CI_{0}_{1}_{2}_TeV".format("LL",interference,signalMass)
                name = "diff_CI_{0}_{1}_{2}_TeV".format(chirality,interference,signalMass)
                print green(name)
                hists[signalMass] = f.Get(name) 
                hists[signalMass].Rebin(rebinScale)
                hists[signalMass].Scale(scale)

                # do fit
                mc.reset()
                nominal = setupFit(interference=interference,
                              chirality=chirality,
                              channel=channel,
                              lumi=lumi,fitMin=fitMin,
                              fitMax=fitMax,extrapMin=extrapMin,
                              extrapMax=extrapMax,inject=inject,
                              signalMass=signalMass,
                              release=release,
                              # tag="-fMax{0}".format(fitMax/1000).replace(".","p"),
                              plotOn=0,fitOn=1,
                              backgroundName="backgroundTemplate",
                              useSbModel=True,
                              # useSbModel=False,
                             )
                x = nominal.get("x")
                morphedSignal = nominal.get("signalModel")
                morphedSignalNorm = nominal.get("signalNorm").getVal()
                morphedSignalRw = rw.rooWrap(morphedSignal,x)
                morphedSignalRw.scale(morphedSignalNorm)
                name = "{0}-{1} {2} TeV".format(chirality,interference,signalMass)
                signals[name] = morphedSignalRw


        path = "plots/sigShape-{0}-{1}.png".format(interference,channel)
        label = "{0} {1}".format(channel.replace("mm","$\mu\mu$"),interference)
        plotter.signalShapePlot(signals,hists,label=label,path=path)






print green("DONE")
