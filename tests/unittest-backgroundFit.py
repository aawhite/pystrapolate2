################################################################################
# Tests background fit
#
# What is checked
# 1) Background function fit converges
# 2) Fit params aren't different from expectation
#
# Exits 0 if no problem
# Exits 1 if failing a test
################################################################################

import sys
sys.path = ["../source"] + sys.path
from setup import *
import plotter

release   = "21"
channel   = "ee"
fitMin    = 0.25
# fitMin    = 1
fitMax    = 2
extrapMin = 6
extrapMax = 6

default = {\
           "signalInjectScale":0,
           "fitMin":fitMin,"fitMax":fitMax,"extrapMin":extrapMin,"extrapMax":extrapMax,
           "interpLeadingCoef":False,
           }

default["backgroundModel"]    = dictionary.data[release][channel]["backgroundModel"]
default["backgroundTemplate"] = dictionary.data[release][channel]["backgroundTemplate"]
nominal = ps.extrapolation(default)

nominal.fit()
fitResult = nominal._fitResults[0]

# nominal.get("model").Print(); quit()

correctValues = {
    "p2":-0.952870773507,
    "p3":-0.0900167128668,
    "p0":-11.0339625429,
    "p1":-4.16355733694,
    "signalNorm":0.0,
    "diphotonLeadingCoef":5.75471939134,
    "backgroundNorm":128433.850116,
}
rooParams = nominal.get("model").getParameters(RooArgSet(nominal.get("x")))
iterator = rooParams.fwdIterator()
ret = {}
while True:
    var=iterator.next()
    if not var: break
    name = var.GetName()
    val  = var.getVal()
    exp  = correctValues[name] 
    if exp==0: continue
    diffFromExp = abs(val-exp)/exp
    if diffFromExp>1e-9:
        print correctValues[name]-val,correctValues[name],val
        print red("fail backgroundFit unit test",name)
        sys.exit(1)
    print green("pass",name,val)

# plot
plotter.backgroundFitPlot(nominal,
                          title="Test of background fit",
                          path="plots/unittest-backgroundFit-plot.png")

print green("DONE")
sys.exit(0)
