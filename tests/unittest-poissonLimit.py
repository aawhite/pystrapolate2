from __future__ import division

################################################################################
#   Unit test for poisson limit caluclator
################################################################################

import sys
sys.path = ["../source"] + sys.path
from setup import *
import plotter
os.popen("rm plots/*")

################################################################################
# Poisson cross check code
################################################################################
from scipy.stats import poisson
def pTail(nObs,nExp):
  # return the poisson probability in tail above nObs
  return 1-poisson.cdf(nObs,nExp)

def upperLimit(nObs,nExp,ci):
  # return the 95% upper limit on signal
  nSig=0
  # select which direction to move nSig
  initTail = pTail(nExp,nObs)
  print "#"*50
  print "Initial probability in tail:", initTail
  print "#"*50
  sign=-0.1

  if initTail < 1-ci:
    while True:
      print nObs, nExp, nSig, pTail(nExp+nSig,nObs)
      if pTail(nExp+nSig,nObs) > 1-ci: break
      nSig+=1*sign
  else:
    while True:
      print nObs, nExp, nSig, pTail(nExp+nSig,nObs)
      if pTail(nExp+nSig,nObs) < 1-ci: break
      nSig-=1*sign
  return nSig


nBkgList = np.arange(1,30,1)
lims = {}
nBkgs = {}

ci=0.95
lims["None"] = [upperLimit(nExp,nExp,ci) for nExp in nBkgList]
nBkgs["None"] = nBkgList


# for w in [0,1,5,10,50]:
# for w in [0,1,10]:
for w in [1,5,10]:
    nBkgs[w]= nBkgList
    lims [w]= [limits.poissonLimit(nExp,nExp,npWidth1=w,npWidth2=1)["upperLimit"] for nExp in nBkgList]
    # lims [w]= [nExp*w for nExp in nBkgs[w]]

plotter.poissonPlot(nBkgs,lims)
