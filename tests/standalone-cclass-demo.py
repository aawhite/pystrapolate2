from __future__ import division
import sys,os
sys.path = ["/home/prime/Downloads/buildRoot/lib/"] + sys.path
import ROOT
from ROOT import RooWorkspace
from ROOT import RooArgSet
from ROOT import RooDataSet
from ROOT import RooLinearVar
from ROOT.RooStats import ModelConfig
from ROOT.RooStats import AsymptoticCalculator
from ROOT.RooStats import HypoTestInverter
from ROOT.RooStats import HypoTestCalculatorGeneric

from ROOT import gROOT
gROOT.LoadMacro("class.h")
from ROOT import custom


def nSigLimit(nBkg, nObs,i=1,interference="const",chirality="LL",channel="ee",npWidth1=1,npWidth2=1,npWidth3=1):
    """ calculate single bin limit on nSig
        np1 = nuissance parameter width  number:1
        nBkg is background expectation
        nSigLowerVal is the minimum value of nSig
        nObs is events observed
    """

    ##################################################
    # make workspace
    ##################################################
    w = RooWorkspace("w","w")
    w.factory("nObs[{0}]".format(nObs))
    w.factory("nBkg[{0}]".format(nBkg))
    w.factory("x[130,0,6000]")
    w.var("x").setConstant()
    x = w.var("x")

    ###################################################
    ## make lambda
    ###################################################
    ## inverted lambda, used for limits
    ## w.factory("lambdaInv[-200,-12]")
    #w.factory("lambdaInv[0,100]")
    #w.factory("slope[-1]")
    #w.factory("offset[100]")
    #lambd = RooLinearVar("lambda","lambda",w.var("lambdaInv"),w.var("slope"),w.var("offset"))
    #getattr(w,"import")(lambd)

    w.factory("lambda[0,100]")
    poiName = "lambda"
    nSigClass = custom("nSig","nSig",w.var("x"),w.obj("lambda"))
    getattr(w,"import")(nSigClass)

    # w.factory("nSig[0,100]")
    # poiName = "nSig"

    ##################################################
    # make poisson model
    ##################################################
    # w.factory("sum:nExp(nSig,prod:bkgProd(sum:bkgSum(1,np1[0,-1,1],np2[0,-1,1],np3[0,-1,1]),nBkg))")
    w.factory("sum:nExp(nSig,prod:bkgProd(sum:bkgSum(1,np1[0,-5,5],np2[0,-5,5],np3[0,-5,5]),nBkg))")
    w.factory("Gaussian:g1(np1,0,npWidth1[{0}])".format(npWidth1/nBkg));
    w.factory("Gaussian:g2(np2,0,npWidth2[{0}])".format(npWidth2/nBkg));
    w.factory("Gaussian:g3(np3,0,npWidth3[{0}])".format(npWidth3/nBkg));
    w.factory("Poisson:pois(nObs,nExp)")
    w.factory("PROD:model(pois,g1,g2,g3)");
    # w.factory("PROD:model(pois,a[1])");
    # w.factory("PROD:model(pois,g1,g2)");

    data = RooDataSet("data","", RooArgSet(w.var("nObs")))
    w.var("nObs").setVal(nObs)
    data.add(RooArgSet(w.var("nObs")))
    getattr(w,"import")(data)

    ##################################################
    # create S+B hypothesis H1
    ##################################################
    sbModel = ModelConfig("sbModel",w)
    sbModel.SetPdf(w.pdf("model"))
    sbModel.SetParametersOfInterest(RooArgSet(w.var(poiName)))
    sbModel.SetObservables(RooArgSet(w.var("nObs")))
    sbModel.SetNuisanceParameters(RooArgSet(w.var("np1"),w.var("np2"),w.var("np3")))
    sbModel.SetSnapshot(RooArgSet(w.var(poiName)))
    ##################################################

    ##################################################
    # create null hypothesis H0 (based on H)
    ##################################################
    bModel = sbModel.Clone()
    bModel.SetName("bModel")
    w.obj(poiName).setVal(0)
    bModel.SetSnapshot(RooArgSet(w.obj(poiName)))
    ##################################################

    ##################################################
    # create hypotest inverter
    ##################################################
    ac = AsymptoticCalculator(data, bModel, sbModel)
    ac.SetOneSided(True)
    # ac.SetOneSided(0)
    hypoCalc = HypoTestInverter(ac)
    # hypoCalc.SetFixedScan(100,-100,-90,False)
    # hypoCalc.SetFixedScan(100,0.1,100,False)
    hypoCalc.SetFixedScan(100,0.1,10,True)
    hypoCalc.SetConfidenceLevel(0.95)
    hypoCalc.UseCLs(True)
    # hypoCalc.UseCLs(0)

    htr = hypoCalc.GetHypoTestCalculator().GetHypoTest()
    htir = hypoCalc.GetInterval()
    w.obj(poiName).setVal(htir.UpperLimit())
    ret = w.obj("nSig").getVal()

    # background only
    w.var(poiName).setVal(0); print 0,":",w.obj("lambda").getVal()
    w.var(poiName).setVal(10); print 10,":",w.obj("lambda").getVal()
    w.var(poiName).setVal(20); print 20,":",w.obj("lambda").getVal()
    w.var(poiName).setVal(30); print 30,":",w.obj("lambda").getVal()
    w.var(poiName).setVal(40); print 40,":",w.obj("lambda").getVal()
    w.var(poiName).setVal(50); print 50,":",w.obj("lambda").getVal()
    w.var(poiName).setVal(60); print 60,":",w.obj("lambda").getVal()
    w.var(poiName).setVal(70); print 70,":",w.obj("lambda").getVal()
    w.var(poiName).setVal(80); print 80,":",w.obj("lambda").getVal()
    w.var(poiName).setVal(90); print 90,":",w.obj("lambda").getVal()
    w.var(poiName).setVal(100); print 100,":",w.obj("lambda").getVal()
    # large signal only
    w.obj("nExp").Print()
    # quit()

    # # background only
    # w.var("lambdaInv").setVal(100); print 100,":",w.obj("nSig").getVal()
    # w.var("lambdaInv").setVal(90); print 90,":",w.obj("nSig").getVal()
    # w.var("lambdaInv").setVal(80); print 80,":",w.obj("nSig").getVal()
    # w.var("lambdaInv").setVal(70); print 70,":",w.obj("nSig").getVal()
    # w.var("lambdaInv").setVal(60); print 60,":",w.obj("nSig").getVal()
    # w.var("lambdaInv").setVal(50); print 50,":",w.obj("nSig").getVal()
    # w.var("lambdaInv").setVal(40); print 40,":",w.obj("nSig").getVal()
    # w.var("lambdaInv").setVal(30); print 30,":",w.obj("nSig").getVal()
    # w.var("lambdaInv").setVal(20); print 20,":",w.obj("nSig").getVal()
    # w.var("lambdaInv").setVal(10); print 10,":",w.obj("nSig").getVal()
    # w.var("lambdaInv").setVal(0); print 0,":",w.obj("nSig").getVal()
    # # large signal only
    # w.obj("nExp").Print()
    # # quit()

    # import matplotlib.pyplot as plt
    # os.popen("rm plots/*")
    # x=[]; y=[]
    # for i in range(htir.ArraySize()):
    #     x.append(htir.GetXValue(i))
    #     y.append(htir.CLs(i))
    # plt.plot(x,y,label="CLs")
    # plt.xlabel("POI")
    # plt.yscale("log")
    # plt.legend()
    # print "nSigX=",x
    # print "nSigY=",y
    # plt.show()
    # # plt.savefig("plots/out.png")
    # quit()



    print "-"*50
    print "model",w.obj("model").getVal()
    print "nExp",w.obj("nExp").getVal()
    print "np1",w.var("np1").getVal()
    print "np2",w.var("np2").getVal()
    print "np3",w.var("np3").getVal()
    print "-"*50

    return ret

if __name__=="__main__":
    # some settings for limits
    nBkg= 9.60053680468
    nObs = 6.0
    npWidth1= 0.458967154816
    npWidth2= 2.10999625248
    npWidth3= 0.180570557745
    limitNSig1=nSigLimit(nBkg,nObs,npWidth1=npWidth1,npWidth2=npWidth2,npWidth3=npWidth3)
    print "="*50
    print "limitNSig1",limitNSig1
    # print "limitNSig2",limitNSig2
    print "="*50
