from __future__ import division

################################################################################
# Make comparison between old and new signals
################################################################################

import sys
sys.path = ["../source"] + sys.path
from setup import *
import plotter
import roowrap as rw


oldSigPath = "../../SharedInputsProj/rel21_ci/templates_r21_CHAN_new"
newSigPath = "../../SharedInputsProj/rel21_ci/templates_r21_CHAN_august"
masses=[16,20,30]
os.popen("rm plots/*")

for channel in ["ee"]:
    for interference in ["const","dest"]:
    # for interference in ["const"]:
    # for interference in ["dest"]:
        for chirality in ["LL","LR","RL","RR"]:
        # for chirality in ["RL"]:
            fileName = "CI_Template_{0}.root".format(chirality)
            oldPath = os.path.join(oldSigPath.replace("CHAN",channel),fileName)
            newPath = os.path.join(newSigPath.replace("CHAN",channel),fileName)
            # open
            oldF = ROOT.TFile.Open(oldPath)
            newF = ROOT.TFile.Open(newPath)
            # get hists
            shapes = {}
            for mass in masses:
                hName = "diff_CI_{0}_{1}_{2}_TeV".format(chirality,interference,mass)
                hOld = oldF.Get(hName)
                hNew = newF.Get(hName)
                hOld.Scale(139/80.5)
                hNew.Scale(139/80.5)
                hOld.Rebin(10)
                hNew.Rebin(10)
                xNew = np.array([hNew.GetBinCenter(i) for i in range(1,hNew.GetNbinsX()+1)])
                yNew = np.array([hNew.GetBinContent(i) for i in range(1,hNew.GetNbinsX()+1)])
                xOld = np.array([hOld.GetBinCenter(i) for i in range(1,hOld.GetNbinsX()+1)])
                yOld = np.array([hOld.GetBinContent(i) for i in range(1,hOld.GetNbinsX()+1)])
                xNew = xNew[30:]
                yNew = yNew[30:]
                xOld = xOld[30:]
                yOld = yOld[30:]
                shapes[mass] = {}
                shapes[mass]["xNew"] = xNew
                shapes[mass]["yNew"] = yNew
                shapes[mass]["xOld"] = xOld
                shapes[mass]["yOld"] = yOld

            # make plot
            plotter.compareSignalShapes(shapes,interference,chirality,channel)



