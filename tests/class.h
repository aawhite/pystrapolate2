#ifndef CCLASS
#define CCLASS

/*
 This class is meant for debugging issues with roofit
 for negative POI's
*/

#include "RooAbsReal.h"
#include "RooRealProxy.h"
#include "RooTrace.h"

class RooRealVar;

class custom : public RooAbsReal {
public:

    custom(const char *name, const char *title, 
               RooAbsReal& _mll, RooAbsReal& _lambda) :
        RooAbsReal(name,title),
        lambda("lambda","lambda",this,_lambda)
    {
    };

    custom(const custom& other, const char* name=0) :
        RooAbsReal(other,name),
        lambda("lambda",this,other.lambda)
    {
    };

    virtual TObject* clone(const char* newname) const { return new custom(*this,newname); }
    inline virtual ~custom() {  }
    RooRealProxy lambda ;

protected:
    Double_t evaluate() const {
        return lambda;
        // return 100-lambda;
        // return 100-0.01*lambda*lambda;
    };
private:
    ClassDef(custom,1) // Gaussian PDF
};

ClassImp(custom);

#endif

