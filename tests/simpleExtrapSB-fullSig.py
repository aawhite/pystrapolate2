
"""
## This file is an example of fitting with S+B
"""

from __future__ import division
import sys,os,math,random,glob,time
sys.argv.append("-b")
sys.path = ["/home/prime/Downloads/buildRoot/lib/"] + sys.path


sys.path = ["../source"] + sys.path
import roowrap as rw
from setup import *
# from modules import *

import matplotlib.pyplot as plt

def plot(pdf,rdh,x,path="plots/output.png",errorsOn=True):
    print green("Making plot")
    frame=x.frame(RooFit.Title("Example"))
    if errorsOn:
        rdh.plotOn(frame,RooFit.LineColor(kBlack))
    else:
        rdh.plotOn(frame,RooFit.LineColor(kBlack),DataError(2))
    pdf.plotOn(frame,RooFit.LineColor(kRed))
    # chi2 = RooChi2Var("chi2","chi2",pdf,rdh).getVal()
    chi2 = frame.chiSquare(2)
    frame.SetTitle("chi2={0:.4g}".format(chi2))
    c = TCanvas("x","x",800,300)
    frame.Draw()
    c.Draw()
    img = TImage.Create()
    img.FromPad(c)
    img.WriteImage(path)

def plot_rw(x,pdf,bkg,rdh,w=None,bkgRdh=None,bkgNorm=None,fitMin=None,low=None,fitMax=None,path="plots/output_rw.png"):
    print green("Plotting")
    # Make plot using rooWrap
    plt.clf(); plt.cla()
    rdh = rw.rooWrap(rdh,x)
    bkgRdh = rw.rooWrap(bkgRdh,x)
    pdf = rw.rooWrap(pdf,x)
    bkg = rw.rooWrap(bkg,x)

    pdf.normalizeTo(rdh,minRange=fitMin,maxRange=fitMax)
    bkg.normalize(bkgNorm.getVal(),minRange=fitMin,maxRange=fitMax)
    # bkgRdh.normalizeTo(rdh)
    plt.plot(rdh.x,rdh.y,"ko",label="Template")
    plt.plot(bkgRdh.x,bkgRdh.y,"bo",label="Background Template")
    plt.plot(pdf.x,pdf.y,"C3",label="S+B Model")
    plt.plot(bkg.x,bkg.y,"C0",label="B Model")
    plt.plot([fitMax]*2,plt.ylim(),"c",linewidth=10,alpha=0.5,label="FitMax")
    plt.yscale("log")
    plt.xscale("log")
    plt.legend(frameon=0,fontsize=12)
    plt.savefig(path,bbox_inches="tight")

from ROOT.RooFit import RecycleConflictNodes
from ROOT.RooFit import RenameConflictNodes
def add(w,x):
    """ add to workspace """
    print "==== Adding:", x
    print type(x).__name__
    # x.Print()
    # getattr(w,"import")(x)
    # getattr(w,"import")(x,RecycleConflictNodes())
    if type(x).__name__ == "RooBinning":
        getattr(w,"import")(x)
    elif type(x).__name__ == "RooDataHist":
        getattr(w,"import")(x)
    else:
        getattr(w,"import")(x,RecycleConflictNodes())
        # getattr(w,"import")(x,RenameConflictNodes("Alt"))




def _setBinningToTemplate(w,binningHist):
    """ Set binning of "x" to that of template """
    # get binning
    nBins  = binningHist.numEntries()
    low    = binningHist.get(0)["x"].getVal()
    second = binningHist.get(1)["x"].getVal()
    high   = binningHist.get(nBins-1)["x"].getVal()
    width  = second - low
    # correct for bin center value
    low  -= width/2
    high += width/2
    # make binning
    binning = RooBinning(low,high,"full")
    binning.addUniform(nBins,low,high)
    add(w,binning)
    # set variable "x" to have binning from binningHist
    w.var("x").setBinning(binning)


globalFiles = []
def loadHisto(path,name,rebin=1,scale=1):
    """ Load histo (rebin and scale) """
    global globalFiles
    rebin=int(rebin)
    print "Loading {0}:{1}".format(path,name)
    # open file and extract histo
    f = TFile.Open(path)
    f.Print()
    h = f.Get(name)
    h.Print()
    print "Rebin by", rebin
    h.Rebin(rebin)
    print "Scale by", scale
    h.Scale(scale)
    # save in global variable
    globalFiles.append(f)
    print "Loading done"
    return h

def testFit(nSig=10,plotOn=True):
    precision = 1e-10
    RooAbsPdf.defaultIntegratorConfig().setEpsRel(precision);
    RooAbsPdf.defaultIntegratorConfig().setEpsAbs(precision);
    # set up workspace
    low = 150
    fitMin = 500
    fitMin = 250
    # fitMin = 200
    fitMin = 300
    # fitMin = 150
    # fitMin = 350
    # fitMin = 400
    fitMax = 2050
    # fitMax = 2140
    # fitMax = 2400 # good with 200
    # fitMax = 3000
    fitMax = 2*1000
    high = 6000

    # signal type
    chirality = "LL"
    interference = "const"
    # interference = "dest"
    signalMass = 12
    signalMass = 24
    # signalMass = 28
    channel = "ee"
    lumi = 139
    w = RooWorkspace("w","w")

    # inject or not signal
    inject = 0

    rebinScale = 10
    x = RooRealVar("x","x",low,low,high)
    l = RooRealVar("lambda","lambda",signalMass,10,100)
    add(w,x)
    add(w,l)
    signalPdf,signalPdfNorm = mc.morphedSignal(interference,chirality,rebin=1*rebinScale,w=w,channel=channel,lumi=lumi)
    signalPdf.SetName("morphed")

    # high = 2000
    add(w,x)

    # w.factory("x[{0},{1}]".format(low,high))
    # x = w.var("x")

    # fullBinning = RooBinning(low,high,"full")
    # fullBinning.addUniform(1000,low,high)

    # fitBinning = RooBinning(low,fitMax,"fit")
    # x.setBinning(fitBinning)

    # ================================================================================
    # Signal
    # ================================================================================
    # x,l,signalPdf,signalPdfNorm = mc.morphedSignal(interference,chirality,channel=channel,lumi=lumi,rebin=10)
    # add(w,x)
    # add(w,l)
    # quit()


    # ================================================================================
    # generate background 
    # ================================================================================
    bkgPath = "../../SharedInputsProj/rel21_templates/merged_ee.root"
    bkgHisto= "ee_dibosonNoOutliers_201516fakes_smoothDY_smoothTTbar"
    # bkgRebin= 1000/100
    bkgRebin= 10*rebinScale
    bkgScale = lumi/139
    h_bkg = loadHisto(bkgPath,bkgHisto,rebin=bkgRebin,scale=bkgScale)
    # flatten
    # for i in range(1,h_bkg.GetNbinsX()+1):
        # h_bkg.SetBinContent(i,10000+i*100+2*i**2)
        # h_bkg.SetBinContent(i,100+i*10+0.1*i**2)
    # roofit-ify
    rdh  = RooDataHist("data","data",RooArgList(w.var("x")),h_bkg.Clone())
    bkgRdh=rdh.Clone()
    _setBinningToTemplate(w,rdh)
    w.var("x").setRange("fitRange",fitMin,fitMax)
    w.var("x").setRange("fullRange",low,high)
    w.var("x").setRange("srRange",fitMax,high)

    # add signal
    # default["signalTemplate"] = "../../SharedInputsProj/rel21_ci/templates_r21_{3}_new/CI_Template_{0}.root:diff_CI_{0}_{1}_{2}_TeV:GeV:rebin1:rescale{4}".format(chirality,interference,signalMass,channel,scale)
    sigPath = "../../SharedInputsProj/rel21_ci/templates_r21_{0}_new/CI_Template_{1}.root".format(channel,chirality)
    sigHisto= "diff_CI_{0}_{1}_{2}_TeV".format(chirality,interference,signalMass)
    # sigRebin= 1000/100
    sigRebin= 1*rebinScale
    sigScale = lumi/80.5
    h_sig = loadHisto(sigPath,sigHisto,rebin=sigRebin,scale=sigScale)
    sigRdh  = RooDataHist("sigData","sigData",RooArgList(w.var("x")),h_sig.Clone())
    if inject: rdh.add(sigRdh)

    # roohistpdf
    sigRhp  = RooHistPdf("sigRhp","sigRhp",RooArgSet(x),sigRdh)
    add(w,sigRhp)
    nSigExp = sigRdh.sumEntries("x>{0} && x<{1}".format(low,high))
    sigInt = nSigExp
    w.factory("PROD::sigNormed(sigRhp,sigIntegral[{0}])".format(1/sigInt))
    # w.factory("mu[-10,10]")
    w.factory("mu[0,{0}]".format(2*sigInt))
    # w.factory("mu[0]")
    # w.factory("mu[5.648]")
    print red(w.pdf("sigNormed").Print())
    print red(w.var("mu").Print())


    # nBkg = sum([h_bkg.GetBinContent(i) for i in range(1,h_bkg.GetNbinsX())])
    # nBkg = bkgRdh.sumEntries("x>{0} && x<{1}".format(low,fitMax))
    nBkg = bkgRdh.sumEntries("x","fitRange")

    nBkgReal = w.factory("nBkgReal[{0}]".format(nBkg))
    # nSigReal = w.factory("nSigReal[{0}]".format(nSig))

    # ================================================================================
    # Background
    # ================================================================================
    # set up background
    # bkgPdf = w.factory("EXPR::bkgModel('(2.4952/(pow(91.1876-(x/1000),2)+pow(2.4952,2)))*pow(1-pow((x/1000)/13.0,0.5),diphotonLeadingCoef)*pow((x/1000)/13.0,p0+p1*log((x/1000)/13.0)+p2*log((x/1000)/13.0)^2+p3*log((x/1000)/13.0)^3)',x,diphotonLeadingCoef[2.8088,1,10],p0[-11.492,-30,10],p1[-4.2473,-10,5],p2[-0.92928,-5,1],p3[-0.085168,-1,0.1])")
    bkgPdf = w.factory("EXPR::bkgModel('(2.4952/(pow(91.1876-(x/1000),2)+pow(2.4952,2)))*pow(1-pow((x/1000)/13.0,0.5),diphotonLeadingCoef)*pow((x/1000)/13.0,p0+p1*log((x/1000)/13.0)+p2*log((x/1000)/13.0)^2+p3*log((x/1000)/13.0)^3)',x,diphotonLeadingCoef[30,1,60],p0[-11.492,-30,0],p1[-4.2473,-10,0],p2[-0.92928,-5,0],p3[-0.085168,-1,0])")


    # set up s+b model
    # bkgCoef = w.factory("nBkgFit[{0}]".format(nBkgReal.getVal()))



    print "Making nBkg"
    w.factory("nBkg[{0}]".format(nBkg))
    print "Making bkgMu"
    w.factory("bkgMu[1,0.5,2]")
    print "Making bkgCoef"
    bkgCoef = w.factory("prod::bkgCoef(bkgMu,nBkg)")



    # pdf = RooAddPdf( "model","model",
    #                 RooArgList(bkgPdf,  w.pdf("sigNormed")),
    #                 RooArgList(bkgCoef, w.var("mu"))
    #               )

    pdf = RooAddPdf( "model","model",
                    RooArgList(bkgPdf,  signalPdf),
                    # RooArgList(bkgCoef, w.var("mu"))
                    RooArgList(bkgCoef, signalPdfNorm)
                  )

    pdf.fixAddCoefRange("fitRange",True)
    pdf.fixAddCoefNormalization(RooArgSet(w.var("x")),True)
    getattr(w,"import")(pdf)

    # do fit
    w.pdf("model").fitTo(rdh,
                        RooFit.Range("fitRange")
                        )
    print yellow("FIT DONE")


    plot_rw(w.var("x"),w.pdf("model"),w.pdf("bkgModel"),rdh,
            bkgRdh=bkgRdh,bkgNorm=w.obj("bkgCoef"),
            fitMin=fitMin,low=low,fitMax=fitMax,w=w,
           )

    try:
        print "mu",yellow(w.var("mu").getVal(),sigInt)
        print "integral",yellow(w.pdf("morphed").createIntegral(RooArgSet(w.var("x")),RooFit.Range("fitRange")).getVal())
        print "integral",yellow(w.pdf("morphed").createIntegral(RooArgSet(w.var("x")),RooFit.Range("fullRange")).getVal())
    except: pass
    try:
        print "lambda",yellow(w.var("lambda").getVal())
        # intg = w.pdf("bkgModel").createIntegral(RooArgSet(w.var("x")),RooFit.Range("srRange")).getVal()
        a = w.pdf("bkgModel").createIntegral(RooArgSet(w.var("x")),RooFit.Range("fullRange")).getVal()
        b = w.pdf("bkgModel").createIntegral(RooArgSet(w.var("x")),RooFit.Range("srRange")).getVal()
        nbkg = w.var("nBkgFit").getVal()
        intg = b/a * nbkg
        print "integral bkgModel",yellow(intg)
    except: pass

    nBkgSr = bkgRdh.sumEntries("x>{0} && x<{1}".format(fitMax,high))
    print red("nBkgSr",nBkgSr)

    nSigSr = sigRdh.sumEntries("x>{0} && x<{1}".format(fitMax,high))
    print red("nSigSr",nSigSr)


    # return mu

if __name__=="__main__":
    os.popen("rm plots/*")
    nSig = 1000
    testFit(nSig=nSig,plotOn=True)
    print green("DONE")

