from __future__ import division

################################################################################
#   Make normalized hists to corresponding data
################################################################################

import sys
sys.path = ["../source"] + sys.path
from setup import *
import plotter

files = []
def loadH(info):
    global files
    iPath = info.split(":")[0]
    name = info.split(":")[1]
    iFile = TFile.Open(iPath)
    iHist = iFile.Get(name)
    files.append(iFile)
    iHist.Print()
    return iHist

release = "21"
channel = "mm"
channel = "ee"

loopNames = dictionary.pdfNames[release]
loopNames+=["backgroundTemplate"]
print loopNames
for pdfName in loopNames:
    info= dictionary.data[release][channel][pdfName]
    dataInfo= dictionary.data[release][channel]["data"]
    iHist = loadH(info)
    dHist = loadH(dataInfo)
    oPath = info.split(":")[0].replace(".root","-dataNormed.root")
    oPath = oPath.replace("rel21_templates","rel21_templates-dataNormed")

    # normalize
    lo = 250
    lo = 300
    # lo = 150
    hi = 6000
    ib0 = iHist.FindBin(lo)
    ib1 = iHist.FindBin(hi)
    db0 = dHist.FindBin(lo)
    db1 = dHist.FindBin(hi)

    ratio = dHist.Integral(db0,db1)/iHist.Integral(ib0,ib1)

    oFile = TFile.Open(oPath,"RECREATE")
    oFile.cd()
    oHist = iHist.Clone()
    oHist.Scale(ratio)
    oHist.Write()

    print yellow(ratio)

    print green(oPath)


