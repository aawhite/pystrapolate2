from __future__ import division

################################################################################
#   Unit test for background only fit
################################################################################

import sys
sys.path = ["../source"] + sys.path
from setup import *
import plotter


def sigFitMacro(interference=None,chirality=None,
                  channel=None,lumi=None,
                  fitMin=None,fitMax=None,
                  extrapMin=None,extrapMax=None,
                  signalMass=None,
                  inject=0,release="21",
                  useSbModel=1,
                  backgroundName="backgroundTemplate",
                  plotOn=0,fitOn=1,
                  tag="",
              ):
    """ Fit given signal
    """
    default = {\
               "signalInjectScale":inject,
               "fitMin":fitMin,"fitMax":fitMax,"extrapMin":extrapMin,"extrapMax":extrapMax,
               "interpLeadingCoef":False,
               "interference":interference,
               "chirality":chirality,
               "channel":channel,
               "lumi":lumi,
               "fitOn":fitOn,
               "tag":tag,
               "useSbModel":useSbModel,
               }
    rebinScale = 10
    scale = lumi/80.5
    default["backgroundModel"]    = dictionary.data[release][channel]["backgroundModel"]
    # fix dilep lead

    # use signal template as background
    default["backgroundTemplate"] = "../../SharedInputsProj/rel21_ci/templates_r21_{3}_new/CI_Template_{0}.root:diff_CI_{0}_{1}_{2}_TeV:GeV:rebin{4}:rescale{5}".format(chirality,interference,signalMass,channel,1*rebinScale,scale)

    nominal = ps.extrapolation(default)
    nominal.fit()
    if plotOn:
        plotter.plot_rw(nominal,useSbModel,
                path="plots/nominalFit-sb-{0}-{1}-{2}-sb{3}{4}.png".format(interference,chirality,signalMass,useSbModel,tag)
               )
    return nominal


# default inputs
channel = "ee"
release = "21"
lumi = 139

fitMin = 300
fitMin = 1000
fitMin = 800
fitMin = 600
fitMin = 500

fitMax = 2000
extrapMin = fitMax
extrapMax = 5000
# fitMaxes = [1500,1750,2000,2500,3000]

inject = 1

interferences = ["dest","const"]
chiralities = ["LL","LR","RL","RR"]

interferences = ["dest"]
# interferences = ["const"]
chiralities = ["LL"]
# chiralities = ["RR"]
# chiralities = ["RL"]

signalMasses = [12,20,30,40]
# signalMasses = [40]
# signalMasses = [26]
# signalMasses = [20]
signalMasses = [16]
# signalMasses = [80]

os.popen("rm plots/*")

for interference in interferences:
    for chirality in chiralities:
        for signalMass in signalMasses:
        # for fitMax in fitMaxes:
            # do fit
            nominal = fitMacro(interference=interference,
                          chirality=chirality,
                          channel=channel,
                          lumi=lumi,fitMin=fitMin,
                          fitMax=fitMax,extrapMin=extrapMin,
                          extrapMax=extrapMax,inject=inject,
                          signalMass=signalMass,
                          release=release,
                          # tag="-fMax{0}".format(fitMax/1000).replace(".","p"),
                          plotOn=1,fitOn=1,
                          backgroundName="backgroundTemplate",
                          useSbModel=True,
                          # useSbModel=False,
                         )
            print nominal



print green("DONE")
