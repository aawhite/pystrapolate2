from __future__ import division

################################################################################
#   Unit test for background only fit
################################################################################

import sys
sys.path = ["../source"] + sys.path
from setup import *
import plotter

os.popen("rm plots/*")

# default inputs
interference = "const"
chirality = "LL"
channel = "ee"
release = "21"
inject = 0
lumi = 139
signalMass = 20
nToys = 10

fitMin = 300
fitMax = 2000
extrapMin = fitMax
extrapMax = 6000
fitMaxes = [1500,1750,2000,2500,3000]

for fitMax in fitMaxes:
    # do fit
    nominal = fitMacro(interference=interference,
                  chirality=chirality,
                  channel=channel,
                  lumi=lumi,fitMin=fitMin,
                  fitMax=fitMax,extrapMin=extrapMin,
                  extrapMax=extrapMax,inject=inject,
                  signalMass=signalMass,
                  release=release,
                  tag="-fMax{0}".format(fitMax/1000).replace(".","p"),
                  plotOn=1,fitOn=1,
                  backgroundName="backgroundTemplate",
                  useSbModel=False,
                 )

# nominalYields = nominal.yields()
# nominalSs = nominalYields["nSpur"]


print green("DONE")
