from __future__ import division
import sys,os
sys.path = ["/home/prime/Downloads/buildRoot/lib/"] + sys.path
import ROOT
from ROOT import RooWorkspace
from ROOT import RooArgSet
from ROOT import RooDataSet
from ROOT import RooLinearVar
from ROOT.RooStats import ModelConfig
from ROOT.RooStats import AsymptoticCalculator
from ROOT.RooStats import HypoTestInverter
from ROOT.RooStats import HypoTestCalculatorGeneric

sys.path=["../source"]+sys.path
sys.path=["../settings"]+sys.path
from modules import *
from modules import mc as modelConstructor


def nSigLimit(nBkg, nObs,i=1,interference="const",chirality="LL",channel="ee",npWidth1=1,npWidth2=1,npWidth3=1):
    """ calculate single bin limit on nSig
        np1 = nuissance parameter width  number:1
        nBkg is background expectation
        nSigLowerVal is the minimum value of nSig
        nObs is events observed
    """
    ##################################################
    # make workspace
    ##################################################
    w = RooWorkspace("w","w")
    w.factory("nObs[{0}]".format(nObs))
    w.factory("nBkg[{0}]".format(nBkg))
    w.factory("x[130,0,6000]")
    w.var("x").setConstant()
    w.var("nBkg").setConstant()
    x = w.var("x")


    ##################################################
    # make lambda
    ##################################################
    # inverted lambda, used for limits
    w.factory("lambdaInv[-100,-12]")
    # w.factory("lambdaInv[0,100]")
    # w.factory("lambdaInv[0,100]")
    # w.factory("slope[-1]")
    # w.factory("offset[0]")
    # lambd = RooLinearVar("lambda","lambda",w.var("lambdaInv"),w.var("slope"),w.var("offset"))
    # getattr(w,"import")(lambd)


    poiName = "lambdaInv"
    rebinScale   = 1
    lumi         = 139
    srMin = dictionary.ranges[channel][interference][chirality]["extrapMin"]
    # srMin = 1000
    srMax = 6000
    # Set up morphed signal
    hint = modelConstructor.morphedIntegral(interference,chirality,
                            rebin=rebinScale,w=w,channel=channel,lumi=lumi,
                            srMin=srMin,srMax=srMax,
                           )
    hint.SetName("nSig")
    hint.SetTitle("nSig")
    getattr(w,"import")(hint)

    # from ROOT import custom
    # nSigClass = custom("nSig","nSig",w.var("x"),w.obj("lambda"))
    # getattr(w,"import")(nSigClass)

    # w.factory("nSig[0,100]")

    # # background only
    # for i in np.arange(0,100,0.1):
    #     w.var(poiName).setVal(i)
    #     print i, w.obj("nSig").getVal()
    # # large signal only
    # quit()

    ##################################################
    # make poisson model
    ##################################################
    # w.factory("sum:nExp(nSig,prod:bkgProd(sum:bkgSum(1,np1[0,-1,1],np2[0,-1,1],np3[0,-1,1]),nBkg))")
    w.factory("sum:nExp(nSig,prod:bkgProd(sum:bkgSum(1,np1[0,-5,5],np2[0,-5,5],np3[0,-5,5]),nBkg))")
    w.factory("Gaussian:g1(np1,0,npWidth1[{0}])".format(npWidth1/nBkg));
    w.factory("Gaussian:g2(np2,0,npWidth2[{0}])".format(npWidth2/nBkg));
    w.factory("Gaussian:g3(np3,0,npWidth3[{0}])".format(npWidth3/nBkg));
    w.factory("Poisson:pois(nObs,nExp)")
    w.factory("PROD:model(pois,g1,g2,g3)");
    # w.factory("PROD:model(pois,a[1])");
    # w.factory("PROD:model(pois,g1,g2)");

    data = RooDataSet("data","", RooArgSet(w.var("nObs")))
    w.var("nObs").setVal(nObs)
    data.add(RooArgSet(w.var("nObs")))
    getattr(w,"import")(data)



    ##################################################
    # create S+B hypothesis H1
    ##################################################
    sbModel = ModelConfig("sbModel",w)
    sbModel.SetPdf(w.pdf("model"))
    sbModel.SetParametersOfInterest(RooArgSet(w.var(poiName)))
    sbModel.SetObservables(RooArgSet(w.var("nObs")))
    sbModel.SetNuisanceParameters(RooArgSet(w.var("np1"),w.var("np2"),w.var("np3")))
    sbModel.SetSnapshot(RooArgSet(w.var(poiName)))
    ##################################################

    ##################################################
    # create null hypothesis H0 (based on H)
    ##################################################
    bModel = sbModel.Clone()
    bModel.SetName("bModel")
    w.obj(poiName).setVal(-100)
    bModel.SetSnapshot(RooArgSet(w.obj(poiName)))
    ##################################################

    ##################################################
    # create hypotest inverter
    ##################################################
    ac = AsymptoticCalculator(data, bModel, sbModel)
    ac.SetOneSided(True)
    hypoCalc = HypoTestInverter(ac)
    # hypoCalc.SetFixedScan(100,-35,-15,False)
    # hypoCalc.SetFixedScan(100,-35,-15,False)
    hypoCalc.SetFixedScan(100,-100,-10,False)
    # hypoCalc.SetFixedScan(100,0,70,False)
    hypoCalc.SetConfidenceLevel(0.95)
    hypoCalc.UseCLs(True)
    # hypoCalc.UseCLs(0)

    htr = hypoCalc.GetHypoTestCalculator().GetHypoTest()
    htir = hypoCalc.GetInterval()
    w.obj(poiName).setVal(htir.UpperLimit())

    # # background only
    # for i in np.arange(-100,0,1):
    #     w.var(poiName).setVal(i)
    #     print i,w.var(poiName).getVal(), w.obj("nSig").getVal()
    # # large signal only
    # quit()

    # d = open("/home/prime/plots/clsComp/data.txt","r").read()
    # exec(d)
    # import matplotlib.pyplot as plt
    # os.popen("rm plots/*")
    # x=[]; y=[]
    # for i in range(htir.ArraySize()):
    #     xVal = htir.GetXValue(i)
    #     w.var(poiName).setVal(xVal)
    #     nSig = w.obj("nSig").getVal()
    #     print xVal,nSig
    #     x.append(nSig)
    #     y.append(htir.CLs(i))
    # plt.plot(x,y,label="CLs")
    # plt.plot(nSigX,nSigY,label="Correct")
    # plt.xlabel("nSig")
    # plt.yscale("log")
    # plt.legend()
    # # print "lambdaX=",x
    # # print "lambdaY=",y
    # plt.show()
    # # plt.savefig("plots/out.png")
    # quit()


    ret = w.obj("nSig").getVal()
    print "-"*50
    print "poi",w.obj(poiName).getVal()
    print "model",w.obj("model").getVal()
    print "nExp",w.obj("nExp").getVal()
    print "np1",w.var("np1").getVal()
    print "np2",w.var("np2").getVal()
    print "np3",w.var("np3").getVal()
    print "-"*50

    return ret

precision = 1e-10
RooAbsPdf.defaultIntegratorConfig().setEpsRel(precision);
RooAbsPdf.defaultIntegratorConfig().setEpsAbs(precision);

if __name__=="__main__":
    # some settings for limits
    nBkg= 9.60053680468
    nObs = 6.0
    npWidth1= 0.458967154816
    npWidth2= 2.10999625248
    npWidth3= 0.180570557745
    limitNSig1=nSigLimit(nBkg,nObs,
                 npWidth1=npWidth1,
                 npWidth2=npWidth2,
                 npWidth3=npWidth3)
    print "="*50
    print "limitNSig1",limitNSig1
    # print "limitNSig2",limitNSig2
    print "="*50
