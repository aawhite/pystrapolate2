from __future__ import division
import sys, os
import math
import ROOT
import numpy as np
import array

sys.path = [os.path.dirname(os.path.abspath(__file__)) + "/../source"] + sys.path
from setup import *

def makeNPRanking(wsPath, wsFile, varSign="up", when="postfit"):
    print green("==") * 25
    print green("_______making NP ranking")
    print wsPath + wsFile
    f = ROOT.TFile(wsPath + wsFile)
    
    myWs = f.Get("w")
    print yellow("==") * 25
    print yellow("_______Printing WS")

    myWs.Print()
    mc = myWs.obj("sbModel")
    pdf = mc.GetPdf()
    data = myWs.data("data")
    np1Sig = myWs.obj("npWidth1").getValV()
    np2Sig = myWs.obj("npWidth2").getValV()

    nll = pdf.createNLL(data, ROOT.RooFit.Constrain(mc.GetNuisanceParameters()))
    nll.enableOffsetting(True)

    np = mc.GetNuisanceParameters()
    iter = np.createIterator()
    poi = mc.GetParametersOfInterest()
    iterPoi = poi.createIterator()

    histos = {}
    b = iterPoi.Next()
    while b:
        rrvPOI = b
        rrvPOI.setConstant(False)
        rrvPOI.setVal(0.01)
        rrvPOI.setMin(-100)
        rrvPOI.setMax(100)

        if rrvPOI:
            poiName = rrvPOI.GetName()
            histos[poiName] = ROOT.TH1F("h_{0}".format(poiName), "", 3, 0, 3)
            histos[poiName].SetCanExtend(ROOT.TH1.kAllAxes)

        b = iterPoi.Next()
    iterPoi.Reset()

    iter2 = np.createIterator()
    a = iter2.Next()
    while a:
        rrv = a
        rrv.setVal(0.0001)
        a = iter2.Next()
    iter2.Reset()

    # Initial fit to get central values of POI without NP variations
    print yellow("==") * 25
    print yellow("Setting up Minimizer")
    minim_ini = ROOT.RooMinimizer(nll)
    minim_ini.setPrintLevel(0)
    minim_ini.setStrategy(2)
    minim_ini.setOffsetting(True)
    minim_ini.setProfile()
    minim_ini.setEps(0.00001)
    minim_ini.minimize("Minuit")
    minim_ini.minos()

    print yellow("==") * 25
    print yellow("Initial Fit")
    result_ini = minim_ini.save()
    result_ini.Print()
    myWs.saveSnapshot("initialFit", myWs.allVars())

    # Make fit to see effect of each NP (only interested in central value -> no need to run minos)
    print yellow("==") * 25
    print yellow("Setting up Minimizer to find effect of NPs")
    minim = ROOT.RooMinimizer(nll)
    minim.setStrategy(2)
    minim.setPrintLevel(0)
    minim.setProfile()
    minim.setEps(0.00000001)

    a = iter.Next()
    while a:
        rrv = a
        print yellow("==") * 25
        print yellow("Run fit for NP {0}, {1} {2}".format(rrv.GetName(),when,varSign))
        if not rrv:
            print red("This is not a systematics NP")
            continue

        myWs.loadSnapshot("initialFit")

        b = iterPoi.Next()
        # shift POI value to make it more stable
        while b:
            rrvPOI = b
            rrvPOI.setVal(rrvPOI.getValV())
            b = iterPoi.Next()
        iterPoi.Reset()

        # Impact of shit
        if when == "prefit":
            if a.GetName() == "np1":
                if varSign == "down": rrv.setVal(rrv.getValV() - np1Sig)
                elif varSign == "up": rrv.setVal(rrv.getValV() + np1Sig)
            elif a.GetName() == "np2":
                if varSign == "down": rrv.setVal(rrv.getValV() - np2Sig)
                elif varSign == "up": rrv.setVal(rrv.getValV() + np2Sig)
        elif when == "postfit":
            if varSign == "down": rrv.setVal(rrv.getValV() + rrv.getErrorLo())
            elif varSign == "up": rrv.setVal(rrv.getValV() + rrv.getErrorHi())
        rrv.setConstant(True)

        minim.minimize("Minuit")
        b = iterPoi.Next()
        while b:
            rrvPOI = b
            name = rrvPOI.GetName()
            iniVal = result_ini.floatParsFinal().find(name).getValV()
            if rrvPOI:
                print yellow("==") * 25
                print yellow("{0}, {1}, {2} deltaPOI = {3}".format(rrv.GetName(), when, varSign, rrvPOI.getValV() - iniVal))
                histos[name].Fill(rrv.GetName(), rrvPOI.getValV() - iniVal)
            b = iterPoi.Next()
        iterPoi.Reset()

        a = iter.Next()
    iter.Reset()

    fout = ROOT.TFile(
        "output/NPranking_{0}_{1}_{2}".format(when, varSign, wsFile),
        "RECREATE",
    )
    result_ini.Write("fitResult_nominal")
    b = iterPoi.Next()
    while b:
        rrvPOI = b
        name = rrvPOI.GetName()
        if rrvPOI:
            histos[name].Write(name)
        b = iterPoi.Next()
    iterPoi.Reset()

    fout.Close()

