
################################################################################
# extra.py
# Quality of life functions common to several applications
# Common procedures like:
#    * Do nominal fit
#    * Set limit
#    * Throw toys
# Common imported classes
################################################################################

print "Modules"

import sys,os, math, copy, glob
sys.path = [os.path.realpath(__file__)] + sys.path

# included to work on UMT3int01:
sys.path = ["/usr/lib64/python2.6/site-packages"] + sys.path
# included to work on UMT3int02 (not clear if needed):
sys.path = ["/usr/lib64/python2.7/site-packages"] + sys.path
sys.path = ["../settings"] + sys.path



import numpy as np
import numpy.ma as ma
import random
# from random import gauss
from math import sqrt


from modules import *
# RooAbsPdf = ROOT.RooFit.RooAbsPdf

# fix random seeds
# random.seed(1)
# np.random.seed(1)

# pystrap library
import pystrap as ps
import limits

import cPickle as pickle

# dictionary of settings
import dictionary
import systematics
# import accEff
import function_systematics

try:
    import plotter
except:
    print "="*50
    print "Warning: no plotting"
    print "="*50
import observed

def yellow(*string):
    """ return string as red """
    string = [str(s) for s in string]
    ret = "\033[33m{0}\033[39m".format(" ".join(string))
    return ret

def red(*string):
    """ return string as red """
    string = [str(s) for s in string]
    ret = "\033[31m{0}\033[39m".format(" ".join(string))
    return ret

def green(*string):
    """ return string as green """
    string = [str(s) for s in string]
    ret = "\033[32m{0}\033[39m".format(" ".join(string))
    return ret

def blue(*string):
    """ return string as green """
    string = [str(s) for s in string]
    ret = "\033[34m{0}\033[39m".format(" ".join(string))
    return ret

import modelConstructor as mc

def fitMacro(interference=None,chirality=None,
                  channel=None,lumi=None,
                  fitMin=None,fitMax=None,
                  extrapMin=None,extrapMax=None,
                  signalMass=None,
                  inject=0,release="21",
                  useSbModel=1,
                  backgroundName="backgroundTemplate",
                  plotOn=0,fitOn=1,
                  fitToData=False,
                  scrambleInput=False,
                  tag="",
              ):
    """ Fit given signal
        make plot of extrapolation
        return nominal extrapolation
    """
    rebinScale = 1
    default = {\
               "signalInjectScale":inject,
               "fitMin":fitMin,"fitMax":fitMax,"extrapMin":extrapMin,"extrapMax":extrapMax,
               "interpLeadingCoef":False,
               "interference":interference,
               "chirality":chirality,
               "channel":channel,
               "lumi":lumi,
               "fitOn":fitOn,
               "tag":tag,
               "rebinSignal":10*rebinScale,
               "useSbModel":useSbModel,
               }
    # print red(lumi); quit()
    scale = lumi/80.5
    if inject:
        default["signalTemplate"] = "../../SharedInputsProj/rel21_ci/templates_r21_{3}_august/CI_Template_{0}.root:diff_CI_{0}_{1}_{2}_TeV:GeV:rebin{4}:rescale{5}".format(chirality,interference,signalMass,channel,1*rebinScale,scale)
    default["backgroundModel"]    = dictionary.data[release][channel]["backgroundModel"]
    # default["backgroundModel"]    = "EXPR::backgroundModel('1+b1*pow(x/1000,a1)+b2*pow(x/1000,a2)',x,a1[-10,0],b1[0,1000000000],a2[-10,0],b2[0,1000000000])"
    # default["backgroundModel"]    = "EXPR::backgroundModel('(2.4952/(pow(91.1876-(x/1000),2)+pow(2.4952,2)))*pow(1-pow((x/1000)/13.0,0.5),diphotonLeadingCoef)*pow((x/1000)/13.0,p0+p1*log((x/1000)/13.0)+p2*log((x/1000)/13.0)^2+p3*log((x/1000)/13.0)^3)',x,diphotonLeadingCoef[7,1,60],p0[-11.492,-30,0],p1[-4.2473,-10,0],p2[-0.92928,-5,0],p3[-0.085168,-1,0])"

    x = 99
    x = random.randint(0,999)
    # random.seed(x)
    np.random.seed(x)
    RooRandom.randomGenerator().SetSeed(x)
    trandom = TRandom(x)
    TRandom.SetSeed(trandom)

    # fix dilep lead
    print yellow("Fitting background: ",backgroundName)
    default["backgroundTemplate"] = dictionary.data[release][channel][backgroundName]
    # print red(default["backgroundTemplate"]); quit()
    if not fitToData:
        default["backgroundTemplate"]+=":rescale{0}".format(lumi/139)
        # don't rebin smeared mc
        if 0 or backgroundName not in dictionary.currentSmearedNames:
            default["backgroundTemplate"]+=":rebin{0}".format(rebinScale*10)
    # default["signalModel"] = "RooGaussian::signalModel(x,mean[2],std[0.5])"
    # default["backgroundTemplate"]+=":rebin{0}".format(rebinScale*10)

    # scramble input throws a toy from input
    if scrambleInput:
        default["backgroundTemplate"]+=":scramble"

    nominal = ps.extrapolation(default)

    # low = 300; high=2000; nBins=2000-300
    # binning = RooBinning(low,high,"fit")
    # binning.addUniform(nBins,low,high)
    # nominal.get("x").setBinning(binning)
    # nominal.get("backgroundModel").setNormRange("fitRange")


    # # check signal normalization (should be close to 1)
    # total=0
    # nominal.get("lambda").setVal(20)
    # for i in np.arange(300,2000):
    #     nominal.get("x").setVal(i)
    #     # total+=nominal.get("signalModel").getVal()
    #     total+=nominal.get("backgroundModel").getVal()
    #     # total+=nominal.get("model").getVal()
    # print green("Total",total)
    # quit()

    # nominal.get("lambda").setVal(12)
    # nominal.get("lambda").setVal(16)
    # nominal.get("lambda").setVal(100)
    # nominal.get("lambda").setConstant()
    # 4.93002e-05

    if inject:
        nominal.get("lambda").setVal(16)
    else:
        nominal.get("lambda").setVal(100)

    nominal.fit()

    # print red(nominal.get("backgroundNorm").getVal()); quit()

    # print "#"*50
    # while nominal.get("model").getVal()>0:
    #     nominal.get("x").setVal(nominal.get("x").getVal()*1.01)
    # print "model",nominal.get("model").getVal()
    # print "backgroundNorm",nominal.get("backgroundNorm").getVal()
    # print "backgroundModel",nominal.get("backgroundModel").getVal()
    # print "signalNorm",nominal.get("signalNorm").getVal()
    # print "signalModel",nominal.get("signalModel").getVal()
    # print "x",nominal.get("x").getVal()
    # print "#"*50

    # for l in np.arange(16,100,10):
    #     nominal.get("lambda").setVal(l)
    #     print l,nominal.get("signalNorm").getVal()
    # quit()

    if plotOn:
        path = "plots/nominalFit-sb-{0}-{1}-{2}-sb{3}{4}-{5}-{6}.png".format(interference,chirality,signalMass,useSbModel,tag,channel,backgroundName)
        # simple plot
        plotter.plot_rw_detail(nominal,useSbModel,channel=channel,lumi=lumi,path=path)
        # plotter.plot_rw(nominal,useSbModel,channel=channel,lumi=lumi,path=path)
    return nominal


def getSyst(channel,interference,func=0,lumi=0):
    # load systematics from systematics file
    pdfSs = systematics.systematics[channel][interference]["pdfSs"]
    fitSs = systematics.systematics[channel][interference]["fitSs"]
    nBkg = systematics.systematics[channel][interference]["nBkg"]
    nObs  = observed.observed[channel][interference]["nObs"]
    if not func and not lumi:
        return nBkg,pdfSs,fitSs
    chis = ["LL","LR","RL","RR"]
    funcSsAll = [abs(function_systematics.function_systematics[channel][interference][chi]["funcSs"]) for chi in chis]
    funcSs  = max(funcSsAll)
    if not lumi:
        return nObs,nBkg,pdfSs,fitSs,funcSs
    lumiSs = systematics.lumiUncert

    # when lumi is set, return:
    return nObs,nBkg,pdfSs,fitSs,funcSs,lumiSs

def getAccEff(channel,interference,func=0):
    # load acc*eff from acc*eff file
    ret = accEff.accEff[channel+"-"+interference]
    return ret
