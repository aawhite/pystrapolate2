from __future__ import division
from setup import *
from modules import *
from modules import mc as modelConstructor

# Calculate significances. Should this just go in the limit.py?

def significance(nBkg, nObs, nSig, npWidth1=1, npWidth2=1, npWidth3=1,doAcCalc=True):
    """ Signifiance and p-value calculations
        np1 = nuissance parameter width  number:1
        np2 = nuissance parameter width  number:2
        np3 = nuissance parameter width  number:3
        doAsyCalc = True  - Use asymptotic calculator
        doAsyCalc = False  - Use Frequentist calculator
    """
   
    ##################################################
    # make workspace
    ##################################################
    w = RooWorkspace("w","w")
    w.factory("x[130,0,6000]")
    w.var("x").setConstant()
    x = w.var("x")
    w.factory("nSig[1,-100,100]")

    ##################################################
    # make poisson model
    ##################################################
    w.factory("sum:nExp(nSig,prod:bkgProd(sum:bkgSum(1,np1[0,-1,1],np2[0,-1,1],np3[0,-1,1]),nBkg[{0}]))".format(nBkg))
    w.factory("Poisson:pois(nObs[0],nExp)");
    w.factory("Gaussian:g1(np1,0,npWidth1[{0}])".format(npWidth1/nBkg));
    w.factory("Gaussian:g2(np2,0,npWidth2[{0}])".format(npWidth2/nBkg));
    w.factory("Gaussian:g3(np3,0,npWidth3[{0}])".format(npWidth3/nBkg));
    w.factory("PROD:model(pois,g1,g2,g3)");

    data = RooDataSet("data","", RooArgSet(w.var("nObs")));
    w.var("nObs").setVal(nObs);
    data.add(RooArgSet(w.var("nObs")));
    # import data set in workspace and save it in a file
    getattr(w,"import")(data);

    ##################################################
    # create S+B hypothesis H1
    ##################################################
    sbModel = ModelConfig("sbModel",w);
    sbModel.SetPdf(w.pdf("model"));
    sbModel.SetParametersOfInterest(RooArgSet(w.var("nSig")));
    sbModel.SetObservables(RooArgSet(w.var("nObs")));
    sbModel.SetNuisanceParameters(RooArgSet(w.var("np1"), w.var("np2"), w.var("np3")));
    w.obj("nSig").setVal(nSig);
    sbModel.SetSnapshot(RooArgSet(w.var("nSig")));
    ##################################################

    ##################################################
    # create null hypothesis H0 (based on H)
    ##################################################
    bModel = sbModel.Clone();
    bModel.SetName("bModel");
    w.obj("nSig").setVal(0);
    bModel.SetSnapshot(RooArgSet(w.obj("nSig")));
    ##################################################

    #################################################
    # Do hypothesis tests
    ##################################################
    if doAcCalc: 
        AsymptoticCalculator.SetPrintLevel(-1);
        ac = AsymptoticCalculator(data, sbModel, bModel);
        ac.SetOneSided(True)
        asymCalcResult = ac.GetHypoTest()
        asymCalcResult.SetPValueIsRightTail(True)
        asymCalcResult.SetBackgroundAsAlt(False)
        asymCalcResult.Print()
        
        # calculate p-value and significance 
        pval = AsymptoticCalculator.GetExpectedPValues(asymCalcResult.NullPValue(), asymCalcResult.AlternatePValue(), 0, False)
        significance = ROOT.Math.normal_quantile_c(pval, 1)
    else: 
        fc = ROOT.RooStats.FrequentistCalculator(data, sbModel, bModel)
        fc.SetToys(5000,2000)
        profll = ROOT.RooStats.ProfileLikelihoodTestStat(sbModel.GetPdf())
        # profll.SetOneSidedDiscovery(True)
        
        toymcs = fc.GetTestStatSampler()
        toymcs.SetNEventsPerToy(1)
        toymcs.SetTestStatistic(profll)
        
        print 'adjusting for non-extended formalism'

        fqResult = fc.GetHypoTest()
        print yellow("==") * 25
        print yellow("Frequentist Calculator result")
        fqResult.Print()

        c = ROOT.TCanvas()
        plot = ROOT.RooStats.HypoTestPlot(fqResult)
        plot.SetLogYaxis(True)
        plot.Draw()
        c.SaveAs("test.pdf")
        pval = "##"
        significance = "##"

    # get resutls
    ret = {}
    ret["pValue"] = pval
    ret["significance"] = significance
    return ret

if __name__=="__main__":
    ### test
    from setup import *
    
    channel = "ee"
    interference = "const"
    nObs, nBkg, pdfSs, fitSs, funcSs = getSyst(channel, interference, func=1)
    nSig = nObs - nBkg

    scanDict = significance(nBkg, nObs, nSig, npWidth1=pdfSs,npWidth2=fitSs,npWidth3=funcSs)
    
    print "pValue: ",scanDict