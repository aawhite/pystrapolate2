import sys, os
from setup import *

################################################################################
# Class for converting limits on nSig in SR to limits on CI Lambda
# Example usage:
#    lambdaConvert = lambdaLimit()
#    observed = lambdaConvert.getLambdaLimit(observed,extrapMin,extrapMax,channel,model=chirality,interference=interference,lumi=lumi)
#    expected = lambdaConvert.getLambdaLimit(expected,extrapMin,extrapMax,channel,model=chirality,interference=interference,lumi=lumi)
#    uOneSig  = lambdaConvert.getLambdaLimit(uOneSig,extrapMin,extrapMax,channel,model=chirality,interference=interference,lumi=lumi)
#    uTwoSig  = lambdaConvert.getLambdaLimit(uTwoSig,extrapMin,extrapMax,channel,model=chirality,interference=interference,lumi=lumi)
#    lOneSig  = lambdaConvert.getLambdaLimit(lOneSig,extrapMin,extrapMax,channel,model=chirality,interference=interference,lumi=lumi)
#    lTwoSig  = lambdaConvert.getLambdaLimit(lTwoSig,extrapMin,extrapMax,channel,model=chirality,interference=interference,lumi=lumi)
################################################################################

def getLambda(tailMin,limit):
    file2 = ROOT.TFile("../data/CIRW.root")
    DY = file2.Get("h_ee_RecoMass_Lin")
    CI = file2.Get("CI_etaLR_const_mass_reco")
    lowMass_bin = DY.GetXaxis().FindBin(tailMin*1000)
    highMass_bin = DY.GetXaxis().FindBin(6000)
    y_bins = []
    ey     = [ROOT.Double(0)]*8
    ex     = [ROOT.Double(0.000001)]*10
    x_bins = [0.0,(1.0/(40*40)),(1.0/(35*35)),(1.0/(30*30)),(1.0/(25*25)),(1.0/(20*20)),(1.0/(15*15)),(1.0/(10*10))]
    y_bins.append(0)
    y_bins.append(CI.IntegralAndError(CI.GetXaxis().FindBin(40.0), CI.GetXaxis().FindBin(40.0), lowMass_bin, highMass_bin, ey[1]) - DY.IntegralAndError(lowMass_bin, highMass_bin, ey[0]))
    y_bins.append(CI.IntegralAndError(CI.GetXaxis().FindBin(35.0), CI.GetXaxis().FindBin(35.0), lowMass_bin, highMass_bin, ey[2]) - DY.IntegralAndError(lowMass_bin, highMass_bin, ey[0]))
    y_bins.append(CI.IntegralAndError(CI.GetXaxis().FindBin(30.0), CI.GetXaxis().FindBin(30.0), lowMass_bin, highMass_bin, ey[3]) - DY.IntegralAndError(lowMass_bin, highMass_bin, ey[0]))
    y_bins.append(CI.IntegralAndError(CI.GetXaxis().FindBin(25.0), CI.GetXaxis().FindBin(25.0), lowMass_bin, highMass_bin, ey[4]) - DY.IntegralAndError(lowMass_bin, highMass_bin, ey[0]))
    y_bins.append(CI.IntegralAndError(CI.GetXaxis().FindBin(20.0), CI.GetXaxis().FindBin(20.0), lowMass_bin, highMass_bin, ey[5]) - DY.IntegralAndError(lowMass_bin, highMass_bin, ey[0]))
    y_bins.append(CI.IntegralAndError(CI.GetXaxis().FindBin(15.0), CI.GetXaxis().FindBin(15.0), lowMass_bin, highMass_bin, ey[6]) - DY.IntegralAndError(lowMass_bin, highMass_bin, ey[0]))
    y_bins.append(CI.IntegralAndError(CI.GetXaxis().FindBin(10.0), CI.GetXaxis().FindBin(10.0), lowMass_bin, highMass_bin, ey[7]) - DY.IntegralAndError(lowMass_bin, highMass_bin, ey[0]))
    x_arr = np.array(x_bins)
    y_arr = np.array(y_bins)
    ey_err = np.array(ey)
    ex_err = np.array(ex)
    g_Fit = ROOT.TGraphErrors(8,x_arr,y_arr,ex_err,ey_err)
    func = ROOT.TF1("CI_1000_6000" ,"[0]+[1]*x+[2]*x*x",0.0,0.01);
    g_Fit.Fit(func)
    for x in limit:
        print func.GetX(x)
    lambdaVals = [(1/func.GetX(x))**0.5 for x in limit]
    return lambdaVals

def interp(y,x1,x2,y1,y2):
    """ Linear interpolation 
        Returns x(y) based on points (x1,y1) -- (x2,y2)
    """
    if x1==x2: return None
    slope = (y2-y1)/(x2-x1)
    return (y-y1)/slope + x1

class lambdaLimit:
    """ Class to do the limit conversion.
        Once run, it saves the lambda's into a dictionary to save time
        Furthermore, it pickles its data, so it can be used next time
        Data structure: sigYields[srMin][srMax][histName]
    """
    def __init__(self):
        # try loading from pickle
        # self.picklePath = "pickle/sigYields.pickle"
        # self.sigPath = "../data/ci/templates_r21_CHANNEL_new/CI_Template_MODEL.root"
        # previous
        self.sigPath = "../../SharedInputsProj/rel21_ci/templates_r21_CHANNEL_new/CI_Template_MODEL.root"
        # new from deshan
        self.sigPath = "../../SharedInputsProj/rel21_ci/templates_r21_CHANNEL_august/CI_Template_MODEL.root"

        self.scale = 1 # scale for above signal hists
        try: 
            self.sigYields = pickle.load(open(self.picklePath,"r"))
        except:
            self.sigYields = {}

    def _interp(self,y,x1,x2,y1,y2):
        """ Linear interpolation 
            Returns x(y) based on points (x1,y1) -- (x2,y2)
        """
        if x1==x2: return None
        slope = (y2-y1)/(x2-x1)
        return (y-y1)/slope + x1

    def __del__(self):
        """ Destructor. On close, pickle data into file """
        #output = open(self.picklePath,"w")
        #pickle.dump(self.sigYields,output)

    def _calcYield(self,srMin,srMax,histName,histPath):
        """ Calculate yield from scratch 
            use self.sigPath
        """
        try:
            # print yellow("hist path", histPath)
            f = TFile.Open(histPath)
            hist = f.Get(histName)
            minBin = hist.FindBin(srMin*self.scale)
            maxBin = hist.FindBin(srMax*self.scale)
            yld = hist.Integral(minBin,maxBin)
            f.Close()
        except:
            raise BaseException(red("Failed to load hist. Check:",histPath,histName))
        return yld

    def _getYield(self,srMin,srMax,histName,histPath):
        """ Return sig yield 
            srMin,srMax: start and stop of signal region
            sigModel: name of histogram to integrate
        """
        sy = self.sigYields
        if srMin not in sy.keys(): sy[srMin] = {}
        if srMax not in sy[srMin].keys(): sy[srMin][srMax] = {}
        if histName not in sy[srMin][srMax].keys():
            sy[srMin][srMax][histName] = self._calcYield(srMin,srMax,histName,histPath)
        # now this has been calculated or already exists, return it
        return sy[srMin][srMax][histName]

    def getLambdaLimit(self,nSigLimit,srMin,srMax,channel,model="LL",interference="const",lumi=None):
        """ Calculate lower limit on Lambda via upper limit on nSig
            Used signal hist interpolation 
        """
        if lumi==None:
            raise BaseException("Need to provide lumi to getLambdaLimit")
        # loop over lambdas
        nSigVals = {}
        prevLambd = None
        for lambd in range(12,100,2):

            histName = "diff_CI_{0}_{1}_{2}_TeV".format(model,interference,lambd)
            path = self.sigPath.replace("MODEL",model)
            histPath = path.replace("CHANNEL",channel)

            # print histPath
            nSig = self._getYield(srMin,srMax,histName,histPath)
            nSig*=lumi/80.5 # divide by signal hist lumi
            nSigVals[lambd] = nSig
            if nSig<nSigLimit:
                if prevLambd==None:
                    print red("No Histogram Found (LOW) for nSigLimit={0} in range {1}-{2}".format(nSigLimit,srMin,srMax))
                    return "low"
                lambdLimit = interp( nSigLimit,
                                     x1=prevLambd,x2=lambd,
                                     y1=nSigVals[prevLambd],y2=nSigVals[lambd]
                                   )
                return lambdLimit
            prevLambd = lambd
        # The limit setting failed, so the highest mass lambda is the highest "excluded"
        # This shouldn't happen, but I won't raise an exception since nothing went wrong in the function
        print red("No Histogram Found (HIGH) for nSigLimit={0} in range {1}-{2}".format(nSigLimit,srMin,srMax))
        print red("Best option was",lambd,nSig,nSigLimit)
        return "high"
        # return np.nan




