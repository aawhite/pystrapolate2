Pystrapolate: the Non-resonant dilepton framework
=================================================
Aaron White (CERN, 2019)

```
    ______  _____        _______  _____  _______  _____  __   _ _______
    |     \   |   |      |______ |_____]    |    |     | | \  | |______
    |_____/ __|__ |_____ |______ |          |    |_____| |  \_| ______|

        ___  _   _ ____ ___ ____ ____ ___  ____ _    ____ ___ ____
        |__]  \_/  [__   |  |__/ |__| |__] |  | |    |__|  |  |___
        |      |   ___]  |  |  \ |  | |    |__| |___ |  |  |  |___
```

Get started quickly
===================

Run ``analysis/singleLimit.py``, the script has default parameters, and will produce some plots.

The output should be saved to ``analysis/limits/`` and ``analysis/plots/`` 

Running the limits
==================
- Limits run in `analysis/` are produced by `finalLimit.py` which can be run via `finalLimitDriver.sh`. This saves the limit information in `analysis/limits` as picked files
- To make tables and plots with these outputs, use `analysis/multiLimitPlotter-data.py` and comment/uncomment the functions at the bottom of the script

Files
=====
- **analysis/:** scripts for running the analysis
    - The main script to run is ``singleLimit.py`` which is intended to be run using CLI arguements:
        - Nominal fit
        - Fit to toys to get fitSS
        - Fit to PDF variations to get pdfSS
        - Perform limit setting (n-events only)
    - ``multiLimitDriver.py`` runs ``singleLimit.py`` in 4 threads for each signal model
    - ``multiLimitPlotter.py`` plots the limits stored in ``./limits``
    - ``crosscheck-bOnly-sb.py`` performs a comparison between the B-only and S+B background models, including fitSS and pdfSS. Also writes the function uncertainty to `settings/function_systematics.py` [Example](#bComparison)
    - ``crosscheck-signalInjection.py`` performs a signal injection test for the S+B model to check the linearity of signal recovered vs injected. Error bands used are from the B-only background estimate. [Example](#injections)
    - ``generateSystematics.py`` run systematics fits on MC, and save to ``settings/systematics.py``
    - ``systematicsTable.py``, makes a nice table of the systematics
- **settings/:** analysis level settings for things like
    - `dictionary.py`
        - what background PDF is used
        - what MC or data is used
        - which PDF variations are considered
        - The CR and SR definitions
    - `observed.py`
        - Observed numbers of events in the SR's
    - `systematics.py`, written by `analysis/generateSystematics.py`
        - The CR uncertainty
        - The SS uncertainty
    - `function_systematics.py`, written by `analysis/crosscheck-bOnly-sb.py`
        - The function choice uncertainty
- **source/:** backend files:
    - ``pystrap.py``: Pystrapolate driver
    - ``plotter.py``: Plotting
    - ``roowrap.py``: Make matplotlib plots easier
    - ``modules.py``: ROOT and RooFit modules to be imported by ``setup.py`` and other backend classes 
    - ``setup.py``: Collection of functions and libraries to be imported by frontend scripts (like ``singleLimit.py``)
    - ``morphedSignal.h``: Morphed signal model and normalization (called by ``modelConstructor.py``)
    - ``modelConstructor.py``: Function for loading histograms and launching the morphed model 
    - ``limits.py`` limit setting function (n-events only). This can be run stand-alone as an example/test
    - ``lambdaConversion.py`` convert limits on n-events to limits on Lambda
    - (to be added) limit conversion from N signal events to Lambda
- **tests/:** unit tests for validating the framework
    - The key tests are intended to check the core functionality:
        - ``unittest-bFit.py``, which performs b-only fits on MC for various mass ranges, and outputs control plots. [Example](#bFitControl)
        - ``unittest-sbFit.py``, which performs sb-only fits on MC for signal models, and outputs control plots. [Example](#sbFitControl)
        - ``unittest-signalShapes.py``, which performs a comparison between the morphed signal PDF from ``morphedSignal.h`` and the corresponding templates. [Example](#signalShapes)
    - These can be checked against the expected control plots below
    - Further tests on particular aspects of the model are less user friendly, and have names ``simple...py``

Control plots
=============

Morphed signal shapes 
------------------------
Comparison to TH1 histograms. The thick lines are the TH1's, the thin lines are the corresponding morphed PDF (unit-normalized) multiplied by the morphed normalization
<a name="signalShapes"></a>
![In urxvt](doc/signalShapes.png)

B-only vs S+B background estimates 
----------------------------------
Comparison run on data between the B-only and S+B background estimates. Error bars are from PDF-SS and Fit-SS
<a name="bComparison"></a>
![In urxvt](doc/bComparison.png)

S+B Signal Injection Linearity
------------------------------
Run on MC with injected signal. These show the signal recovered vs the signal injected for different Lambda mass points. The error bar is the error on the recovered signal, which comes from the B-only background uncertainty
<a name="injections"></a>
![In urxvt](doc/injections.png)

S+B fit control plots 
---------------------
<a name="sbFitControl"></a>
![In urxvt](doc/sbFitControl.png)

B-only fit control plots 
------------------------
<a name="bFitControl"></a>
![In urxvt](doc/bFitControl.png)

Changes
-------
v2.3

