from __future__ import division

################################################################################
# Cross check B-only vs S+B background models
# Comparison includes SS uncertainties
# Output is a control plot
################################################################################

import sys
sys.path = ["../source"] + sys.path
from setup import *
import plotter

def doFit(fitToData=False,useSbModel=None):
    """
    ############################## 
    #    1) Do nominal fit
    #    2) Do toy fits
    #    3) Do PDF fits
    #    4) Do fit to "data"
    ############################## 
    """ 
    fitSs=0
    pdfSs=0
    chi = "LL"
    fitMin = dictionary.ranges[channel][interference][chi]["fitMin"]
    fitMax = dictionary.ranges[channel][interference][chi]["fitMax"]
    extrapMin = dictionary.ranges[channel][interference][chi]["extrapMin"]
    print fitMin, fitMax, extrapMin

    ############################### 
    ## do fit
    ############################### 
    #nominalMc = fitMacro(
    #                    interference=interference,
    #                    chirality=chirality,
    #                    channel=channel,
    #                    lumi=lumi,fitMin=fitMin,
    #                    fitMax=fitMax,extrapMin=extrapMin,
    #                    extrapMax=extrapMax,inject=inject,
    #                    signalMass=signalMass,
    #                    release=release,
    #                    plotOn=0,fitOn=fitOn,
    #                    useSbModel=useSbModel,
    #                    backgroundName="backgroundTemplate",
    #                 )
    #yields = nominalMc.yields()
    #nBkg = yields["nBkg"]
    #nominalSs = yields["nSpur"]
    #print green("nBkg",nBkg)
    ############################### 
    ## toys
    ############################### 
    #for i in range(nToys):
    #    nominalMc.fitToy(saveToy=0)
    #fitSsDist = nominalMc.toyFitDiff()
    #fitSs = np.array(fitSsDist).std()
    ############################### 
    ## fit PDF uncertainties
    ############################### 
    #pdfVarYields={}
    #loopNames = dictionary.pdfNames[release]
    #loopNames.append("backgroundTemplate")
    #for i, pdfName in enumerate(loopNames):
    #    if i==nPdfs: break
    #    print green("Fitting pdf variaiton",pdfName)
    #    pdfVariation = fitMacro(interference=interference,
    #                  chirality=chirality,
    #                  channel=channel,
    #                  lumi=lumi,fitMin=fitMin,
    #                  fitMax=fitMax,extrapMin=extrapMin,
    #                  extrapMax=extrapMax,inject=inject,
    #                  signalMass=signalMass,
    #                  release=release,
    #                  useSbModel=useSbModel,
    #                  plotOn=0,fitOn=fitOn,
    #                  backgroundName=pdfName,
    #                 )
    #    pdfVarYields[pdfName]=pdfVariation.yields()
    #pdfSsDist = [abs(pdfVarYields[y]["nSpur"]) for y in pdfVarYields.keys()]
    #if len(pdfSsDist):
    #    pdfSs = max(pdfSsDist)
    #    pdfSs = max(pdfSs,nominalSs)

    # load systematics from systematics file
    pdfSs = systematics.systematics[channel][interference]["pdfSs"]
    fitSs = systematics.systematics[channel][interference]["fitSs"]

    ############################## 
    # do fit to "data"
    ############################## 
    if fitToData: backgroundName="data"
    else: backgroundName="backgroundTemplate"
    nominal = fitMacro(
                        interference=interference,
                        chirality=chirality,
                        channel=channel,
                        lumi=lumi,fitMin=fitMin,
                        fitMax=fitMax,extrapMin=extrapMin,
                        extrapMax=extrapMax,inject=inject,
                        signalMass=signalMass,
                        release=release,
                        plotOn=0,fitOn=fitOn,
                        useSbModel=useSbModel,
                        backgroundName=backgroundName,
                        fitToData=fitToData,
                     )
    yields = nominal.yields()
    nominalSs = yields["nSpur"]
    nBkg = yields["nBkg"]
    print green("nBkg",nBkg)

    # quit()

    return nBkg,pdfSs,fitSs

# default inputs
interference = "const"
chirality = "LL"
channel = "ee"
release = "21"
lumi = 139
signalMass = 20
inject = 0

quick = True
# quick = False

if quick:
    nPdfs=0
    nToys=0
else:
    nToys=50
    nPdfs=200

fitMin = 300
fitMax = 2000
# fitMax = 1500
# fitMax = 4000
extrapMin = fitMax
extrapMax = 6000

os.popen("rm plots/*")
fitOn = 1

results = defaultdict(lambda:defaultdict(lambda:defaultdict(dict)))

# for channel in ["ee"]:
for channel in ["mm"]:
# for channel in ["ee","mm"]:
    # for interference in ["const","dest"]:
    for interference in ["dest"]:
    # for interference in ["const"]:
        # for chirality in ["LL","LR","RL","RR"]:
        # for chirality in ["LL","RR"]:
        for chirality in ["LL"]:
        # for chirality in ["LR"]:

            fitToData=True
            # fitToData=False

            # chirality = "LL"
            sb_nBkg,sb_pdfSs,sb_fitSs = doFit(fitToData=fitToData,useSbModel=True)
            # print red(sb_nBkg)
            # quit()
            b_nBkg,b_pdfSs,b_fitSs = doFit(fitToData=fitToData,useSbModel=False)
            systDict = {}
            systDict["b_nBkg"]  = b_nBkg
            systDict["sb_nBkg"] = sb_nBkg
            systDict["funcSs"]  = sb_nBkg-b_nBkg
            results[channel][interference][chirality] = systDict
            # continue

            path = "plots/nbkg-{0}-{1}-{2}.png".format(chirality,interference,channel)
            label = r"{0} {1}, ${2}$ channel".format(chirality,interference,channel.replace("mm","\mu\mu"))
            plotter.backgroundEstimateCompare(b_pdfSs=b_pdfSs,b_fitSs=b_fitSs,b_nBkg=b_nBkg,sb_pdfSs=sb_pdfSs,sb_fitSs=sb_fitSs,sb_nBkg=sb_nBkg,
                                              path=path,label=label
                                             )
            # print yellow(b_nBkg,sb_nBkg)
        results[channel][interference] = dict(results[channel][interference])
    results[channel] = dict(results[channel])
results = dict(results)


rewriteOn = False
systematicsPath="../settings/function_systematics.py"

print systematics
if rewriteOn:
    f=open(systematicsPath,'w')
    f.write("import numpy as np\n")
    f.write("nan = np.nan\n\n")
    f.write("function_systematics = {0}\n\n".format(repr(results)))
    print green("Write results to {0}\n".format(systematicsPath))
print green("DONE")
