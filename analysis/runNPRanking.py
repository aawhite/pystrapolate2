from __future__ import division
import sys, os

sys.path = [os.path.dirname(os.path.abspath(__file__)) + "/../source"] + sys.path
from setup import *
import makeNPRanking

def makeWs(outFile, nBkg, nObs, npWidth1=1, npWidth2=1, npWidth3=1):
    w = RooWorkspace("w", "w")
    w.factory("x[0]")

    lumi = 139
    w.factory("sum:nExp(nSig[0,-100,100],prod:bkgProd(sum:bkgSum(1,np1[0,-1,1],np2[0,-1,1],np3[0,-1,1]),nBkg[{0}]))".format(nBkg))
    w.factory("Poisson:pois(nObs[0],nExp)");
    w.factory("Gaussian:g1(np1,0,npWidth1[{0}])".format(npWidth1 / nBkg));
    w.factory("Gaussian:g2(np2,0,npWidth2[{0}])".format(npWidth2 / nBkg));
    w.factory("Gaussian:g3(np3,0,npWidth3[{0}])".format(npWidth3 / nBkg));
    # Gaussian constraint for Lumi uncertainty  1.7% holding on off this until we decide on nSig or not..
    # w.factory("Gaussian:g4(np4,0,npWidth4[{0}])".format(0.017))
    w.factory("PROD:model(pois,g1,g2,g3)")

    # print w.obj("np1Width2").getVal()
    w.defineSet("nps", "np1,np2,np3")
    # w.defineSet("globs","nExp")
    ###
    mc = ModelConfig("sbModel",w);
    mc.SetPdf(w.pdf("model"));
    mc.SetParametersOfInterest(RooArgSet(w.var("nSig")));
    mc.SetObservables(RooArgSet(w.var("nObs")));
    mc.SetNuisanceParameters(RooArgSet(w.set("nps")));

    # these are needed for the hypothesis tests
    mc.SetSnapshot(RooArgSet(w.var("nSig")));
    mc.SetGlobalObservables(""); # removed etienne
    mc.Print();
    # import model in the workspace
    getattr(w,"import")(mc);
    # make data set with the number of observed events
    w.var("nObs").setVal(nObs);
    data = RooDataSet("data","", RooArgSet(w.var("nObs")));
    data.add(RooArgSet(w.var("nObs")));
    # import data set in workspace and save it in a file
    getattr(w,"import")(data);
    # ##################################################
    sbModel = mc;

    # create null hypothesis H0 (based on H)
    bModel = sbModel.Clone();
    bModel.SetName("bModel");
    bModel.SetSnapshot((RooArgSet(w.var("nSig"))));
    # fix nSig value to 0
    nSigNoSig = bModel.GetParametersOfInterest().first();
    nSigNoSig.setVal(0);
    # nSigNoSig.setConstant();
    bModel.SetSnapshot(RooArgSet(nSigNoSig));

    w.SaveAs("workspaces/{0}".format(outFile))

channels = {"ee","mm"}
interferences = {"const","dest"}
nSyst = 3

os.popen("mkdir workspaces; mkdir output")
os.popen("rm plots/*")

for channel in channels:
    for interference in interferences:
        wsFile = "{0}_{1}_workspace.root".format(channel, interference)
        nObs, nBkg, pdfSs, fitSs, funcSs = getSyst(channel, interference, func=1)

        makeWs(wsFile,nBkg,nObs,fitSs,pdfSs,funcSs)

        makeNPRanking.makeNPRanking("workspaces/", wsFile, varSign="up", when="postfit")
        makeNPRanking.makeNPRanking("workspaces/", wsFile, varSign="down", when="postfit")
        makeNPRanking.makeNPRanking("workspaces/", wsFile, varSign="up", when="prefit")
        makeNPRanking.makeNPRanking("workspaces/", wsFile, varSign="down", when="prefit")

        os.popen("root -l '../source/plotNPranking.C(\"{0}\",  \"{1}\", \"{2}\", \"{3}\", {4})'".format(channel,interference,wsFile, "nSig", nSyst))

