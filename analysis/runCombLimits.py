#! /usr/bin/env python2

from __future__ import division
import sys,os
# sys.path = [os.path.dirname(__file__)+"/../source"] + sys.path
sys.path = [os.path.dirname(os.path.abspath(__file__))+"/../source"] + sys.path
from setup import *
import plotter

################################################################################
#   Basic fit/limit setting using pre-recorded limits from settings
################################################################################ 

def doFit(channel,interference):

    chi="LL"
    fitMin = dictionary.ranges[channel][interference][chi]["fitMin"]
    fitMax = dictionary.ranges[channel][interference][chi]["fitMax"]
    extrapMin = dictionary.ranges[channel][interference][chi]["extrapMin"]
    extrapMax = 6000
    name = "{0}-{1}".format(channel,interference)
    tag ="-"+name


    ################################################################################
    # Data fit
    ################################################################################
    nominal = fitMacro(
                        interference=interference,
                        chirality=chi,
                        channel=channel,
                        lumi=lumi,fitMin=fitMin,
                        fitMax=fitMax,extrapMin=extrapMin,
                        extrapMax=extrapMax,inject=inject,
                        signalMass="12",
                        release=release,
                        plotOn=0,fitOn=1,
                        useSbModel=0,
                        backgroundName="data",
                        fitToData=1,
                        # backgroundName="backgroundTemplate",
                     )
    yields = nominal.yields()

    nBkg = yields["nBkg"] # nominal fit background expected
    nObs = yields["nObs"]  # nominal fit observed
    
    return nObs, nBkg

def doCombLim():
    channel = "ll"

    ################################################################################
    # limit setting (on signal region)
    ################################################################################
    limitResult = {}
    for interference in ["const","dest"]:
        for chirality in ["LL", "LR", "RL", "RR"]:
            # data yeilds
            nObs_ee, nBkg_ee = doFit("ee", interference)
            nObs_mm, nBkg_mm = doFit("mm", interference)
            # systematics
            nObs_ee, nBkgMc_ee, pdfSs_ee, fitSs_ee, funcSs_ee = getSyst("ee", interference, func=1)
            nObs_mm, nBkgMc_mm, pdfSs_mm, fitSs_mm, funcSs_mm = getSyst("mm", interference, func=1)

            name = "{0}-{1}-{2}".format("ll", interference, chirality)
            tag = "-" + name
            limitResult[name] = limits.poissonCombLimit3l(nBkg_ee, nObs_ee, nBkg_mm, nObs_mm, chirality=chirality, interference=interference,
                                                    npWidth1_ee=pdfSs_ee, npWidth2_ee=fitSs_ee, npWidth3_ee=funcSs_ee,
                                                    npWidth1_mm=pdfSs_mm, npWidth2_mm=fitSs_mm, npWidth3_mm=funcSs_mm,
                                                )

            print yellow("="*50)
            print yellow("Limit results ll",interference,chirality)
            # print "Expected background: {0}, observed: {1}".format(nBkg,nObs)
            print "limit                    :  Value "
            print "Limit on Lambda          : ", limitResult[name]["lambda_upperLimit"]
            print "Expected limit on Lambda : ", limitResult[name]["lambda_expLimit"]
            print "Lambda +1sigma           : ", limitResult[name]["lambda_uOneSig"]
            print "Lambda -1sigma           : ", limitResult[name]["lambda_lOneSig"]
            print "Lambda +2sigma           : ", limitResult[name]["lambda_uTwoSig"]
            print "Lambda -2sigma           : ", limitResult[name]["lambda_lTwoSig"]
            
            print yellow("="*50)

            # additional info
            limitResult[name]["name"] = name
            limitResult[name]["chirality"] = chirality
            limitResult[name]["interference"] = interference
            limitResult[name]["channel"] = channel
            limitResult[name]["interference"] = interference
            limitResult[name]["release"] = release
            limitResult[name]["tag"] = tag
            limitResult[name]["lumi"] = lumi
            limitResult[name]["nBkg_ee"] = nBkg_ee
            limitResult[name]["nBkg_mm"] = nBkg_mm

    return limitResult

################################################################################
# DEFAULT INPUTS
# When run without inputs, these are used
################################################################################
release = "21"
inject = 0
lumi = 139

# load inputs

print green("Running combined limits")
limitResult = doCombLim()

picklePath = "limits/singleLimit-ll.pickle"
pickle.dump(limitResult,open(picklePath,"w"))
print green("Saving result to",picklePath)

print green("DONE")

