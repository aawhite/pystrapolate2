import sys
sys.path = ["../source"] + sys.path
from setup import *

##################################################
# Unblind SR
##################################################


class histInt:
    def __init__(self,th1):
        self.th1 = th1
        self.name = th1.GetName()
        self.nBins = th1.GetNbinsX()
        self.lo = th1.GetBinLowEdge(1)
        self.hi = th1.GetBinLowEdge(self.nBins+1)
        # self.lo = 300
        # self.hi = 6000
        # loBin = th1.FindBin(self.lo)
        # hiBin = th1.FindBin(self.hi)
        self.width = th1.GetBinLowEdge(2)-th1.GetBinLowEdge(1)
        # self.y = np.array([th1.Integral(i,self.nBins) for i in range(loBin,hiBin)])

        self.y = np.array([th1.GetBinContent(i) for i in range(1,self.nBins+1)])
        self.x = np.array([th1.GetBinCenter(i) for i in range(1,self.nBins+1)])

        # self.y = np.array([th1.Integral(i,self.nBins) for i in range(1,self.nBins+1)])
        # self.x = np.array([th1.GetBinCenter(i) for i in range(1,self.nBins+1)])


    def __str__(self):
        # return "histInt {0} nBins={1}x{2} [{3},{4}]".format(self.name,self.nBins,self.width,self.x[0],self.x[-1])
        return "histInt {0} nBins={1} [{2},{3}]".format(self.name,self.nBins,self.lo,self.hi)

def loadHisto(path,name,scale=1):
    print "-"*50
    print green("Load",name)
    print path
    f = TFile.Open(path)
    # f.Print()
    h = f.Get(name)
    # h.Print()
    # print "Bin Width",h.GetBinWidth(1)
    # print "Min",h.GetBinLowEdge(1)
    # print "Max",h.GetBinLowEdge(h.GetNbinsX()+1)
    # print "Scaling",scale
    h.Scale(scale)

    # rebin and re-scale
    new = TH1F(name,name,57*2,300,6000)
    for i in range(1,h.GetNbinsX()+1):
        c = h.GetBinCenter(i)
        v = h.GetBinContent(i)
        new.Fill(c,v)
    ret = histInt(new)
    print yellow(ret)
    return ret

output={}
srMax = 6000

# for channel in ["mm"]:
for channel in ["ee","mm"]:
    iPathMc = "../../SharedInputsProj/rel21_templates/merged_{0}.root".format(channel)
    iPathMcNorm = "../../SharedInputsProj/rel21_templates-dataNormed/merged_{0}-dataNormed.root".format(channel)
    iPathData = "../../SharedInputsProj/rel21_dataTemplates/mInv_spectrum_combined_ll_rel21_fullRun2.root"
    iPathSig = "../../SharedInputsProj/rel21_ci/templates_r21_{0}_new/CI_Template_LL.root".format(channel)
    nameFakes  = "{0}_201516fakes".format(channel)
    nameTtbar  = "{0}_diboson_smoothTTbar".format(channel)
    nameDy     = "{0}_diboson_smoothDY".format(channel)
    nameDiboson= "{0}_dibosonNoOutliers".format(channel)
    if channel=="ee":
        nameMc     = "ee_dibosonNoOutliers_201516fakes_smoothDY_smoothTTbar"
    else:
        nameMc     = "mm_dibosonNoOutliers_dilepWt_smoothDY_smoothTTbar"
    nameData   = "mInv_spectrum_201518_{0}_1GeV".format(channel)


    bkgScale = 1
    if channel=="ee": fakes   = loadHisto(iPathMc,nameFakes)
    ttbar   = loadHisto(iPathMc,nameTtbar,scale=bkgScale)
    dy      = loadHisto(iPathMc,nameDy,scale=bkgScale)
    diboson = loadHisto(iPathMc,nameDiboson,scale=bkgScale)
    mc      = loadHisto(iPathMcNorm,nameMc,scale=bkgScale)
    data    = loadHisto(iPathData,nameData)

    # print mc.x
    # intInFull = sum(mc.y)
    # intInSr = sum(mc.y[mc.x>2070])
    # print red("intInSr",intInSr)
    # print red("intInFull",intInFull)
    # quit()

    for interference in ["const","dest"]:
    # for interference in ["const"]:
        srMin = dictionary.ranges[channel][interference]["LL"]["extrapMin"]
        # srMin=400
        column = {}
        column["0DY"]="{0:.3f}".format(sum(dy.y[mc.x>srMin]))
        column["1ttbar"]="{0:.3f}".format(sum(ttbar.y[mc.x>srMin]))
        column["2Diboson"]="{0:.3f}".format(sum(diboson.y[mc.x>srMin]))
        if channel=="ee":column["3Fakes"]="{0:.3f}".format(sum(fakes.y[mc.x>srMin]))
        else: column["3Fakes"]="    "
        column["4MC Total"]="{0:.3f}".format(sum(mc.y[mc.x>srMin]))
        column["5Data"]="{0:.3f}".format(sum(data.y[data.x>srMin]))
        column["6StatSig"]="{0:.3f}".format((sum(data.y[mc.x>srMin])-sum(mc.y[mc.x>srMin]))/sqrt(sum(mc.y[mc.x>srMin])))
        output["{0}-{1}".format(channel,interference)]=column

columns = output.keys()
columns = sorted(columns,reverse=0)
rows = output[columns[0]].keys()
rows = sorted(rows)
print r"\begin{tabular}{r l l l l l}\toprule"
print r"{0} & {1} \\".format("","& ".join(columns))
for row in rows:
    line = "{0} \t {1} ".format(row[1:],"\t ".join([output[c][row] for c in columns]))
    # line = r"{0} & {1} \\".format(row[1:],"& ".join([output[c][row] for c in columns]))
    print line
print r"\bottomrule\end{tabular}"
