import sys
sys.path = ["../source"] + sys.path
from setup import *

################################################################################
# Make plots of all the limits in ./limits/
# Also make latex tables
################################################################################

def lcHelp(limitDict):
    """ Convert limit into lambda limit """
    # lambda conversion
    import lambdaConversion as lc
    lambdaConvert = lc.lambdaLimit() # class to calculate limits

    expLimit     = limitDict["expLimit"]
    extrapMin    = limitDict["extrapMin"]
    extrapMax    = limitDict["extrapMax"]
    channel      = limitDict["channel"]
    chirality    = limitDict["chirality"]
    interference = limitDict["interference"]
    lumi         = limitDict["lumi"]
    lamLimit     = lambdaConvert.getLambdaLimit(expLimit,extrapMin,extrapMax,channel,model=chirality,interference=interference,lumi=lumi)
    return lamLimit


def makeTableLimits(limitDicts):
    """ Make table of limits """

    # sorted keys
    table    = "\\begin{tabular}{r l l l l l l l l l}\\toprule\n"

    for iInt,interference in enumerate(["const","dest"]):
        names       = sorted(limitDicts.keys(),key=lambda x: "LL.LR.RL.RR".find(x.split("-")[2])+100*("dest" in x))
        names       = [x for x in names if interference in x]
        pdfSs       = ["{0:.2f}".format(limitDicts[n]["pdfSs"]) for n in names]
        fitSs       = ["{0:.2f}".format(limitDicts[n]["fitSs"]) for n in names]
        expLimit    = ["{0:.2f}".format(limitDicts[n]["expLimit"]) for n in names]
        lamLimit    = ["{0:.2f}".format(lcHelp(limitDicts[n])) for n in names]
        nBkg        = ["{0:.2f}".format(limitDicts[n]["nBkg"]) for n in names]
        nBkgMc      = ["{0:.2f}".format(limitDicts[n]["nBkgMc"]) for n in names]
        # build table
        table   += "       & "+" & ".join(names).replace("ee-","")+"\\\\\n"
        table   += "Limit (N Sig)  & "+" & ".join(expLimit)+"\\\\\n"
        table   += "Limit ($\\Lambda$ TeV)  & "+" & ".join(lamLimit)+"\\\\\n"
        table   += "PDF SS & "+" & ".join(pdfSs)+"\\\\\n"
        table   += "Fit SS & "+" & ".join(fitSs)+"\\\\\n"
        table   += "N Bkg  & "+" & ".join(nBkgMc)+"\\\\\n"
        if iInt!=1: table += "\\hline\n"

    table   += "\\bottomrule\\end{tabular}"
    print table


iPaths = "limits/single*"
os.popen("rm plots/*")

# load limitDicts
iPaths = glob.glob(iPaths)
limitDicts = {}
for iPath in iPaths:
    print iPath
    limitDict = pickle.load(open(iPath))
    limitDicts[limitDict["name"]] = limitDict

# # call limit plot
makeTableLimits(limitDicts)

# plotter.limitPlot_nSig(limitDicts)
# plotter.limitPlot_nSig(limitDicts,nsig=False)

plotter.yieldsPlot(limitDicts,nsig=False)

# plotter.pdfSsRatioPlot(limitDicts)
# plotter.fitSsRatioPlot(limitDicts)

