import sys
sys.path = ["../source"] + sys.path
from setup import *

################################################################################
# Make plots of all the limits in ./limits/
# Also make latex tables
################################################################################

def lcErrLimit(limitDict,chirality):
    """ Convert limit into lambda limit """
    # lambda conversion
    import lambdaConversion as lc
    lambdaConvert = lc.lambdaLimit() # class to calculate limits
    limitU     = limitDict["uOneSig"]
    limitD     = limitDict["lOneSig"]
    extrapMin    = limitDict["extrapMin"]
    extrapMax    = limitDict["extrapMax"]
    channel      = limitDict["channel"]
    interference = limitDict["interference"]
    lumi         = limitDict["lumi"]
    up     = lambdaConvert.getLambdaLimit(limitU,extrapMin,extrapMax,channel,model=chirality,interference=interference,lumi=lumi)
    dn     = lambdaConvert.getLambdaLimit(limitD,extrapMin,extrapMax,channel,model=chirality,interference=interference,lumi=lumi)
    return {"up":up,"dn":dn}

def lcExpLimit(limitDict,chirality):
    """ Convert nSig limit into lambda limit """
    # lambda conversion
    import lambdaConversion as lc
    lambdaConvert = lc.lambdaLimit() # class to calculate limits
    limit     = limitDict["nSig_expLimit"]
    extrapMin    = limitDict["extrapMin"]
    extrapMax    = limitDict["extrapMax"]
    channel      = limitDict["channel"]
    interference = limitDict["interference"]
    lumi         = limitDict["lumi"]
    lamLimit     = lambdaConvert.getLambdaLimit(limit,extrapMin,extrapMax,channel,model=chirality,interference=interference,lumi=lumi)
    return lamLimit

def lcObsLimit(limitDict,chirality):
    """ Convert nSig limit into lambda limit """
    # lambda conversion
    import lambdaConversion as lc
    lambdaConvert = lc.lambdaLimit() # class to calculate limits
    limit     = limitDict["nSig_upperLimit"]
    extrapMin    = limitDict["extrapMin"]
    extrapMax    = limitDict["extrapMax"]
    channel      = limitDict["channel"]
    interference = limitDict["interference"]
    lumi         = limitDict["lumi"]
    lamLimit     = lambdaConvert.getLambdaLimit(limit,extrapMin,extrapMax,channel,model=chirality,interference=interference,lumi=lumi)
    return lamLimit


def makeTableLimits(limitDicts):
    """ Make table of limits """
    # sorted keys
    table    = "\\begin{tabular}{r l l l l l l l l l}\\toprule\n"

    for iInt,interference in enumerate(["const","dest"]):
        names       = [x for x in limitDicts.keys() if interference in x]
        pdfSs       = ["{0:.2f}".format(limitDicts[n]["pdfSs"]) for n in names]
        fitSs       = ["{0:.2f}".format(limitDicts[n]["fitSs"]) for n in names]
        expLimit    = ["{0:.2f}".format(limitDicts[n]["expLimit"]) for n in names]
        lamLimitLL  = ["{0:.2f}".format(lcExpLimit(limitDicts[n],"LL")) for n in names]
        lamLimitRL  = ["{0:.2f}".format(lcExpLimit(limitDicts[n],"RL")) for n in names]
        lamLimitLR  = ["{0:.2f}".format(lcExpLimit(limitDicts[n],"LR")) for n in names]
        lamLimitRR  = ["{0:.2f}".format(lcExpLimit(limitDicts[n],"RR")) for n in names]
        nBkg        = ["{0:.2f}".format(limitDicts[n]["nBkg"]) for n in names]
        nObs        = ["{0:.2f}".format(limitDicts[n]["nObs"]) for n in names]
        # build table
        # table   += "       & "+" & ".join(names).replace("ee-","")+"\\\\\n"
        table   += "       & "+" & ".join(names)+"\\\\\n"
        table   += "Limit (N Sig)  & "+" & ".join(expLimit)+"\\\\\n"
        table   += "LL Limit ($\\Lambda$ TeV)  & "+" & ".join(lamLimitLL)+"\\\\\n"
        table   += "RL Limit ($\\Lambda$ TeV)  & "+" & ".join(lamLimitRL)+"\\\\\n"
        table   += "LR Limit ($\\Lambda$ TeV)  & "+" & ".join(lamLimitLR)+"\\\\\n"
        table   += "RR Limit ($\\Lambda$ TeV)  & "+" & ".join(lamLimitRR)+"\\\\\n"
        table   += "PDF SS & "+" & ".join(pdfSs)+"\\\\\n"
        table   += "Fit SS & "+" & ".join(fitSs)+"\\\\\n"
        table   += "N Background  & "+" & ".join(nBkg)+"\\\\\n"
        table   += "N Observed  & "+" & ".join(nObs)+"\\\\\n"
        if iInt!=1: table += "\\hline\n"

    table   += "\\bottomrule\\end{tabular}"
    print table

def makeTableLimitsUnblind(limitDicts):
    """ Make table of limits """
    print "#"*50
    print green("Table of nSig limits in terms of lambda" )
    print "#"*50,"\n"
    # sorted keys
    table    = "\\begin{tabular}{r l l l l l l l l l}\\toprule\n"
    table   += r"& & LL $\Lambda$ TeV & LR $\Lambda$ TeV & RL $\Lambda$ TeV & RR $\Lambda$ TeV \\"+"\n"

    for iChan,channel in enumerate(["mm-const","ee-const","mm-dest","ee-dest"]):
        # names       = [x for x in limitDicts.keys() if channel in x]
        names = [channel]
        pdfSs       = ["{0:.2f}".format(limitDicts[n]["pdfSs"]) for n in names]
        fitSs       = ["{0:.2f}".format(limitDicts[n]["fitSs"]) for n in names]
        expLimitLL  = ["{0:.2f}".format(lcExpLimit(limitDicts[n],"LL")) for n in names]
        expLimitRL  = ["{0:.2f}".format(lcExpLimit(limitDicts[n],"RL")) for n in names]
        expLimitLR  = ["{0:.2f}".format(lcExpLimit(limitDicts[n],"LR")) for n in names]
        expLimitRR  = ["{0:.2f}".format(lcExpLimit(limitDicts[n],"RR")) for n in names]
        obsLimitLL  = ["{0:.2f}".format(lcObsLimit(limitDicts[n],"LL")) for n in names]
        obsLimitRL  = ["{0:.2f}".format(lcObsLimit(limitDicts[n],"RL")) for n in names]
        obsLimitLR  = ["{0:.2f}".format(lcObsLimit(limitDicts[n],"LR")) for n in names]
        obsLimitRR  = ["{0:.2f}".format(lcObsLimit(limitDicts[n],"RR")) for n in names]
        # build table
        # table   += "       & "+" & ".join(names).replace("ee-","")+"\\\\\n"
        # table   += "       & "+" & ".join(names)+"\\\\\n"
        table += r"\multirow{2}{*}{"+channel+"} & "
        table += "Expected & "
        table += expLimitLL[0]+" & "+expLimitLR[0]+" & "+expLimitRL[0]+" & "+expLimitRR[0]+"\\\\\n"
        table += "& Observed & "
        table += obsLimitLL[0]+" & "+obsLimitLR[0]+" & "+obsLimitRL[0]+" & "+obsLimitRR[0]+"\\\\\n"
        # table += 
        if iChan!=3: table += "\\hline\n"

    table   += "\\bottomrule\\end{tabular}"
    print table
    print 

def makeTableLimitsUnblind_syst(limitDicts):
    """ Make table of limit uncertainties """
    # sorted keys
    table    = "\\begin{tabular}{r l l l l l l l l l}\\toprule\n"
    table   += r"& & LL $\Lambda$ TeV & LR $\Lambda$ TeV & RL $\Lambda$ TeV & RR $\Lambda$ TeV \\"+"\n"

    for iChan,channel in enumerate(["mm-const","ee-const","mm-dest","ee-dest"]):
        # names       = [x for x in limitDicts.keys() if channel in x]
        names = [channel]
        pdfSs       = ["{0:.2f}".format(limitDicts[n]["pdfSs"]) for n in names]
        fitSs       = ["{0:.2f}".format(limitDicts[n]["fitSs"]) for n in names]
        expLimitLL  = ["{0:.2f}".format(lcExpLimit(limitDicts[n],"LL")) for n in names]
        expLimitRL  = ["{0:.2f}".format(lcExpLimit(limitDicts[n],"RL")) for n in names]
        expLimitLR  = ["{0:.2f}".format(lcExpLimit(limitDicts[n],"LR")) for n in names]
        expLimitRR  = ["{0:.2f}".format(lcExpLimit(limitDicts[n],"RR")) for n in names]
        upLimitLL  = ["{0:.2f}".format(lcErrLimit(limitDicts[n],"LL")["up"]) for n in names]
        upLimitRL  = ["{0:.2f}".format(lcErrLimit(limitDicts[n],"RL")["up"]) for n in names]
        upLimitLR  = ["{0:.2f}".format(lcErrLimit(limitDicts[n],"LR")["up"]) for n in names]
        upLimitRR  = ["{0:.2f}".format(lcErrLimit(limitDicts[n],"RR")["up"]) for n in names]
        dnLimitLL  = ["{0:.2f}".format(lcErrLimit(limitDicts[n],"LL")["dn"]) for n in names]
        dnLimitRL  = ["{0:.2f}".format(lcErrLimit(limitDicts[n],"RL")["dn"]) for n in names]
        dnLimitLR  = ["{0:.2f}".format(lcErrLimit(limitDicts[n],"LR")["dn"]) for n in names]
        dnLimitRR  = ["{0:.2f}".format(lcErrLimit(limitDicts[n],"RR")["dn"]) for n in names]
        # build table
        # table   += "       & "+" & ".join(names).replace("ee-","")+"\\\\\n"
        # table   += "       & "+" & ".join(names)+"\\\\\n"
        table += r"\multirow{3}{*}{"+channel+"} & "
        table += r"Expected & "
        table += expLimitLL[0]+" & "+expLimitLR[0]+" & "+expLimitRL[0]+" & "+expLimitRR[0]+"\\\\\n"
        table += r"& $+1\sigma$ & "
        table += upLimitLL[0]+" & "+upLimitLR[0]+" & "+upLimitRL[0]+" & "+upLimitRR[0]+"\\\\\n"
        table += r"& $-1\sigma$ & "
        table += dnLimitLL[0]+" & "+dnLimitLR[0]+" & "+dnLimitRL[0]+" & "+dnLimitRR[0]+"\\\\\n"
        # table += 
        if iChan!=3: table += "\\hline\n"

    table   += "\\bottomrule\\end{tabular}"
    print table

def makeTableLimitsUnblind_nSig(limitDicts):
    """ Make table of limits """
    print "#"*50
    print green("Table of nSig limits" )
    print "#"*50,"\n"
    # sorted keys
    table    = "\\begin{tabular}{r l l l l l l l l l}\\toprule\n"
    table   += r"Channel & Expected & Observed & $+1\sigma$ & $-1\sigma$ \\"+"\n"

    # for iChan,channel in enumerate(["mm-const"]):
    for iChan,channel in enumerate(["mm-const","ee-const","mm-dest","ee-dest"]):
        # names       = [x for x in limitDicts.keys() if channel in x]
        names = [channel]
        # print [limitDicts[n]["pdfSs"] for n in names]; quit()
        pdfSs       = ["{0:.2f}".format(limitDicts[n]["pdfSs"]) for n in names]
        fitSs       = ["{0:.2f}".format(limitDicts[n]["fitSs"]) for n in names]
        expLimit    = ["{0:.2f}".format(limitDicts[n]["nSig_expLimit"]) for n in names]
        obsLimit    = ["{0:.2f}".format(limitDicts[n]["nSig_upperLimit"]) for n in names]
        dnLimit     = ["{0:.2f}".format(limitDicts[n]["nSig_lOneSig"]) for n in names]
        upLimit     = ["{0:.2f}".format(limitDicts[n]["nSig_uOneSig"]) for n in names]
        # build table
        # table   += "       & "+" & ".join(names).replace("ee-","")+"\\\\\n"
        # table   += "       & "+" & ".join(names)+"\\\\\n"
        table += channel+" & "
        table += expLimit[0]+" & "+obsLimit[0]+" & "+upLimit[0]+" & "+dnLimit[0]+"\\\\\n"
        # table += 
        # if iChan!=3: table += "\\hline\n"

    table   += "\\bottomrule\\end{tabular}"
    print table
    print 

def makeTableLimitsUnblind_lambda(limitDicts,name="LambdaAc"):
    """ Make table of limits """
    print "%","#"*50
    print green(r"{\color{gray} Limits in terms of lambda (directly), calculator "+name+"}")
    print "%","#"*50,"\n"
    # sorted keys
    cols = sorted(limitDicts.keys())
    table    = r"{\tablesize\begin{tabular}{r l l l l l l l l l}\toprule"+"\n"
    table   += r"& & LL $\Lambda$ TeV & LR $\Lambda$ TeV & RL $\Lambda$ TeV & RR $\Lambda$ TeV \\"+"\n"

    # for iChan,channel in enumerate(["mm-const","ee-const","mm-dest","ee-dest"]):
    for channel in cols:
        table+=r"\multirow{2}{*}{"+channel+"} & "
        table += r"Expected & "
        for chirality in ["LL","LR","RL","RR"]:
            try: expected = limitDicts[channel]["{0}_{1}_expLimit".format(chirality,name)]
            except: expected = -1
            table += "{0:.2f} & ".format(expected)
        table=table[:-2]+r"\\"+"\n& "
        table += r"Observed & "
        for chirality in ["LL","LR","RL","RR"]:
            
            try: observed = limitDicts[channel]["{0}_{1}_upperLimit".format(chirality,name)]
            except: observed = -1
            table += "{0:.2f} & ".format(observed)
        table=table[:-2]+r"\\"+"\n"+r"\midrule"

    table   += "\\bottomrule\\end{tabular}}"
    print table
    print 

# iPaths = "limits/single*const*"
iPaths = "limits/single*ee*"; tag="-ee-"
# iPaths = "limits/single*mm*"; tag="-mm-"
iPaths = "limits/single*"; tag=""
iPaths = "limits-50-5000/single*"; tag=""
iPaths = "limitsAll10k/*"
# iPaths = "limitsAll10k-2/*"
iPaths = "limits-30-10000-5nd/*" # just LL
os.popen("rm plots/*")

# load limitDicts
iPaths = glob.glob(iPaths)
limitDicts = defaultdict(dict)
for iPath in iPaths:
    print iPath
    limitDict = pickle.load(open(iPath))
    limitDicts[limitDict["name"]].update(limitDict)
print 

# # call limit plot
# makeTableLimits(limitDicts)
# make tables with lambda conversion
# makeTableLimitsUnblind_syst(limitDicts)

# # nice tables:
# makeTableLimitsUnblind_nSig(limitDicts)
# makeTableLimitsUnblind(limitDicts)
# makeTableLimitsUnblind_lambda(limitDicts,name="LambdaAc")
makeTableLimitsUnblind_lambda(limitDicts,name="LambdaToy2")
# quit()

# other oplots

# path = "plots/limits-nsig.png"
# plotter.limitPlot_nSig_data(limitDicts,path=path)
# # quit()

# path = "plots/limits-visibleXsBr.png"
# plotter.limitPlot_nSig_data(limitDicts,scale="xsBrVisible",path=path)
# # quit()

# path = "plots/limits-xsBr.png"
# plotter.limitPlot_nSig_data(limitDicts,scale="xsBr",path=path)
# # quit()

# path = "plots/limits-lambda-{0}.png".format(tag)
# plotter.limitPlot_lambda_data(limitDicts,path=path)

# path = "plots/limits-lambda-{0}.png".format(tag)
# plotter.limitPlot_nSig_data(limitDicts,nsig=False,path=path)
# # quit()

# scale="kg"
# path = "plots/limits-{0}-{1}.png".format(scale,tag)
# plotter.limitPlot_nSig_data(limitDicts,scale=scale,nsig=False,path=path)

# scale="neuron"
# path = "plots/limits-{0}-{1}.png".format(scale,tag)
# plotter.limitPlot_nSig_data(limitDicts,scale=scale,nsig=False,path=path)

# scale="r"
# path = "plots/limits-{0}-{1}.png".format(scale,tag)
# plotter.limitPlot_nSig_data(limitDicts,scale=scale,nsig=False,path=path)

# scale="transistor"
# path = "plots/limits-{0}-{1}.png".format(scale,tag)
# plotter.limitPlot_nSig_data(limitDicts,scale=scale,nsig=False,path=path)
