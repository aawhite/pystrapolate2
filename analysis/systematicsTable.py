from __future__ import division

################################################################################
# Cross check B-only vs S+B background models
# Comparison includes SS uncertainties
# Output is a control plot
################################################################################

import sys
sys.path = ["../source"] + sys.path
from setup import *
import plotter


# default inputs
interference = "const"
chirality = "LL"
channel = "ee"
release = "21"
lumi = 139
signalMass = 20
inject = 0


fitOn = 1

print r"channel & interference & nObs & nBkg & pdfSs & fitSs & funcSs \\"
# for channel in ["ee"]:
# for channel in ["mm"]:
for channel in ["ee","mm"]:
    for interference in ["const","dest"]:
    # for interference in ["dest"]:
    # for interference in ["const"]:
        nObs,b_nBkg,b_pdfSs,b_fitSs,funcSs = getSyst(channel,interference,func=1)
        nObs=int(nObs)
        line = [nObs,b_nBkg,b_pdfSs,b_fitSs,funcSs]
        line = ["{0:.2f}".format(s) for s in line]
        line = [channel,interference]+line
        print " & ".join(line)+r" \\"

print green("In percent"); print

# print r"Channel & Interference & fitSs & pdfSs & funcSs \\"
print r"Channel & Interference & SU & ISSU & CRBU \\"
# for channel in ["ee"]:
# for channel in ["mm"]:
for channel in ["ee","mm"]:
    for interference in ["const","dest"]:
    # for interference in ["dest"]:
    # for interference in ["const"]:
        nObs,b_nBkg,b_pdfSs,b_fitSs,funcSs = getSyst(channel,interference,func=1)
        nObs=int(nObs)
        line = [b_fitSs,b_pdfSs,funcSs]
        line = [r"{0:.2f}\%".format(s/b_nBkg*100) for s in line]
        line = [channel,interference]+line
        line = " & ".join(line)+r" \\"
        line = line.replace("const","Constructive")
        line = line.replace("dest","Destructive")
        line = line.replace("mm",r"$\upmu\upmu$")
        print line

# print
# print r"Background uncertainty & \multicolumn{2}{c|}{Constructive} & \multicolumn{2}{c}{Destructive} \\"
# print r" & ee & $\upmu\upmu$ & ee & $\upmu\upmu$ \\"
# for uncert in ["pdf","fit","func"]:
#     line = uncert+" & "
#     for interference in ["const","dest"]:
#         for channel in ["ee","mm"]:
#         # for interference in ["dest"]:
#         # for interference in ["const"]:
#             nObs,b_nBkg,b_pdfSs,b_fitSs,funcSs = getSyst(channel,interference,func=1)
#             tmp = {}
#             tmp["pdf"] = b_pdfSs
#             tmp["fit"] = b_fitSs
#             tmp["func"] = funcSs
#             line += r"{0:.2f}\% & ".format(tmp[uncert]/b_nBkg)
#     line=line[:-2]
#     line+=r" \\"
#     line = line.replace("pdf","Induced spurious signal")
#     line = line.replace("fit","Statistical uncertainty")
#     line = line.replace("func","Control region bias")
#     print line



# print green("DONE")

