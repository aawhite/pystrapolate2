#! /usr/bin/env python2

from __future__ import division
import sys,os
# sys.path = [os.path.dirname(__file__)+"/../source"] + sys.path
sys.path = [os.path.dirname(os.path.abspath(__file__))+"/../source"] + sys.path
from setup import *
import plotter

################################################################################
#   Basic fit/limit setting using pre-recorded limits from settings
################################################################################ 

def doFit(channel,interference,chiralities,
          nToys=None,
         ):
    chiralities = chiralities.split(",")

    chi="LL"
    fitMin = dictionary.ranges[channel][interference][chi]["fitMin"]
    fitMax = dictionary.ranges[channel][interference][chi]["fitMax"]
    extrapMin = dictionary.ranges[channel][interference][chi]["extrapMin"]
    extrapMax = 6000
    name = "{0}-{1}".format(channel,interference)
    tag ="-"+name


    ################################################################################
    # Data fit
    ################################################################################
    nominal = fitMacro(
                        interference=interference,
                        chirality=chi,
                        channel=channel,
                        lumi=lumi,fitMin=fitMin,
                        fitMax=fitMax,extrapMin=extrapMin,
                        extrapMax=extrapMax,inject=inject,
                        signalMass="12",
                        release=release,
                        plotOn=0,fitOn=1,
                        useSbModel=0,
                        backgroundName="data",
                        fitToData=1,
                        # backgroundName="backgroundTemplate",
                     )

    # systematics
    nObs,nBkg,pdfSs,fitSs,funcSs,lumiSs = getSyst(channel,interference,lumi=1)
    # pdfSs/=5


    #################################################################################
    ## limit setting (on signal region)
    #################################################################################

    yields = nominal.yields()
    nBkg = yields["nBkg"] # nominal fit background expected
    nObs = yields["nObs"] # nominal fit observed




    limitResult = {}

    # nBkg = 10
    # nObs = int(nBkg)
    # npWidth = 0.001
    # pdfSs = npWidth
    # fitSs = npWidth
    # funcSs = npWidth
    # settings = {}
    # settings["nToys"] = 1000
    # settings["nSteps"] = 30
    # settings["scanMin"] = 0.01
    # settings["scanMax"] = 50
    # acResult = limits.acLimit(nBkg, nObs, npWidth1=pdfSs,npWidth2=fitSs,npWidth3=funcSs,settings=settings)
    # # print yellow("Observed\t{0:.5f}".format(acResult["observed"]))
    # # print yellow("Expected\t{0:.5f}".format(acResult["expected"]))
    # # quit()
    # fcResult = limits.fcLimit(nBkg, nObs, npWidth1=pdfSs,npWidth2=fitSs,npWidth3=funcSs,settings=settings)
    # # print report
    # print yellow("="*50)
    # print yellow("nToys    = {0}".format(settings["nToys"]))
    # print yellow("nStep    = {0}".format(settings["nSteps"]))
    # print yellow("nObs     = {0}".format(nObs))
    # print yellow("nBkg     = {0}".format(nBkg))
    # print yellow("        \tAC Lim   \tFC Lim".format(acResult["observed"],fcResult["observed"]))
    # print yellow("Observed\t{0:.5f}\t{1:.5f}".format(acResult["observed"],fcResult["observed"]))
    # print yellow("Expected\t{0:.5f}\t{1:.5f}".format(acResult["expected"],fcResult["expected"]))
    # print yellow("="*50)
    # quit()

    # nBkg = 10
    # nObs = int(nBkg)
    # npWidth = 0.001
    # pdfSs = npWidth
    # fitSs = npWidth
    # funcSs = npWidth

    # nToys = 10000
    nSteps = 30

    # nToys,nSteps = 200,10
    # nBkg = 1000
    # nObs = int(nBkg)

    limitResultNSigAc   = limits.poissonLimit1_ac(nBkg, nObs, npWidth1=pdfSs,npWidth2=fitSs,npWidth3=funcSs)
    limitResult.update(limitResultNSigAc)
    # print green(nObs); quit()

    limitResultNSigFc   = limits.poissonLimit1_fc(nBkg, nObs, npWidth1=pdfSs,npWidth2=fitSs,npWidth3=funcSs,nToys=nToys,nSteps=nSteps)
    limitResult.update(limitResultNSigFc)

    # print red(limitResultNSigAc["nSigAc_upperLimit"])
    # print green(limitResultNSigFc["nSigFc_upperLimit"])
    # print ; quit()

    # limitResultToys = {"LL_LambdaToy_upperLimitNSig":0,"LL_LambdaToy_expLimitNSig":0}
    # limitResultToys   = limits.poissonLimit3lToys(nBkg, nObs, npWidth1=pdfSs,npWidth2=fitSs,npWidth3=funcSs,
    #                                               interference=interference,channel=channel,chirality="LL",
    #                                               nToys=nToys,nSteps=nSteps
    #                                              )
    # limitResult.update(limitResultToys)

    # print yellow("="*50)
    # print yellow("nObs     = {0}".format(nObs))
    # print yellow("nBkg     = {0}".format(nBkg))
    # print "AC obs",limitResultNSig["nSig_upperLimit"]
    # print yellow("FC obs",limitResultToys["LL_LambdaToy3_upperLimitNSig"])
    # print "-"
    # print "AC exp",limitResultNSig["nSig_expLimit"]
    # print yellow("FC exp",limitResultToys["LL_LambdaToy3_expLimitNSig"])
    # print yellow("="*50)
    # quit()

    # 1k toys:
    # nSig_upperLimit 16.3058085111
    # nSig_expLimit   11.9521554452
    # LambdaToy_upperLimitNSig 16.220995485


    # quit()

    for chirality in chiralities:
        # limitResultLambdaLumi = limits.poissonLimitLumi(nBkg, nObs, npWidth1=pdfSs,npWidth2=fitSs,npWidth3=funcSs,
        #                                       chirality=chirality,interference=interference,channel=channel,
        #                                      )

        limitResultAc = limits.poissonLimit3l(nBkg, nObs, npWidth1=pdfSs,npWidth2=fitSs,npWidth3=funcSs,
                                              chirality=chirality,interference=interference,channel=channel,
                                             )


        limitResultToys1 = limits.poissonLimit2lToys(nBkg, nObs, npWidth1=pdfSs,npWidth2=fitSs,
                                                  chirality=chirality,interference=interference,channel=channel,
                                                  nToys=nToys,nSteps=nSteps
                                             )

        # quit()

        # limitResultToys3 = limits.poissonLimit3lToys(nBkg, nObs, npWidth1=pdfSs,npWidth2=fitSs,npWidth3=funcSs,
        #                                             interference=interference,channel=channel,chirality=chirality,
        #                                             nToys=nToys,nSteps=nSteps
        #                                            )


        print yellow("="*50)
        print yellow("Limit results",channel,interference,chirality)
        print "Expected background: {0}, observed: {1}".format(nBkg,nObs)
        print "limit                            :  Value"
        # print "nSig +1sigma                     : ",limitResultNSigFc["nSig_uOneSig"]
        # print "nSig limit                       : ",limitResultNSigFc["nSig_upperLimit"]
        # print "nSig limit (exp)                 : ",limitResultNSigFc["nSig_expLimit"]
        print "LambdaToy1 +1sigma               : ",limitResultToys1["{0}_LambdaToy2_uOneSig".format(chirality)]
        print "LambdaToy1 limit (conv nSig)     : ",limitResultToys1["{0}_LambdaToy2_upperLimitNSig".format(chirality)]
        print "LambdaToy1 limit on Lambda       : ",limitResultToys1["{0}_LambdaToy2_upperLimit".format(chirality)]
        # print "LambdaToy3 +1sigma               : ",limitResultToys3["{0}_LambdaToy3_uOneSig".format(chirality)]
        # print "LambdaToy3 limit (conv nSig)     : ",limitResultToys3["{0}_LambdaToy3_upperLimitNSig".format(chirality)]
        # print "LambdaToy3 limit on Lambda       : ",limitResultToys3["{0}_LambdaToy3_upperLimit".format(chirality)]
        print "LambdaAc +1sigma                 : ",limitResultAc["{0}_LambdaAc_uOneSig".format(chirality)]
        print "LambdaAc limit (conv nSig)       : ",limitResultAc["{0}_LambdaAc_upperLimitNSig".format(chirality)]
        print "LambdaAc limit on Lambda         : ",limitResultAc["{0}_LambdaAc_upperLimit".format(chirality)]

        # print "lumiUncertLamb +1sigma           : ",limitResultLambdaLumi["{0}_lambdaLumi_uOneSig".format(chirality)]
        # print "lumiUncertLamb limit (conv nSig) : ",limitResultLambdaLumi["{0}_lambdaLumi_upperLimitNSig".format(chirality)]
        # print "lumiUncertLamb limit on Lambda   : ",limitResultLambdaLumi["{0}_lambdaLumi_upperLimit".format(chirality)]

        print yellow("="*50)

        # quit()

        # limitResult.update(limitResultToys3)
        limitResult.update(limitResultAc)
        limitResult.update(limitResultToys1)
    print yellow("nBkg=",nBkg)

    # additional info
    limitResult["name"] = name 
    limitResult["pdfSs"] = pdfSs
    limitResult["fitSs"] = fitSs
    limitResult["channel"] = channel
    limitResult["interference"] = interference
    limitResult["release"] = release
    limitResult["tag"] = tag
    limitResult["lumi"] = lumi
    limitResult["extrapMin"] = extrapMin
    limitResult["extrapMax"] = extrapMax
    limitResult["fitMin"] = fitMin
    limitResult["fitMax"] = fitMax
    limitResult["nBkg"] = nBkg
    limitResult["nObs"] = nObs
    limitResult["nSteps"] = nSteps
    limitResult["nToys"] = nToys
    return limitResult




################################################################################
# DEFAULT INPUTS
# When run without inputs, these are used
################################################################################
release = "21"
inject = 0
lumi = 139

# load inputs, otherwise use defaults
try:
    channel = sys.argv[1]
    interference = sys.argv[2]
    chiralities = sys.argv[3]
    outputPath = sys.argv[4]
    nToys = int(sys.argv[5])
except:

    # channel = "ee"
    # interference = "const"
    # chiralities = "LL"
    # outputPath = "limits"

    channel = "mm"
    interference = "const"
    chiralities = "LL"
    outputPath = "limits"
    nToys = 1000

print green("Doing limits for",channel,interference)
limitResult = doFit(channel,interference,chiralities,nToys=nToys)
picklePath = "{0}/singleLimit-{1}-{2}-{3}-nToy{4}.pickle".format(outputPath,channel,interference,chiralities.replace(",","-"),nToys)
pickle.dump(limitResult,open(picklePath,"w"))
print green("Saving result to",picklePath)

        # quit()

print green("DONE")


