#mod.x[i]>fitMin ! /usr/bin/env python2

from __future__ import division
import sys,os
# sys.path = [os.path.dirname(__file__)+"/../source"] + sys.path
sys.path = [os.path.dirname(os.path.abspath(__file__))+"/../source"] + sys.path
from setup import *
import plotter

################################################################################
#   Most basic fit implementation
#   Arguements are CLI X=Y
################################################################################ 

# os.popen("rm plots/*")

################################################################################
# DEFAULT INPUTS
# When run without inputs, these are used
################################################################################
interference = "const"
# interference = "dest"
chirality = "LL"
channel = "ee"
release = "21"
inject = 0
lumi = 139
signalMass = 24
nToys = 3
doPdf = True
fitMin = 300
fitMax = 2000
extrapMin = fitMax
extrapMax = 6000
tag=""
plotOn=0
useSbModel = False
limitsOutputPath = "limits"

################################################################################
# CLI INPUTS
# These should take the form of ordered pairs:
#    fitMin=500
################################################################################
for param in sys.argv:
    if "=" not in param: continue
    name = param.split("=")[0]
    val = param.split("=")[1]
    print yellow(param, "{0}={1}".format(name,val))
    try: # try for number
        exec("{0}={1}".format(name,val))
    except: # try for string
        exec("{0}='{1}'".format(name,val))

if fitMin>fitMax: 
    print red("fitMin>fitMax, quitting")
    quit()

################################################################################
# Data fit
################################################################################

# nominal1 = fitMacro(
#                     interference=interference,
#                     chirality=chirality,
#                     channel=channel,
#                     lumi=lumi,fitMin=fitMin,
#                     fitMax=fitMax,extrapMin=extrapMin,
#                     extrapMax=extrapMax,inject=inject,
#                     signalMass=signalMass,
#                     release=release,
#                     plotOn=plotOn,fitOn=1,
#                     useSbModel=0,
#                     tag=tag+"-data",
#                     # backgroundName="data",
#                     backgroundName="backgroundTemplate",
#                  )
# dataYields = nominal1.yields()
# # quit()

# nominal2 = fitMacro(
#                     interference=interference,
#                     chirality=chirality,
#                     channel=channel,
#                     lumi=lumi,fitMin=fitMin,
#                     fitMax=fitMax,extrapMin=extrapMin,
#                     extrapMax=extrapMax,inject=inject,
#                     signalMass=signalMass,
#                     release=release,
#                     plotOn=plotOn,fitOn=1,
#                     useSbModel=0,
#                     tag=tag+"-data",
#                     backgroundName="data",
#                     # backgroundName="backgroundTemplate",
#                  )
# dataYields2 = nominal2.yields()

# nominal3 = fitMacro(
#                     interference=interference,
#                     chirality=chirality,
#                     channel=channel,
#                     lumi=lumi,fitMin=fitMin,
#                     fitMax=fitMax,extrapMin=extrapMin,
#                     extrapMax=extrapMax,inject=inject,
#                     signalMass=signalMass,
#                     release=release,
#                     plotOn=plotOn,fitOn=1,
#                     useSbModel=0,
#                     tag=tag+"-data",
#                     backgroundName="data",
#                     # backgroundName="backgroundTemplate",
#                  )
# dataYields3 = nominal3.yields()

# print red(dataYields["nSpur"])
# print red(dataYields2["nSpur"])
# print red(dataYields3["nSpur"])
# print green("DONE"); quit()

################################################################################
# Nominal fit
################################################################################
name = "{0}-{1}-{2}{3}".format(channel,interference,chirality,tag)
nominal = fitMacro(
                    interference=interference,
                    chirality=chirality,
                    channel=channel,
                    lumi=lumi,fitMin=fitMin,
                    fitMax=fitMax,extrapMin=extrapMin,
                    extrapMax=extrapMax,inject=inject,
                    signalMass=signalMass,
                    release=release,
                    # plotOn=0,fitOn=1,
                    plotOn=plotOn,fitOn=1,
                    useSbModel=useSbModel,
                    tag=tag+"-nominal",
                    backgroundName="backgroundTemplate",
                    # backgroundName="data",
                 )
nominalYields = nominal.yields()
nominalSs = nominalYields["nSpur"]
print red(nominalYields["nSpur"])
# print green("DONE"); quit()

# nominal chi2
x=nominal.get("x")
mod=rw.rooWrap(nominal.get("model"),x)
bkg=rw.rooWrap(nominal.get("backgroundTemplate"),x)
mod.normalizeTo(bkg,minRange=fitMin,maxRange=fitMax)
chi2 = []
for i in range(len(mod.x)):
    if mod.x[i]!=bkg.x[i]:
        print "MISSMATCH"
    if mod.x[i]>fitMin and mod.x[i]<fitMax:
        chi2.append(((mod.y[i]-bkg.y[i])**2)/mod.y[i])
chi2Ndof=sum(chi2)/len(chi2)
chi2 = sum(chi2)
print chi2Ndof, chi2
quit()

################################################################################
# Fit spurious signal (toys)
################################################################################
for i in range(nToys):
    nominal.fitToy(saveToy=plotOn)
if nToys:
    fitSsDist = nominal.toyFitDiff()
    fitSs = np.array(fitSsDist).std()
else:
    fitSsDist = 0
    fitSs = 0
# plot
if plotOn:
    path = "plots/fitSs-{0}.png".format(name)
    plotter.uncertHisto(fitSsDist,nominalSs,fitSs,path=path,
                        xlabel="Fit Spurious Signal")
    # detailed plot
    path = path.replace("fitSs","fitSsBand")
    plotter.plot_details(nominal,path=path)

print green("DONE"); quit()

################################################################################
# fit PDF uncertainties
################################################################################
if doPdf:
    pdfVarYields={}
    for i, pdfName in enumerate(dictionary.pdfNames[release]):
        print green("Fitting pdf variaiton",pdfName)
        pdfVariation = fitMacro(interference=interference,
                      chirality=chirality,
                      channel=channel,
                      lumi=lumi,fitMin=fitMin,
                      fitMax=fitMax,extrapMin=extrapMin,
                      extrapMax=extrapMax,inject=inject,
                      signalMass=signalMass,
                      release=release,
                      tag=tag+pdfName,
                      plotOn=0,fitOn=1,
                      useSbModel=useSbModel,
                      backgroundName=pdfName,
                     )
        pdfVarYields[pdfName]=pdfVariation.yields()
        # if i==2: break
    pdfSsDist = [abs(pdfVarYields[y]["nSpur"]) for y in pdfVarYields.keys()]
    pdfSs = max(pdfSsDist)
    pdfSs = max(pdfSs,abs(nominalSs))
    for i,pdf in enumerate(pdfVarYields.keys()):
        print i,yellow(pdfVarYields[pdf]["nSpur"], pdf)
    print yellow("Nominal:",nominalSs)
    print "len",len(pdfSsDist)
    print red(pdfSsDist)
    print red(max(pdfSsDist))
    print red(pdfSs)
else:
    pdfSsDist = 0
    pdfSs = 0
    pdfSs = 0

# print green("DONE"); quit()

# plot
if plotOn:
    path = "plots/pdfSs-{0}.png".format(name)
    plotter.uncertHisto(pdfSsDist,nominalSs,pdfSs,path=path,
                        xlabel="PDF Spurious Signal")




################################################################################
# limit setting (on signal region)
################################################################################
# pdfSs=10
# fitSs=10
nBkg = nominalYields["nBkg"] # nominal fit background expected
nObs = nominalYields["nObs"] # nominal fit observed
nBkgMc = nominalYields["nBkgMc"]
# fitSs = 1 # crucial, remove this
limitResult = limits.poissonLimit(nBkg, nObs, npWidth1=pdfSs,npWidth2=fitSs)
limitResult["nomSs"] = nBkg-nBkgMc
limitResult["fitSs"] = fitSs
limitResult["pdfSs"] = pdfSs
limitResult["fitSsDist"] = fitSsDist
limitResult["pdfSsDist"] = pdfSsDist
limitResult["upperLimitRel"] = limitResult["upperLimit"]/nBkgMc if nBkgMc!=0 else -1
limitResult["nomSsRel"] = (nBkg-nBkgMc)/nBkgMc if nBkgMc!=0 else -1
limitResult["fitSsRel"] = fitSs/nBkgMc if nBkgMc!=0 else -1
limitResult["pdfSsRel"] = pdfSs/nBkgMc if nBkgMc!=0 else -1
limitResult["meanFitSs"] = np.array(fitSsDist).mean()/nBkgMc if nBkgMc!=0 else -1
limitResult["meanPdfSs"] = np.array(pdfSsDist).mean()/nBkgMc if nBkgMc!=0 else -1
limitResult["nObs"] = nObs
limitResult["nBkg"] = nBkg
limitResult["nBkgMc"] = nBkgMc
limitResult["lambda"] = nominalYields["lambda"]
limitResult["name"] = name
limitResult["chirality"] = chirality
limitResult["channel"] = channel
limitResult["interference"] = interference
limitResult["release"] = release
limitResult["tag"] = tag
limitResult["lumi"] = lumi
limitResult["extrapMin"] = extrapMin
limitResult["extrapMax"] = extrapMax
limitResult["fitMin"] = fitMin
limitResult["fitMax"] = fitMax
limitResult["inject"] = inject
limitResult["signalMass"] = signalMass
# chi2 of nominal in CR

# add data limit

dataNBkg = dataYields["nBkg"] # nominal fit background expected
dataNObs = dataYields["nObs"] # nominal fit observed
dataLimitResult = limits.poissonLimit(dataNBkg, dataNObs, npWidth1=pdfSs,npWidth2=fitSs)
# rename
dataLimitResult = {k+"-data":dataLimitResult[k] for k in dataLimitResult.keys()}
dataLimitResult["nBkg-data"] = dataNBkg
dataLimitResult["nObs-data"] = dataNObs
limitResult.update(dataLimitResult)

fullName = name+"-{0}-{1}-{2}".format(fitMin,fitMax,extrapMin)
os.popen("mkdir {0}".format(limitsOutputPath))
picklePath = "{0}/singleLimit-{1}.pickle".format(limitsOutputPath,fullName)
pickle.dump(limitResult,open(picklePath,"w"))
print green("Saving result to",picklePath)

# import lambdaConversion as lc
# lambdaConvert = lc.lambdaLimit()
# expected = limitResult['expLimit']
# expectedLambda = lambdaConvert.getLambdaLimit(expected,extrapMin,extrapMax,channel,model=chirality,interference=interference,lumi=lumi)
# print yellow("Ranges:",fitMin,fitMax,extrapMin)
# print red("fitSs",fitSs)
# print red("pdfSs",pdfSs)
# print red("nObs",nObs)
# print red("nBkg",nBkg)
# print red("nObs",nObs)
# print yellow("Expected limit N Sig:  {0}".format(expected))
# print yellow("Expected limit Lambda: {0}".format(expectedLambda))

print green("DONE")

