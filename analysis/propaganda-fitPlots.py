from __future__ import division

################################################################################
# Make nice plot of fit with log-binned data
################################################################################

import sys
sys.path = ["../source"] + sys.path
from setup import *
import plotter

os.popen("rm plots/*")

def doFit(fitToData=False,useSbModel=None):
    """
    ############################## 
    #    1) Do nominal fit
    #    2) Do toy fits
    #    3) Do PDF fits
    #    4) Do fit to "data"
    ############################## 
    """ 

    fitMax    = dictionary.ranges[channel][interference][chirality]["fitMax"]
    extrapMin = dictionary.ranges[channel][interference][chirality]["extrapMin"]
    fitMin    = dictionary.ranges[channel][interference][chirality]["fitMin"]
    print fitMax, extrapMin

    nBkgs = {}
    fitMin=int(fitMin)
    ############################## 
    # do fit
    ############################## 
    nominalMc = fitMacro(
                        interference=interference,
                        chirality=chirality,
                        channel=channel,
                        lumi=lumi,fitMin=fitMin,
                        fitMax=fitMax,extrapMin=extrapMin,
                        extrapMax=extrapMax,inject=inject,
                        signalMass=signalMass,
                        release=release,
                        plotOn=0,fitOn=1,
                        useSbModel=useSbModel,
                        backgroundName="backgroundTemplate",
                     )
    return nominalMc

def poisson_interval(v):
    if v==0: return 0,0
    # from Ntuple reader code:
    y1 = v + 1.0
    d1 = 1.0 - 1.0/(9.0*y1) + 1.0/(3*sqrt(abs(y1)))
    hi = y1*d1*d1*d1 - v
    y2 = v
    d2 = 1.0 - 1.0/(9.0*y2) - 1.0/(3.0*sqrt(abs(y2)))
    lo = v - y2*d2*d2*d2
    lo = np.array(lo)
    hi = np.array(hi)
    return [lo,hi]

def loadLog(channel,name):
    """ Function to load log-binned data histograms for plotting """
    pathInfo = dictionary.data[release][channel][name]
    f = TFile.Open(pathInfo.split(":")[0])
    f.Print()
    h = f.Get(pathInfo.split(":")[1])
    h.Print()
    # get info from histogram
    nBins = h.GetNbinsX()
    vals  = [h.GetBinContent(i) for i in range(1,nBins+1)]
    bins  = [h.GetBinLowEdge(i) for i in range(1,nBins+1)]
    vals  = np.array(vals)
    bins  = np.array(bins)
    loErr = np.array([poisson_interval(i)[0] for i in vals])
    hiErr = np.array([poisson_interval(i)[1] for i in vals])
    print "-"*50
    return {"x":bins,"y":vals,"loErr":loErr,"hiErr":hiErr}


# default inputs
release = "21"
lumi = 139
signalMass = 20
inject = 0
extrapMax = 6000

os.popen("rm plots/*")
fitOn = 1
chirality="LL"


# for channel in ["ee"]:
# for channel in ["mm"]:
for channel in ["ee","mm"]:
    for interference in ["const","dest"]:
    # for interference in ["const"]:
    # for interference in ["dest"]:
        # data = loadLog(channel,"dataLog")
        data = loadLog(channel,"dataLog")
        sigs={}
        if interference == "dest": masses = [20,24,30]
        if interference == "const": masses = [28,36,58]
        # masses=[30]
        for mass in masses:
            sigs[mass] = loadLog(channel,"sigLog-{0}-{1}".format(interference,mass))
            sigs[mass]["y"]*=139/80
        # get fit
        # extrap = doFit(fitToData=True,useSbModel=False)
        extrap = doFit(fitToData=False,useSbModel=False)

        path = "plots/fit-{0}-{1}.png".format(interference,channel)
        plotter.propPlotFitNoData(data,extrap,sigs,interference=interference,channel=channel,path=path)
        # plotter.propPlotFit(data,extrap,sigs,interference=interference,channel=channel,path=path)
        # plotter.propPlotFitClean(data,extrap,sigs,interference=interference,channel=channel,path=path)

        # quit()

print green("DONE")

