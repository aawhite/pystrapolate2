from __future__ import division

################################################################################
# Test fit to blurry pt spectra
################################################################################

import sys
sys.path = ["../source"] + sys.path
from setup import *
import plotter

def doFit(fitOn=1,useSbModel=None,lumi=0):
    """
    ############################## 
    #    1) Do nominal fit
    #    2) Do toy fits
    #    3) Do PDF fits
    #    4) Do fit to "data"
    ############################## 
    """ 
    fitSs=0
    pdfSs=0
    chi = "LL"
    fitMin = dictionary.ranges[channel][interference][chi]["fitMin"]
    fitMax = dictionary.ranges[channel][interference][chi]["fitMax"]
    extrapMin = dictionary.ranges[channel][interference][chi]["extrapMin"]
    print fitMin, fitMax, extrapMin

    ############################## 
    # do fit
    ############################## 
    # lumi*=15.4*1000
    nominalMc = fitMacro(
                        interference=interference,
                        chirality=chirality,
                        channel=channel,
                        lumi=lumi,fitMin=fitMin,
                        fitMax=fitMax,extrapMin=extrapMin,
                        extrapMax=extrapMax,inject=inject,
                        signalMass=signalMass,
                        release=release,
                        plotOn=0,fitOn=fitOn,
                        useSbModel=useSbModel,
                        backgroundName="backgroundTemplate",
                     )
    yields = nominalMc.yields()
    nBkg = yields["nBkg"]
    nominalSs = yields["nSpur"]
    print green("nBkg",nBkg)

    ############################### 
    ## fit PDF uncertainties
    ############################### 
    #pdfVarYields={}
    #loopNames = dictionary.pdfNames[release]
    #loopNames.append("backgroundTemplate")
    #for i, pdfName in enumerate(loopNames):
    #    if i==nPdfs: break
    #    print green("Fitting pdf variaiton",pdfName)
    #    pdfVariation = fitMacro(interference=interference,
    #                  chirality=chirality,
    #                  channel=channel,
    #                  lumi=lumi,fitMin=fitMin,
    #                  fitMax=fitMax,extrapMin=extrapMin,
    #                  extrapMax=extrapMax,inject=inject,
    #                  signalMass=signalMass,
    #                  release=release,
    #                  useSbModel=useSbModel,
    #                  plotOn=0,fitOn=fitOn,
    #                  backgroundName=pdfName,
    #                 )
    #    pdfVarYields[pdfName]=pdfVariation.yields()
    #pdfSsDist = [abs(pdfVarYields[y]["nSpur"]) for y in pdfVarYields.keys()]
    #if len(pdfSsDist):
    #    pdfSs = max(pdfSsDist)
    #    pdfSs = max(pdfSs,nominalSs)

    ############################## 
    # fit smeared uncertainties
    ############################## 
    smearVarYields={}
    # loopNames = dictionary.smearedNames[release]
    loopNames = dictionary.smearedNames["pSmear"]
    # lumi*=15.4
    for i, smearName in enumerate(loopNames):
        print green("Fitting smeared variaiton",smearName)
        print red("Scaling MC by 15")
        smearVariation = fitMacro(interference=interference,
                      chirality=chirality,
                      channel=channel,
                      lumi=1*lumi,fitMin=fitMin,
                      # lumi=51.90*lumi,fitMin=fitMin,
                      fitMax=fitMax,extrapMin=extrapMin,
                      extrapMax=extrapMax,inject=inject,
                      signalMass=signalMass,
                      release=release,
                      useSbModel=useSbModel,
                      tag = "-"+smearName,
                      plotOn=0,fitOn=fitOn,
                      backgroundName=smearName,
                     )
        smearVarYields[smearName]=smearVariation.yields()
    smearSsDist = [abs(smearVarYields[y]["nSpur"]) for y in smearVarYields.keys()]
    if len(smearSsDist):
        smearSs = max(smearSsDist)
        # smearSs = max(smearSs,nominalSs)

    print yellow("="*50)
    print "Looped over", loopNames
    print green("Nominal SS",nominalSs)
    print green("Nominal nBkg",yields["nBkgMc"])
    print green("Nominal nTotal",yields["nBkgInFitRange"])
    print yellow("Smeared SS Max", max(smearSsDist))
    print yellow("Name\t\tSS\tSS/Nom\tSS/Bkg\tN-SR\tN-Total")
    try:
        trueNom = smearVarYields["m_uu_psmear00"]["nSpur"]
    except:
        trueNom = smearVarYields["m_uu_smear00"]["nSpur"]
    for name in sorted(smearVarYields.keys()):
        nBkgMc = smearVarYields[name]["nBkgMc"]
        relSs = smearVarYields[name]["nSpur"]/nBkgMc
        relNom = smearVarYields[name]["nSpur"]/trueNom
        ss = smearVarYields[name]["nSpur"]
        relSs = abs(relSs)
        nTotal = smearVarYields[name]["nBkgInFitRange"]
        ss = abs(ss)
        s = "{0}\t{1:.2f}\t{2:.2f}\t{3:.4f}\t{4:.2f}\t{5:.0f}".format(name,ss,relNom,relSs,nBkgMc,nTotal)
        print yellow(s)

    # for name in sorted(pdfVarYields.keys()):
    #     nBkgMc = pdfVarYields[name]["nBkgMc"]
    #     relSs = pdfVarYields[name]["nSpur"]/nBkgMc
    #     ss = pdfVarYields[name]["nSpur"]
    #     relSs = abs(relSs)
    #     nTotal = pdfVarYields[name]["nBkgInFitRange"]
    #     relNom = pdfVarYields[name]["nSpur"]/trueNom
    #     ss = abs(ss)
    #     name = name.replace("backgroundTemplate","")
    #     name = name.replace("down","dn")
    #     name = name[0:13]
    #     # s = "{0}\t{1:.4f}\t{2:.4f}\t{3:.2f}\t{4:.0f}".format(name,ss,relSs,nBkgMc,nTotal)
    #     s = "{0}\t{1:.4f}\t{2:.2f}\t{3:.4f}\t{4:.2f}\t{5:.0f}".format(name,ss,relNom,relSs,nBkgMc,nTotal)
    #     print yellow(s)

    # print red("Yields comparison: nominal {0}, smeared {1}".format(yields["nBkg"],smearVarYields["m_uu_noSmear"]["nBkg"]))
    print red("FitMin",fitMin)

    print yellow("="*50)


    # return nBkg,pdfSs

# default inputs
release = "21"
lumi = 139
signalMass = 20
inject = 0

quick = True
quick = False

if quick:
    nPdfs=1
    nToys=1
else:
    nToys=200
    nPdfs=200
extrapMax = 6000

systematicsPath="../settings/systematics.py"
os.popen("rm plots/*")
fitOn = 1

systematics = {}
for channel in ["mm"]:
    systematics[channel]={}
    # for interference in ["const","dest"]:
    for interference in ["const"]:
        systematics[channel][interference]={}
        # for chirality in ["LL","LR","RL","RR"]:
        for chirality in ["LL"]:
            doFit(fitOn=1,useSbModel=0,lumi=lumi)

