import subprocess
import sys #,os,math,time
sys.path = ["../source"] + sys.path
from setup import *


################################################################################
# Script for running singleLimit.py
################################################################################


# new scatter, more mass ranges, optimization 030519
setupsDestEe  = [ \
                    {"channel":"ee","chirality":"LL","interference":"dest", "prevLimExp":19.9,"fitMin":240,"fitMax":2530,"extrapMin":2530},
                    {"channel":"ee","chirality":"LR","interference":"dest", "prevLimExp":21.2,"fitMin":200,"fitMax":1960,"extrapMin":1960},
                    {"channel":"ee","chirality":"RL","interference":"dest", "prevLimExp":21.2,"fitMin":200,"fitMax":1960,"extrapMin":1960},
                    {"channel":"ee","chirality":"RR","interference":"dest", "prevLimExp":21.0,"fitMin":240,"fitMax":2530,"extrapMin":2530},
               ]
setupsConstEe  = [ \
                    {"channel":"ee","chirality":"LL","interference":"const","prevLimExp":25.7,"fitMin":170,"fitMax":2630,"extrapMin":2630},
                    {"channel":"ee","chirality":"LR","interference":"const","prevLimExp":23.9,"fitMin":170,"fitMax":2760,"extrapMin":2760},
                    {"channel":"ee","chirality":"RL","interference":"const","prevLimExp":23.8,"fitMin":170,"fitMax":2140,"extrapMin":2140},
                    {"channel":"ee","chirality":"RR","interference":"const","prevLimExp":25.7,"fitMin":170,"fitMax":2760,"extrapMin":2760},
               ]
setupsConstEe[2]["fitMin"] = 300

# new b-only 2d scan optimization
setupsDestEe  = [ \
                    {"channel":"ee","chirality":"LL","interference":"dest", "prevLimExp":19.9,"fitMin":320,"fitMax":2075,"extrapMin":2773},
                    {"channel":"ee","chirality":"LR","interference":"dest", "prevLimExp":21.2,"fitMin":320,"fitMax":2075,"extrapMin":2773},
                    {"channel":"ee","chirality":"RL","interference":"dest", "prevLimExp":21.2,"fitMin":320,"fitMax":2075,"extrapMin":2773},
                    {"channel":"ee","chirality":"RR","interference":"dest", "prevLimExp":21.0,"fitMin":320,"fitMax":2075,"extrapMin":2773},
               ]
setupsConstEe  = [ \
                    {"channel":"ee","chirality":"LL","interference":"const","prevLimExp":25.7,"fitMin":320,"fitMax":2075,"extrapMin":2075},
                    {"channel":"ee","chirality":"LR","interference":"const","prevLimExp":23.9,"fitMin":320,"fitMax":2075,"extrapMin":2075},
                    {"channel":"ee","chirality":"RL","interference":"const","prevLimExp":23.8,"fitMin":320,"fitMax":2075,"extrapMin":2075},
                    {"channel":"ee","chirality":"RR","interference":"const","prevLimExp":25.7,"fitMin":320,"fitMax":2075,"extrapMin":2075},
               ]

# linearity optimization 220819 has been added to dictionary
setupsConstEe  = [ \
                    {"channel":"ee","chirality":"LL","interference":"const","prevLimExp":25.7},
                    {"channel":"ee","chirality":"LR","interference":"const","prevLimExp":23.9},
                    {"channel":"ee","chirality":"RL","interference":"const","prevLimExp":23.8},
                    {"channel":"ee","chirality":"RR","interference":"const","prevLimExp":25.7},
               ]
setupsDestEe  = [ \
                    {"channel":"ee","chirality":"LL","interference":"dest", "prevLimExp":19.9},
                    {"channel":"ee","chirality":"LR","interference":"dest", "prevLimExp":21.2},
                    {"channel":"ee","chirality":"RL","interference":"dest", "prevLimExp":21.2},
                    {"channel":"ee","chirality":"RR","interference":"dest", "prevLimExp":21.0},
               ]

# muons
setupsConstMm  = [ \
                    {"channel":"mm","chirality":"LL","interference":"const","prevLimExp":23.8},
                    {"channel":"mm","chirality":"LR","interference":"const","prevLimExp":22.5},
                    {"channel":"mm","chirality":"RL","interference":"const","prevLimExp":22.4},
                    {"channel":"mm","chirality":"RR","interference":"const","prevLimExp":22.2},
               ]
setupsDestMm  = [ \
                    {"channel":"mm","chirality":"LL","interference":"dest", "prevLimExp":18.4},
                    {"channel":"mm","chirality":"LR","interference":"dest", "prevLimExp":19.7},
                    {"channel":"mm","chirality":"RL","interference":"dest", "prevLimExp":19.8},
                    {"channel":"mm","chirality":"RR","interference":"dest", "prevLimExp":18.4},
               ]

# linearity optimization 220819 has been added to dictionary
setupsUnified  = [ \
                    {"channel":"ee","chirality":"LL","interference":"const","prevLimExp":25.7},
                    {"channel":"ee","chirality":"LL","interference":"dest", "prevLimExp":19.9},
                    {"channel":"mm","chirality":"LL","interference":"const","prevLimExp":23.8},
                    {"channel":"mm","chirality":"LL","interference":"dest", "prevLimExp":18.4},
               ]


# common parameters
lumi=139
nToys=2
nToys=50

# clear the output directories

# os.popen("rm plots/*")
os.popen("rm limits/*")
os.popen("rm logs/*")

log = "logs/log.txt"
err = "logs/err.txt"

logI=0
# for setups in [setupsConstEe]:
# for setups in [setupsDestEe]:
# for setups in [setupsDestEe,setupsConstEe]:
# for setups in [setupsDestMm,setupsConstMm]:
# for setups in [setupsDestEe,setupsConstEe,setupsDestMm,setupsConstMm]:
for setups in [setupsUnified]:
    parallelList = []
    for setup in setups:
        interference  = setup["interference"]
        chirality     = setup["chirality"]
        channel       = setup["channel"]
        fitMin        = dictionary.ranges[channel][interference][chirality]["fitMin"]
        fitMax        = dictionary.ranges[channel][interference][chirality]["fitMax"]
        extrapMin     = dictionary.ranges[channel][interference][chirality]["extrapMin"]

        # fitMin = 300
        # fitMax = 2000
        # extrapMin = 2000

        command = "python2 singleLimit.py"
        # command += r" quick={0}".format(quick)
        command += r" fitMin={0}".format(fitMin )
        command += r" lumi={0}".format(lumi)
        command += r" channel={0}".format(channel )
        command += r" fitMax={0}".format(fitMax )
        command += r" extrapMin={0}".format(extrapMin )
        command += r" interference={0}".format(interference)
        command += r" chirality={0}".format(chirality)
        command += r" nToys={0}".format(nToys)
        command += r" doPdf={0}".format(True)
        # command += r" signalMass={0}".format(signalMass)
        command += r" plotOn={0}".format(1)
        print green(command)
        # continue
        parallelList.append(subprocess.Popen(command.split(),
                                             stdout=open(log.replace(".txt",str(logI)+".txt"),"w"),
                                             stderr=open(err.replace(".txt",str(logI)+".txt"),"w")
                                            )
                           )
        logI+=1
        # print result
        # break

    for i,p in enumerate(parallelList):
        print red("Waiting {0}/{1}".format(i,len(parallelList)))
        p.wait()

print green("Done")



