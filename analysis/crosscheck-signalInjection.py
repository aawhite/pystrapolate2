from __future__ import division

################################################################################
# Cross check B-only vs S+B background models
# Comparison includes SS uncertainties
# Output is a control plot
################################################################################

import sys
sys.path = ["../source"] + sys.path
from setup import *
import plotter


def doFit(interference=None,
          chirality=None,
          channel=None,
          inject=None,
          signalMass=None,
          fitOn=True,
         ):
    """
    ############################## 
    Collect injection yields with S+B fit
    ############################## 
    """ 

    chi = "LL"
    fitMin = dictionary.ranges[channel][interference][chi]["fitMin"]
    fitMax = dictionary.ranges[channel][interference][chi]["fitMax"]
    extrapMin = dictionary.ranges[channel][interference][chi]["extrapMin"]
    print fitMin, fitMax, extrapMin
    # quit()

    ############################## 
    # do fit to "data"
    ############################## 
    backgroundName="backgroundTemplate"
    nominal = fitMacro(
                        interference=interference,
                        chirality=chirality,
                        channel=channel,
                        lumi=lumi,fitMin=fitMin,
                        fitMax=fitMax,extrapMin=extrapMin,
                        extrapMax=extrapMax,inject=inject,
                        signalMass=signalMass,
                        release=release,
                        plotOn=0,fitOn=fitOn,
                        # useSbModel=False,
                        useSbModel=True,
                        backgroundName=backgroundName,
                     )
    yields = nominal.yields()
    nominalSs = yields["nSpur"]
    nBkg = yields["nBkg"]
    nObs = yields["nObs"]
    nSig = yields["nSigMc"]
    yields["nReco"] = nObs-nBkg
    print green("nBkg",nBkg)
    print green("nSig",nSig)
    return yields

# default inputs
channel = "ee"
release = "21"
lumi = 139

extrapMax = 6000

os.popen("rm plots/*")
fitOn = 1

# signalMassesConst = [28,30,32,40,50] 
# signalMassesConst = [28,30,32,34,36,38,40,42,44,46,48,50] 
signalMassesConst = np.arange(18,51,4)
signalMassesConst = [18,22,26,30,34,40] 
# signalMassesConst = [18,30,40]
# signalMassesDest = [20,22,24,28,40] # for dest
# signalMassesDest = [16] # for dest
# signalMassesDest = [20] # for dest
# signalMassesDest = [24] # for dest
# signalMassesDest = [20,22,24,26,28,30,32,34,36] # for dest
signalMassesDest  = [20,22,24,26,28,30] # for dest
# signalMassesDest = [20,28,36] # for dest
signalMasses = {"const":signalMassesConst,
                "dest": signalMassesDest}

# for channel in ["ee","mm"]:
for channel in ["mm"]:
# for channel in ["ee"]:
    # for interference in ["const","dest"]:
    # for interference in ["dest"]:
    for interference in ["const"]:
        # for chirality in ["LL","LR","RL","RR"]:
        # for chirality in ["RR","LR"]:
        for chirality in ["LL"]:
        # for chirality in ["LR"]:
        # for chirality in ["RL"]:
        # for chirality in ["RR"]:
            yields = {}
            ss = systematics.systematics[channel][interference]

            for signalMass in signalMasses[interference]:
                signalMass=int(signalMass)
                thisYield = doFit(interference=interference,chirality=chirality,channel=channel,
                                  inject=True,signalMass=signalMass,
                                 )
                yields[signalMass] = thisYield

            # make plot
            path = "plots/injFlat-{0}-{1}-{2}.png".format(interference,chirality,channel)
            # score = plotter.injectionPlotFlat(yields,ss,interference,chirality,channel,path=path)
            path = "plots/inj-{0}-{1}-{2}.png".format(interference,chirality,channel)
            plotter.injectionPlot(yields,ss,interference,chirality,channel,path=path)
            path = "limits/inj-{0}-{1}-{2}.pickle".format(interference,chirality,channel)
            pickle.dump([yields,ss],open(path,"w"))

            # quit()



print green("DONE")

