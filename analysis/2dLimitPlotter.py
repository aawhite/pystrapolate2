from __future__ import division
import sys,os
sys.path = [os.path.dirname(os.path.abspath(__file__))+"/../source"] + sys.path
from setup import *
import plotter
from  matplotlib import cm as cm

def lambdaLimit(limitDict,chirality,interference):
    """ Return lambda limit """
    import lambdaConversion as lc
    observed      = limitDict["upperLimit"]
    expected      = limitDict["expLimit"]
    channel       = limitDict["channel"]
    extrapMin     = limitDict["extrapMin"]
    extrapMax     = limitDict["extrapMax"]
    lumi          = limitDict["lumi"]
    # chirality     = limitDict["chirality"]
    # interference  = limitDict["interference"]

    lambdaConvert = lc.lambdaLimit() # class to calculate limits
    observedLambda = lambdaConvert.getLambdaLimit(observed,extrapMin,extrapMax,channel,model=chirality,interference=interference,lumi=lumi)
    # expected = lambdaConvert.getLambdaLimit(expected,extrapMin,extrapMax,channel,model=chirality,interference=interference,lumi=lumi)
    return observedLambda


# fit min/max scan
# iPath = "/media/grandBox/dilepton/condorOut/limits/limit2d-limits-1gev-FitMinMaxScan40x40/*{0}*"
# iPath = "/media/grandBox/dilepton/condorOut/limits/limit2d-limits-1gev-FitMinMaxScan100x100/*{0}*"
# iPath = "/media/grandBox/dilepton/condorOut/limits/limit2d-limits-narrow-FitMinMaxScan40x40/*{0}*"
# iPath = "/media/grandBox/dilepton/condorOut/limits/limit2d-limits-narrow-FitMinMaxScan100x100/*{0}*"
iPath = "/media/grandBox/dilepton/condorOut/limits/limit2d-limits-rounded-FitMinMaxScan40x40/*{0}*"
# iPath = "/media/grandBox/dilepton/condorOut/limits/limit2d-limits-rounded-mm-FitMinMaxScan40x40/*{0}*"
# iPath = "limits/*{0}*"
xLabel = "fitMax"
yLabel = "fitMin"

# extrapMin scan
# iPath = "/media/grandBox/dilepton/condorOut/limits/limit2d-limitsExtrapMaxScan40x40/*{0}*"
iPath = "/media/grandBox/dilepton/condorOut/limits/limit2d-limits-narrow-ExtrapMinScan40x40/*{0}*"
iPath = "/media/grandBox/dilepton/condorOut/limits/limit2d-limits-optFitMin-ExtrapMinScan40x40/*{0}*"
iPath = "/media/grandBox/dilepton/condorOut/limits/limit2d-limits-narrow-ExtrapMinScan100x100/*{0}*"
iPath = "/media/grandBox/dilepton/condorOut/limits/limit2d-limits-rounded-ExtrapMinScan40x40/*{0}*"
yLabel = "fitMax"
xLabel = "extrapMin"

# scan with low-stat sample
yLabel = "fitMax"
xLabel = "extrapMin"
iPath = "/media/grandBox/dilepton/condorOut/output/limit2d-limits-lowStats-ExtrapMinScan40x40/*{0}*"
iPath = "/media/grandBox/dilepton/condorOut/output/limit2d-limits-smooth-ExtrapMinScan40x40/*{0}*"


# xLabel = "fitMax"
# yLabel = "fitMin"
# # lowStats sample
# # iPath = "/media/grandBox/dilepton/condorOut/output/limit2d-limits-lowStats-FitMinMaxScan40x40/*{0}*"
# # smooth sample
# iPath = "/media/grandBox/dilepton/condorOut/output/limit2d-limits-smooth-FitMinMaxScan40x40/*{0}*"

# for 
xLabel = "fitMax"
yLabel = "fitMin"
# for plotting chi2
iPath = "/media/grandBox/dilepton/condorOut/output/limit2d-limits-chi2Test-FitMinMaxScan40x40/*{0}*"
# variables
# limitResult["chi2Ndof_cr"]=chi2Ndof
# limitResult["chi2_cr"] = chi2


vmin=None
vmax=None
vmin=0;vmax=1
setups = [

    # {"variable":"upperLimit", "variableName":"Upper Limit", "vmax":None, "vmin":None},

    # {"variable":"fitSsRel", "variableName":"Fit SS (/N-Bkg)", "vmax":vmax, "vmin":vmin},
    # {"variable":"pdfSsRel", "variableName":"PDF SS (/N-Bkg)", "vmax":vmax, "vmin":vmin},
    # {"variable":"meanFitSs", "variableName":"Mean SS from toys (/N-Bkg)", "vmax":vmax, "vmin":vmin},
    # {"variable":"meanPdfSs", "variableName":"Mean SS from PDFs (/N-Bkg)", "vmax":vmax, "vmin":vmin},

    # scan of chi2 
    {"variable":"chi2Ndof_cr", "variableName":"$\Chi^2$/nDOF value","color":cm.summer, "vmax":0.1, "vmin":None},
    {"variable":"chi2_cr", "variableName":"$\Chi^2$ value","color":cm.summer, "vmax":200, "vmin":None},

    # {"variable":"limit", "variableName":"95\% CL Limit on $\Lambda$ [TeV]", "vmax":32, "vmin":25},
    # {"variable":"limit", "variableName":"95\% CL Limit on $\Lambda$ [TeV]", "vmax":25, "vmin":None},
]



os.popen("rm plots/*")
interferences = ["const","dest"]
channels = ["ee","mm"]
chiralities   = ["LL","LR","RL","RR"]
vmaxChir = {"ee":{"const":{"LL":31,"LR":28,"RL":28,"RR":31},
                  "dest":{"LL":24,"LR":26,"RL":26,"RR":24}},
            "mm":{"const":{"LL":29,"LR":27,"RL":27,"RR":29},
                  "dest":{"LL":22,"LR":23,"RL":23,"RR":22}},
           }
vminChir = {"ee":{"const":{"LL":27,"LR":25,"RL":25,"RR":27},
            "dest":{"LL":19,"LR":19,"RL":19,"RR":19}},
            "mm":{"const":{"LL":26,"LR":24,"RL":24,"RR":26},
            "dest":{"LL":19,"LR":20,"RL":20,"RR":19}},
           }



# interferences = ["dest"]
interferences = ["const"]
chiralities   = ["LL"]
# channels      = ["ee"]



for setup in setups:
    variable=setup["variable"]
    variableName=setup["variableName"]
    # vmax=setup["vmax"]
    # vmin=setup["vmin"]
    for channel in channels:
        for interference in interferences:
            for chirality in chiralities:
                if variable=="limit":
                    vmax=vmaxChir[channel][interference][chirality]
                    vmin=vminChir[channel][interference][chirality]
                else:
                    vmax=setup["vmax"]
                    vmin=setup["vmin"]
                # vmin = None
                # vmax = None
                print green("2d scan for",interference,chirality)
                data = defaultdict(dict)
                # modelPath=iPath.format(interference+"-"+chirality)
                modelPath=iPath.format(channel+"-const"+"-"+"LL")
                modelPaths = glob.glob(modelPath)
                if len(modelPaths)==0:
                    print red("Empty set",modelPath)
                    continue
                for lim in modelPaths:
                    limitDict = pickle.load(open(lim))
                    if limitDict["fitMax"]>3000: continue
                    if limitDict["extrapMin"]>3000: continue
                    x = limitDict[xLabel]
                    y = limitDict[yLabel]
                    if xLabel=="extrapMin" and y<1800: continue
                    if variable=="limit":
                        data[y][x]=lambdaLimit(limitDict,chirality,interference)
                    else:
                        # data[y][x]=limitDict[variable]/limitDict["nBkgMc"]
                        data[y][x]=limitDict[variable]
                        # print limitDict[variable]

                path = "plots/2dscan-{0}-{1}-{2}-{3}.png".format(variable,interference,chirality,channel)
                plotter.scanLimitPlot(data,xLabel,yLabel,
                                      channel=channel,
                                      variable=variableName,
                                      vmax=vmax,
                                      vmin=vmin,
                                      path=path,
                                      setup=setup
                                     )
                # quit()

                # break
            # break

                if variable!="limit": break
            if variable!="limit": break
