from __future__ import division

################################################################################
# Cross check B-only vs S+B background models
# Comparison includes SS uncertainties
# Output is a control plot
################################################################################

import sys
sys.path = ["../source"] + sys.path
from setup import *
import plotter

def doFit(fitToData=False,useSbModel=None):
    """
    ############################## 
    #    1) Do nominal fit
    #    2) Do toy fits
    #    3) Do PDF fits
    #    4) Do fit to "data"
    ############################## 
    """ 

    fitMax = dictionary.ranges[channel][interference][chirality]["fitMax"]
    extrapMin = dictionary.ranges[channel][interference][chirality]["extrapMin"]
    print fitMax, extrapMin

    nBkgs = {}
    # for fitMin in np.arange(250,400,10):
    for fitMin in np.arange(250,501,10):
    # for fitMin in np.arange(150,701,10):
        fitMin=int(fitMin)
        ############################## 
        # do fit
        ############################## 
        nominalMc = fitMacro(
                            interference=interference,
                            chirality=chirality,
                            channel=channel,
                            lumi=lumi,fitMin=fitMin,
                            fitMax=fitMax,extrapMin=extrapMin,
                            extrapMax=extrapMax,inject=inject,
                            signalMass=signalMass,
                            release=release,
                            plotOn=0,fitOn=fitOn,
                            useSbModel=useSbModel,
                            backgroundName="backgroundTemplate",
                         )
        yields = nominalMc.yields()
        nBkg = yields["nBkg"]
        nominalSs = yields["nSpur"]
        nBkgs[fitMin] = nBkg
    print green("nBkg",nBkg)

    return nBkgs

# default inputs
release = "21"
lumi = 139
signalMass = 20
inject = 0
extrapMax = 6000

os.popen("rm plots/*")
fitOn = 1
chirality="LL"


# for channel in ["ee"]:
# for channel in ["mm"]:
for channel in ["ee","mm"]:
    for interference in ["const","dest"]:
    # for interference in ["dest"]:
    # for interference in ["const"]:

        fitToData=False
        nBkgs = doFit(fitToData=fitToData,useSbModel=False)
        print nBkgs
        for k in sorted(nBkgs):
            print green(k,"\t",nBkgs[k])

        # nBkgs = {260: 24.335344323512615, 390: 23.296019510379605, 270: 22.56761553748101, 400: 23.116164783525893, 280: 22.603559059950186, 410: 23.167219332445892, 290: 22.918189902289814, 420: 22.908843190416203, 300: 23.037992727183266, 430: 23.400559750179646, 310: 22.97822738952509, 440: 23.405313868417537, 320: 22.841151535835866, 450: 23.230049004774344, 330: 23.123171953465956, 460: 23.431278267055852, 340: 23.23004033527951, 470: 23.437142079499875, 220: 20.39648747110198, 350: 23.389286939239966, 480: 22.02441402493249, 230: 21.094270471721874, 360: 23.153377488680757, 490: 22.114363040390025, 240: 23.762200354915198, 370: 23.33979332540903, 500: 22.123906074580447, 250: 23.821129400211284, 380: 23.126251687195555}
        plotter.histoFitMinStability(nBkgs,interference=interference,channel=channel,lumi=lumi,xlabel="Background Expected in SR",path="plots/output.png")



print green("DONE")

