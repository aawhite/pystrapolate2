from __future__ import division
import sys, os
sys.path = [os.path.dirname(os.path.abspath(__file__)) + "/../source"] + sys.path
from setup import *
import significance

def makeWs(nBkg, nObs, npWidth1=1, npWidth2=1, npWidth3=1):
    w = RooWorkspace("w", "w")
    w.factory("x[0]")

    lumi = 139
    w.factory("sum:nExp(nSig[0,-100,100],prod:bkgProd(sum:bkgSum(1,np1[0,-1,1],np2[0,-1,1],np3[0,-1,1]),nBkg[{0}]))".format(nBkg))
    w.factory("Poisson:pois(nObs[0],nExp)");
    w.factory("Gaussian:g1(np1,0,npWidth1[{0}])".format(npWidth1 / nBkg));
    w.factory("Gaussian:g2(np2,0,npWidth2[{0}])".format(npWidth2 / nBkg));
    w.factory("Gaussian:g3(np3,0,npWidth3[{0}])".format(npWidth3 / nBkg));
    # Gaussian constraint for Lumi uncertainty  1.7% holding on off this until we decide on nSig or not..
    # w.factory("Gaussian:g4(np4,0,npWidth4[{0}])".format(0.017))
    w.factory("PROD:model(pois,g1,g2,g3)")

    # print w.obj("np1Width2").getVal()
    w.defineSet("nps", "np1,np2,np3")
    # w.defineSet("globs","nExp")
    ###
    mc = ModelConfig("sbModel",w);
    mc.SetPdf(w.pdf("model"));
    mc.SetParametersOfInterest(RooArgSet(w.var("nSig")));
    mc.SetObservables(RooArgSet(w.var("nObs")));
    mc.SetNuisanceParameters(RooArgSet(w.set("nps")));

    # these are needed for the hypothesis tests
    mc.SetSnapshot(RooArgSet(w.var("nSig")));
    mc.SetGlobalObservables(""); # removed etienne
    mc.Print();
    # import model in the workspace
    getattr(w,"import")(mc);
    # make data set with the number of observed events
    w.var("nObs").setVal(nObs);
    data = RooDataSet("data","", RooArgSet(w.var("nObs")));
    data.add(RooArgSet(w.var("nObs")));
    # import data set in workspace and save it in a file
    getattr(w,"import")(data);
    # ##################################################
    sbModel = mc;

    # create null hypothesis H0 (based on H)
    bModel = sbModel.Clone();
    bModel.SetName("bModel");
    bModel.SetSnapshot((RooArgSet(w.var("nSig"))));
    # fix nSig value to 0
    nSigNoSig = bModel.GetParametersOfInterest().first();
    nSigNoSig.setVal(0);
    # nSigNoSig.setConstant();
    bModel.SetSnapshot(RooArgSet(nSigNoSig));

    # w.SaveAs("workspaces/{0}".format(outFile))
    return w

def doFit(w):

    myWs = w
    print yellow("==") * 25
    print yellow("_______Printing WS")

    myWs.Print()
    mc = myWs.obj("sbModel")
    pdf = mc.GetPdf()
    data = myWs.data("data")
    np1Sig = myWs.obj("npWidth1").getValV()
    np2Sig = myWs.obj("npWidth2").getValV()
    nSigI = myWs.obj("nSig").getValV()

    nll = pdf.createNLL(data, ROOT.RooFit.Constrain(mc.GetNuisanceParameters()))
    nll.enableOffsetting(True)

    np = mc.GetNuisanceParameters()
    iter = np.createIterator()
    poi = mc.GetParametersOfInterest()
    iterPoi = poi.createIterator()

    histos = {}
    b = iterPoi.Next()
    while b:
        rrvPOI = b
        rrvPOI.setConstant(False)
        rrvPOI.setVal(0)
        rrvPOI.setMin(-300)
        rrvPOI.setMax(300)

        if rrvPOI:
            poiName = rrvPOI.GetName()
            histos[poiName] = ROOT.TH1F("h_{0}".format(poiName), "", 3, 0, 3)
            histos[poiName].SetCanExtend(ROOT.TH1.kAllAxes)

        b = iterPoi.Next()
    iterPoi.Reset()

    iter2 = np.createIterator()
    a = iter2.Next()
    while a:
        rrv = a
        rrv.setVal(0.0001)
        a = iter2.Next()
    iter2.Reset()

    # Initial fit to get central values of POI without NP variations
    print yellow("==") * 25
    print yellow("Setting up Minimizer")
    minim_ini = ROOT.RooMinimizer(nll)
    minim_ini.setPrintLevel(0)
    minim_ini.setStrategy(2)
    minim_ini.setOffsetting(True)
    minim_ini.setProfile()
    minim_ini.setEps(0.00001)
    minim_ini.minimize("Minuit")
    minim_ini.minos()

    print yellow("==") * 25
    print yellow("Initial Fit")
    result_ini = minim_ini.save()
    result_ini.Print()

    print yellow("==") * 25
    print yellow("Initial nSig: {0}, Final nSig: {1}".format(nSigI,w.obj("nSig").getValV()))

    return w.obj("nSig").getValV()

channels = {"ee"}
interferences = {"const"}

table = ""

for channel in channels:
    for interference in interferences:
        nObs, nBkg, pdfSs, fitSs, funcSs = getSyst(channel, interference, func=1)
        
        w = makeWs(nBkg,nObs,fitSs,pdfSs,funcSs)
        nSig = doFit(w)
        
        sigDict = significance.significance(nBkg, nObs, nSig, npWidth1=pdfSs, npWidth2=fitSs, npWidth3=funcSs,doAcCalc=True)
    
        table += "{0} & {1} \\\ \n".format(channel, interference)
        # table += "nObs & nBkg & nSig & pdfSs & fitSs & funcSs \\\ \n"
        # table += "{0} & {1} & {2} & {3} & {4} & {5} \\\ \n".format(nObs, nBkg, nSig, pdfSs, fitSs, funcSs)
        table += "p-value & significance \\\ \n"
        table += "{0} & {1}  \\\ \n".format(sigDict["pValue"], sigDict["significance"])
        
print yellow(table)
