from __future__ import division

################################################################################
# Acceptance times efficiency
################################################################################

import sys
sys.path = ["../source"] + sys.path
from setup import *
import plotter

def integrate(h,low):
    """ Return integral of h above low """
    ret = h.Integral(h.FindBin(low),h.GetNbinsX())
    return ret

def mean(h,low):
    """ Return integral of h above low """
    lo = h.FindBin(low)
    hi = h.GetNbinsX()
    ingl = h.Integral(lo,hi)
    ret = ingl/(hi-lo)
    return ret

rootFiles=[]
def loadH(channel,name):
    """ Load either histo or TH1 """
    global rootFiles
    path = dictionary.data[release][channel][name].split(":")[0]
    name = dictionary.data[release][channel][name].split(":")[1]
    f    = TFile.Open(path)
    inpt = f.Get(name)
    rootFiles.append(f)
    # make new clone with proper range
    new = TH1F("","",10000-130,130,10000)
    if "TF1" in name:
        print yellow("Loading from function")
        for i in range(0,new.GetNbinsX()+0):
            lo = new.GetBinLowEdge(i)
            hi = new.GetBinLowEdge(i+1)
            c = new.GetBinCenter(i)
            new.Fill(c,inpt.Integral(lo,hi))
    else: # fill from TH1
        print yellow("Bin width of {0} is {1}".format(name,inpt.GetBinWidth(1)))
        for i in range(0,inpt.GetNbinsX()+1):
            c = inpt.GetBinCenter(i)
            v = inpt.GetBinContent(i)
            new.Fill(c,v)
    # check loading worked:
    # inpt.Draw("hist")
    # new.Draw("histsame")
    # raw_input()
    return new

def getAccTimesEffMc(channel=None,interference=None):
    """ Return acc*eff based on MC shape """
    # get ranges
    chi ="LL"
    extrapMin = dictionary.ranges[channel][interference][chi]["extrapMin"]

    backgroundName = "backgroundTemplate"
    effHist = loadH(channel,"accEff")
    recoHist = loadH(channel,backgroundName)

    # not real truth mind you
    truthHist = recoHist.Clone()
    truthHist.Divide(effHist)

    effInt  = integrate(effHist,extrapMin)
    print yellow("Eff*Acc integral",effInt)
    recoInt  = integrate(recoHist,extrapMin)
    print yellow("reco",recoInt)
    truthInt  = integrate(truthHist,extrapMin)
    print yellow("truth",truthInt)

    accTimesEff = recoInt/truthInt
    print green("acc*Eff",accTimesEff)

    return accTimesEff

def getAccTimesEff(channel=None,interference=None):
    """ Return acc*eff using single bin (flat shape) """
    # get ranges
    chi ="LL"
    extrapMin = dictionary.ranges[channel][interference][chi]["extrapMin"]

    backgroundName = "backgroundTemplate"
    effHist = loadH(channel,"accEff")

    # nObs,nBkg,pdfSs,fitSs,funcSs = getSyst(channel,interference,func=1)
    meanEff  = mean(effHist,extrapMin)

    return meanEff

accEffs = {}
release = "21"
for channel in ["ee","mm"]:
    for interference in ["const","dest"]:
        name=channel+"-"+interference
        accEffMc = getAccTimesEffMc(channel=channel,interference=interference)
        accEffs[name+"-MC"] = accEffMc
        accEffMc = getAccTimesEff(channel=channel,interference=interference)
        accEffs[name] = accEffMc

ks = sorted(accEffs.keys(),key=lambda x:1*("ee" in x)+10*("MC" in x))
for k in ks:
    print r"{0} & {1:.3f} \\".format(k,accEffs[k])

print green("DONE")

