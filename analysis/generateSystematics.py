from __future__ import division
from multiprocessing import Process, Queue
import os


################################################################################
# Generate systematics
# jG
################################################################################

# remvoe previous systematic file before importing
rewriteOn = False
# rewriteOn = True
if rewriteOn:
    systematicsPath="../settings/systematics.py"
    os.popen("rm {0}".format(systematicsPath))

import sys
sys.path = ["../source"] + sys.path
from setup import *
import plotter

def doFit(outQueue,interference="const",chirality="LL",channel="ee",useSbModel=None):
    """
    ############################## 
    #    1) Do nominal fit
    #    2) Do toy fits
    #    3) Do PDF fits
    #    4) Do fit to "data"
    ############################## 
    """ 
    fitSs=0
    pdfSs=0
    chi = "LL"
    fitMin = dictionary.ranges[channel][interference][chi]["fitMin"]
    fitMax = dictionary.ranges[channel][interference][chi]["fitMax"]
    extrapMin = dictionary.ranges[channel][interference][chi]["extrapMin"]
    name = channel+"-"+interference
    print fitMin, fitMax, extrapMin
    # quit()
    # return 1,2,3

    ############################## 
    # do fit
    ############################## 

    nominalMcFit = fitMacro(
                        interference=interference,
                        chirality=chirality,
                        channel=channel,
                        lumi=lumi,fitMin=fitMin,
                        fitMax=fitMax,extrapMin=extrapMin,
                        extrapMax=extrapMax,inject=inject,
                        signalMass=signalMass,
                        release=release,
                        plotOn=plotOn,fitOn=fitOn,
                        useSbModel=useSbModel,
                        backgroundName="backgroundTemplate",
                        # fitToData=1, 
                     )
    yields = nominalMcFit.yields()
    nominalMcSs = yields["nSpur"]

    nominalDataFit = fitMacro(
                        interference=interference,
                        chirality=chirality,
                        channel=channel,
                        lumi=lumi,fitMin=fitMin,
                        fitMax=fitMax,extrapMin=extrapMin,
                        extrapMax=extrapMax,inject=inject,
                        signalMass=signalMass,
                        release=release,
                        plotOn=plotOn,fitOn=fitOn,
                        useSbModel=useSbModel,
                        backgroundName="data",
                        fitToData=1, 
                     )
    yields = nominalDataFit.yields()
    nBkg = yields["nBkg"]
    nominalDataSs = yields["nSpur"]
    print green("nBkg",nBkg)
    print green("Data SS",nominalDataSs)
    # return 0,0,0
    # quit()

    # print nominalDataFit._chi2NDof; quit()

    ############################## 
    # toys
    ############################## 
    for i in range(nToys):
        nominalDataFit.fitToy(saveToy=0)
    fitSsDist = nominalDataFit.toyFitDiff()
    fitSs = np.array(fitSsDist).std()



    # draw diagnostic plots for toys
    print red("Fit SS")
    print red(fitSsDist)
    if 0:
        path = "plots/fitSs-{0}.png".format(name)
        print "Plotting",path
        plotter.uncertHisto(fitSsDist,nominalMcSs,nominalDataSs,fitSs,path=path,
                            showNominal=True,
                            xlabel="Fit Spurious Signal")
        # detailed plot
        # path = path.replace("fitSs","fitSsBand")
        # plotter.plot_details(nominalFit,path=path)
    # quit()

    # draw toy distribution plot
    # plotter.plotToys(nominalDataFit); quit()

    ############################## 
    # fit PDF uncertainties
    ############################## 
    pdfVarYields={}
    loopNames = dictionary.pdfNames[release]
    loopNames.append("backgroundTemplate")
    for i, pdfName in enumerate(loopNames):
        if i==nPdfs: break
        if channel=="mm" and "eeOnly" in pdfName: continue
        print green("Fitting pdf variaiton",pdfName)
        pdfVariation = fitMacro(interference=interference,
                      chirality=chirality,
                      channel=channel,
                      lumi=lumi,fitMin=fitMin,
                      fitMax=fitMax,extrapMin=extrapMin,
                      extrapMax=extrapMax,inject=inject,
                      signalMass=signalMass,
                      release=release,
                      useSbModel=useSbModel,
                      plotOn=0,fitOn=fitOn,
                      backgroundName=pdfName,
                     )
        pdfVarYields[pdfName]=pdfVariation.yields()
    pdfSsDist = [abs(pdfVarYields[y]["nSpur"]) for y in pdfVarYields.keys()]
    print "For",channel,interference
    for varName in pdfVarYields.keys():
        print red(pdfVarYields[varName]["nSpur"],varName,"SPUR\t")
    print red(nominalDataSs,"Nominal SPUR\t")
    print red(nominalMcSs,"Nominal SPUR\t")
    # raw_input()
    if len(pdfSsDist):
        pdfSs = max(pdfSsDist)
        # pdfSs = max(pdfSs,nominalSs)

    # draw diagnostic plots for PDFs
    if 0:
        path = "plots/pdfSs-{0}.png".format(name)
        plotter.uncertHisto(pdfSsDist,nominalMcSs,nominalDataSs,pdfSs,path=path,
                            showNominal=0,
                            xlabel="PDF Spurious Signal")

    outQueue.put({"nBkg":nBkg,"pdfSs":pdfSs,"fitSs":fitSs})
    return nBkg,pdfSs,fitSs

# default inputs
release = "21"
lumi = 139
signalMass = 20
inject = 0

quick = True
# quick = False

if quick:
    nPdfs=90
    nPdfs=4
    # nToys=2000
    # nToys=400
    nToys=2
    # systematicsPath=systematicsPath.replace(".py","-quick.py")
else:
    nToys=50
    nPdfs=200
extrapMax = 6000

os.popen("rm plots/*")
fitOn = 1
plotOn = 0

queues = defaultdict(lambda:defaultdict(list))
processes = defaultdict(lambda:defaultdict(list))
systematics = defaultdict(lambda:defaultdict(dict))
for channel in ["ee","mm"]:
# for channel in ["ee"]:
# for channel in ["mm"]:
    # for interference in ["const","dest"]:
    # for interference in ["dest"]:
    for interference in ["const"]:
        queues[channel][interference] = Queue()
        processes[channel][interference]=Process(target=doFit,
                            args=(queues[channel][interference],),
                            kwargs = {"interference":interference,
                                      "chirality":"LL",
                                      "channel":channel,
                                      "useSbModel":False})
                                 # stdout=open(log.replace(".txt",str(logI)+".txt"),"w"),
                                 # stderr=open(err.replace(".txt",str(logI)+".txt"),"w")

        ##############################################
        ## to run in single thread mode, uncomment:
        ##############################################
        #processes[channel][interference].start()
        #processes[channel][interference].join()
#print green("DONE"); quit()


for channel in processes.keys():
    for interference in processes[channel].keys():
        print yellow("Starting",channel,interference)
        processes[channel][interference].start()

# print green("DONE");quit()

for channel in processes.keys():
    for interference in processes[channel].keys():
        print yellow("Joining",channel,interference)
        # nBkg,pdfSs,fitSs = processes[channel][interference].get()
        processes[channel][interference].join()
        systematic = queues[channel][interference].get()
        systematics[channel][interference]=systematic

            # nBkg,pdfSs,fitSs = doFit(useSbModel=False)


systematics = {c:{i:systematics[c][i] for i in systematics[c].keys()} for c in systematics.keys()}
print systematics
if rewriteOn:
    f=open(systematicsPath,'w')
    f.write("import numpy as np\n")
    f.write("nan = np.nan\n\n")
    f.write("systematics = {0}\n\n".format(repr(systematics)))
    f.write("nToys_toGenerate = {0}\n\n".format(repr(nToys)))
    f.write("nPdfs_toGenerate = {0}\n\n".format(repr(nPdfs)))
    f.write("lumiUncert = {0}\n\n".format(0.017))
    print green("Write systematics to {0}\n".format(systematicsPath))
else:
    print yellow("Systematics not written")
print green("DONE")


for k in systematics.keys():
    for j in systematics[k].keys():
        print k,j,systematics[k][j]["pdfSs"]
